unit common;

interface

uses
  Classes, SysUtils, strutils, Forms, Controls, Grids,
  //GR32, GR32_Image, GR32_Transforms, GR32_System, GR32_Resamplers, Graphics,
  Math;

type
  TFilenameContainer = record
    InputFilename: string;
    InputFileDir: string;
    OutputFilename: string;
    OutputFnMaxLen: integer;
  end;
  AFilenamesConstainer = array of TFilenameContainer;
  PFilenamesConstainer = ^AFilenamesConstainer;

const
     {$IFDEF MSWINDOWS}
  sep = '\';
     {$ELSE}
  sep = '/';
     {$IFEND}

  CR = 13;
  LF = 10;

  const_filter_filename = '%(Filename)%';
  const_filter_filedir = '%(FileDir)%';

  //const_ini_section = 'config';
  const_ini_dir_input = 'config$input';
  const_ini_dir_output = 'config$output';
  const_ini_file_mask = 'config$file_mask';
  const_ini_out_filename = 'config$output_filename';
  const_ini_out_file_line = 'config$output_file_line';
  const_ini_lowercase = 'config$lowercase';
  const_ini_filters_rowcount = 'config$filters_rowcount';
  const_ini_leave_files = 'config$leave_original_files';
  const_ini_create_file = 'config$create_file';
  const_ini_constants_rowcount = 'config$constants_rowcount';
  const_ini_replace_d = 'config$replace_d';
  const_ini_full_path = 'config$show_full_path';
  const_ini_fn_with_ext = 'config$fn_with_ext';

  const_out_filename = '%FILENAME%';
  const_out_fn_ucase = '%FN_UCASE%';
  const_out_date = '%DATE%';
  const_out_datetime = '%DATETIME%';

function _ReadDir(input: string; Container: PFilenamesConstainer;
  Extensions: string = '*.*'; search_subdirs: boolean = False): boolean;
procedure MakeDir(Dir: string);
function GetDateTimeString(d: TDateTime; time: boolean = False): string;
procedure WriteControlPlacement(f: TForm; only_wnd: boolean = True);
procedure ReadControlPlacement(f: TForm; only_wnd: boolean = True);
procedure PositionCB(const sgGrid: TStringGrid;
  var iTop, iLeft, iHeight, iWidth: integer);
function Replace(_src, _from, _to: string): string;
function ToStringLength(s: string; l: integer): string;
procedure _Log(Text: string);
{
procedure ResizeJPEGImage(source_file,dest_file:PAnsiChar;
  width,height:Integer;
  keep_aspect:Boolean=True;
  quality:Integer=90);
}
implementation

uses
  main, MxCfgLog;

function _ReadDir(input: string; Container: PFilenamesConstainer;
  Extensions: string = '*.*'; search_subdirs: boolean = False): boolean;
const
  {$IFDEF MSWINDOWS}
  FileMask = '*.*';
  {$ELSE}
  FileMask = '*';
  {$IFEND}
var
  i: integer;
  Rec: TSearchRec;
  tmpAContainer: AFilenamesConstainer;
begin
  i := Length(Container^);
  if FindFirst(input + sep + FileMask, (faArchive and not faDirectory), Rec) = 0 then
    try
      repeat
        main.FormMain.StatusBarApp.Panels[1].Text := input;
        Application.ProcessMessages;
        if (AnsiPos(ExtractFileExt(Rec.Name), Extensions) > 0) or (Extensions = '') then
        begin
          SetLength(Container^, i + 1);
   {$IFDEF MSWINDOWS}
          Container^[i].InputFilename := AnsiToUTF8(Rec.Name);
          Container^[i].InputFileDir := AnsiToUTF8(input);
   {$ELSE}
          Container^[i].InputFilename := Rec.Name;
          Container^[i].InputFileDir := input;
   {$IFEND}
          Inc(i);
        end;
      until FindNext(Rec) <> 0;
    finally
      SysUtils.FindClose(Rec);
    end;

  //SetCurrentDir(input);
  if (search_subdirs = True) and
    (FindFirst(input + sep + '*', (faDirectory), Rec) = 0) then
    try
      repeat
        if ((Rec.Attr and faDirectory) <> 0) and (Rec.Name <> '.') and
          (Rec.Name <> '..') then
        _ReadDir(input + sep + Rec.Name, Container, Extensions, search_subdirs);
      until FindNext(Rec) <> 0;
    finally
      FindClose(Rec);
    end;
end;

procedure MakeDir(Dir: string);

  function Last(What: string; Where: string): integer;
  var
    Ind: integer;

  begin
    Result := 0;

    for Ind := (Length(Where) - Length(What) + 1) downto 1 do
      if Copy(Where, Ind, Length(What)) = What then
      begin
        Result := Ind;
        Break;
      end;
  end;

var
  PrevDir: string;
  Ind: integer;

begin
  if not DirectoryExists(Dir) then
  begin
    // if directory don't exist, get name of the previous directory

    Ind := Last(sep, Dir);         //  Position of the last '/'
    PrevDir := Copy(Dir, 1, Ind - 1);    //  Previous directory

    // if previous directoy don't exist,
    // it's passed to this procedure - this is recursively...
    if not DirectoryExists(PrevDir) then
      MakeDir(PrevDir);

    // In thats point, the previous directory must be exist.
    // So, the actual directory (in "Dir" variable) will be created.
    CreateDir(Dir);
  end;
end;

function GetDateTimeString(d: TDateTime; time: boolean = False): string;
var
  yr, mnth, day, h, m, s, ms: word;
begin
  DecodeDate(d, yr, mnth, day);
  DecodeTime(Now, h, m, s, ms);
  Result := Format('%4d', [yr]) + Format('%02d', [mnth]) +
    Format('%02d', [day]);
  if time then
    Result := Result + Format('%02d', [h]) + Format('%02d', [m]) +
      Format('%02d', [s]);
  Result := StringReplace(Result, ' ', '0', [rfReplaceAll]);
end;

procedure WriteControlPlacement(f: TForm; only_wnd: boolean = True);
var
  idx: integer;
  ctrl: TControl;
begin
  try
    Cfg[f.Name + '$Top'] := IntToStr(f.Top);
    Cfg[f.Name + '$Left'] := IntToStr(f.Left);
    Cfg[f.Name + '$Width'] := IntToStr(f.Width);
    Cfg[f.Name + '$Height'] := IntToStr(f.Height);

    for idx := 0 to -1 + f.ComponentCount do
    begin
      if f.Components[idx] is TControl then
      begin
        ctrl := TControl(f.Components[idx]);
        if (not only_wnd) or (ctrl.Tag = 99) then
        begin
          Cfg[f.Name + '_' + ctrl.Name + '$Top'] := IntToStr(ctrl.Top);
          Cfg[f.Name + '_' + ctrl.Name + '$Left'] := IntToStr(ctrl.Left);
          Cfg[f.Name + '_' + ctrl.Name + '$Width'] := IntToStr(ctrl.Width);
          Cfg[f.Name + '_' + ctrl.Name + '$Height'] := IntToStr(ctrl.Height);
        end;
      end;
    end;


  finally
    //FreeAndNil() ;
  end;
end; (*WriteControlPlacement*)

procedure ReadControlPlacement(f: TForm; only_wnd: boolean = True);
var
  idx: integer;
  ctrl: TControl;
begin
  try
    f.Top := Cfg.AsIntegerDef[f.Name + '$Top', f.Top];
    f.Left := Cfg.AsIntegerDef[f.Name + '$Left', f.Left];
    f.Width := Cfg.AsIntegerDef[f.Name + '$Width', f.Width];
    f.Height := Cfg.AsIntegerDef[f.Name + '$Height', f.Height];

    for idx := 0 to -1 + f.ComponentCount do
    begin
      if f.Components[idx] is TControl then
      begin
        ctrl := TControl(f.Components[idx]);
        if (not only_wnd) or (ctrl.Tag = 99) then
        begin
          ctrl.Top := Cfg.AsIntegerDef[f.Name + '_' + ctrl.Name + '$Top', ctrl.Top];
          ctrl.Left := Cfg.AsIntegerDef[f.Name + '_' + ctrl.Name + '$Left', ctrl.Left];
          ctrl.Width := Cfg.AsIntegerDef[f.Name + '_' + ctrl.Name + '$Width', ctrl.Width];
          ctrl.Height := Cfg.AsIntegerDef[f.Name + '_' + ctrl.Name + '$Height', ctrl.Height];
        end;
      end;
    end;
  finally
    //FreeAndNil() ;
  end;
end; (*ReadControlPlacement*)

procedure PositionCB(const sgGrid: TStringGrid;
  var iTop, iLeft, iHeight, iWidth: integer);
var
  i, iStartRow, iStartCol: integer;
begin
  with sgGrid do
  begin
    iStartRow := TopRow;
    iStartCol := LeftCol;
    iTop := Top;
    iLeft := Left;
    { Compute Row Height }
    for i := iStartRow to Row do
      iTop := iTop + RowHeights[i] + GridLineWidth;

    { Compute Col Width }
    for i := iStartCol to Col - 1 do
      iLeft := iLeft + ColWidths[i] + GridLineWidth;
    if (FixedCols > 0) then
      for i := 0 to FixedCols - 1 do
        iLeft := iLeft + ColWidths[i] + GridLineWidth;

    iTop := iTop + GridLineWidth - (1 * Row);
    iLeft := iLeft + GridLineWidth;

    iHeight := RowHeights[Row] + (GridLineWidth * (Row - iStartRow));
    iWidth := ColWidths[Col];
  end;
end;

function Replace(_src, _from, _to: string): string;
begin
  Result := StringReplace(_src, _from, _to, [rfReplaceAll]);
end;

function ToStringLength(s: string; l: integer): string;
var
  i: integer;
begin
  for i := (l - Length(s)) downto 1 do
    s := s + ' ';
  s := LeftStr(s, l);
  Result := s;
end;

procedure _Log(Text: string);
begin
  Log.Log(Text);
end;

{
procedure ResizeJPEGImage(source_file,dest_file:PAnsiChar;
  width,height:Integer;
  keep_aspect:Boolean=True;
  quality:Integer=90);
var
  ASrcImage: TBitMap32;
  ADstImage: TBitMap32;
  b: TBitMap32;
  j : TJPEGImage;
  new_width, new_height: Integer;
  ext: String;
begin
  ext := LowerCase(ExtractFileExt(source_file));
  if not (ext='.jpg') or not (ext='.jpeg') then Exit;

  j := TJPEGImage.Create;
  j.LoadFromFile(source_file);
  ASrcImage.Assign(j);
  FreeAndNil(j);

  // Original
  ASrcImage := TBitmap32.Create;
  // destintaion
  ADstImage := TBitMap32.Create;

  // calculate new size
  new_width := width;
  new_height := height;
  if (keep_aspect) or (new_width=-1) or (new_height=-1) then begin
     if ((new_width <> -1) and (ASrcImage.Width > ASrcImage.Height)) or (new_height = -1) then begin
        new_height := Floor(ASrcImage.Height * new_width /ASrcImage.Width);
     end else if (new_height <> -1) then begin
         new_width := Floor(ASrcImage.Width * new_height /ASrcImage.Height);
     end;
  end;
  ADstImage.Height := new_height;
  ADstImage.Width := new_width;
  StretchTransfer(ADstImage, ADstImage.BoundsRect, ADstImage.BoundsRect,
       ASrcImage, ASrcImage.BoundsRect, TNearestResampler(0) ,TDrawMode(0));

  b := TBitmap32.Create;
  b.Assign(ADstImage);

  // jpeg
  j := TJPEGImage.Create;
  j.CompressionQuality := quality;
  j.Assign(b);
  j.SaveToFile(dest_file);
  FreeAndNil(j);

  FreeAndNil(b);
  ASrcImage.Free;
  ADstImage.Free;
end;
}
end.
