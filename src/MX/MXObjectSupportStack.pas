{$IFDEF DBG_XOKERNEL}{$D+,L+,O-,W+}{$ELSE}{$D-,L-,O+,W-}{$ENDIF}
{:Optimized routines to simulate stack
@desc Set of routines to emulate stack if you cannot use system's one<br><br>
  <b>Version</b>: <b>0.2.2001.09.26</b><br><br>
  <b>Author</b>: Milosz A. Krajewski (vilge@mud.org.pl)<br><br>
  <b>Advantages</b>:<ul>
    <li>ability to define multiple independent stacks
    <li>speed optimized
    <li>memory optimized (grows if needed)
  </ul>
  <b>Disadvantages</b>:<ul>
    <li>still slower than system's one
  </ul>
  <b>Revision</b>:<ul>
    <li><b>0.1</b> experimental
    <li><b>0.2</b> speed optimizations
  </ul>
  <b>Comments</b><ul>
    <li>This unit is used internally by XObjects, you don't need
    (to be more specific: you shouldn't) use it by yourself
    it it speed-optimized but does not perform any range checking and so on
  </ul>
@todo exponential growth}
unit MXObjectSupportStack;

interface

uses
  SysUtils, Dialogs,
  MXMemory;

const
  //:tag indicating that stack has been initialized
  ss_magictag = $7F2D5A18; // TXOSS.tag should be set to this value if it's initialized
                           // mistake probability: 1/(2^32)
  //:tag indicating that stack has been disabled
  ss_antitag  = $0F0F0F0F; // destroy tag when freeing stack

type
  //:internal stack structure
  TXOSS = record
    tag: Integer;
    idx, top, delta, size: Integer;
    data: PIntMem;
  end;

procedure ss_test_failed();
procedure ss_init(var ss: TXOSS; varsize: Integer);
procedure ss_done(var ss: TXOSS);
function ss_alloc(var ss: TXOSS; c: Integer = 1): Pointer;
function ss_current(var ss: TXOSS; c: Integer = 1): Pointer;
function ss_first(var ss: TXOSS): Pointer;
function ss_count(var ss: TXOSS): Integer;
procedure ss_free(var ss: TXOSS; c: Integer = 1);

implementation
{ *************************************************************************** }
//:Shows error message (internal use only)
procedure ss_test_failed();
begin
  MessageDlg('Stack is not initialized. Fatal error. program halted.', mtError,
             [mbOK], 0);
  Halt;
end;
{-----------------------------------------------------------------------------}
{:Initializes the stack
@desc Initializes the stack. Sets contents to empty, sets delta to 512 and
  adjusts size to 4-byte bounds
@param ss Stack structure
@param varsize Size of stack item
@seealso <see routine="ss_done">}
procedure ss_init(var ss: TXOSS; varsize: Integer);
begin
  with ss do begin
    tag := ss_magictag; idx := -1; top := 0; delta := 512; data := nil;
    size := (varsize + SizeOf(Integer) - 1) div SizeOf(Integer);
    if size <= 0 then size := SizeOf(Integer);
  end;
end;
{-----------------------------------------------------------------------------}
{:Destroys the stack
@desc Frees memory used by the stack and sets tag to <see const="ss_antitag">.
@param ss Stack structure
@seealso <see routine="ss_init">}
procedure ss_done(var ss: TXOSS);
begin
  if ss.data <> nil
    then FreeMem(ss.data);
  ss.tag := ss_antitag;
end;
{-----------------------------------------------------------------------------}
{
// This routine was called from ss_alloc and ss_free but
// due "call" is slow and there is no inline functions i decided to
// embed it into them

procedure ss_adjust(var ss: TXOSS; newTop: Integer);
begin
  // calculate new top as follows:
  // newTop := ((ss.idx + ss.delta) div ss.delta) * ss.delta;
  // realloc := (newTop > ss.top) or
  //   ((newTop < ss.top) and (ss.idx < ss.top - (ss.delta shr 1)));

  // to avoid blinking it's alloceted up when maximum limit is reached
  // but is dealloceted where more what half is not used
  // it could blink, when some proc is called on the edge of stack multiple times

  ReallocMem(ss.data, newTop * ss.size * SizeOf(Integer));
  ss.top := newTop;
end;
}
{-----------------------------------------------------------------------------}
{:Allocs multiple stack slots
@desc Allocs multiple stack slots and returns pointer to first of them
@param ss Stack structure
@param c Slots to alloc (default = 1)
@returns Pointer to first allocated slot
@seealso <see routine="ss_current">
@seealso <see routine="ss_free">}
function ss_alloc(var ss: TXOSS; c: Integer = 1): Pointer;
var
  newTop: Integer;
  i: Integer;
begin
  Inc(ss.idx, c);

  newTop := ((ss.idx + ss.delta) div ss.delta) * ss.delta;
  if (newTop > ss.top) or
     ((newTop < ss.top) and (ss.idx < ss.top - (ss.delta shr 1))) then begin
    ReallocMem(ss.data, newTop * ss.size * SizeOf(Integer));
    ss.top := newTop;
  end;

  Result := @ss.data^[(ss.idx - c + 1)*ss.size];
  // code:
  //   FillChar(Result^, ss.size * c * SizeOf(Integer), 0);
  // has been optimized to:
  for i := 0 to ss.size*c - 1 do PLWMem(Result)^[i] := 0;
end;
{-----------------------------------------------------------------------------}
{:Returns pointer to current top of stack
@desc This routine returns pointer to last item on the stack or pointer to first
  slot of multiple slots allocated on stack
@param ss Stack structure
@param c Number of slots (default 1)
@returns Pointer to last or (if c > 1) to c trailing slots on the stack
@seealso <see routine="ss_alloc">
@seealso <see routine="ss_free">
@seealso <see routine="ss_first">}
function ss_current(var ss: TXOSS; c: Integer = 1): Pointer;
begin
  Result := @ss.data^[(ss.idx - c + 1)*ss.size];
end;
{-----------------------------------------------------------------------------}
{:Returns pointer to first slot on stack
@desc It's usable when threating stack as growing array. Same to
  <see routine="ss_current">(ss, <see routine="ss_count">(ss))
@param ss Stack structure
@returns Pointer to first slot
@seealso <see routine="ss_alloc">
@seealso <see routine="ss_free">
@seealso <see routine="ss_current">
@seealso <see routine="ss_count">}
function ss_first(var ss: TXOSS): Pointer;
begin
  Result := ss.data;
end;
{-----------------------------------------------------------------------------}
{:Number of slots
@desc Returns number of slots currently allocated on stack
@param ss Stack structure
@returns Number of slots allocated
@seealso <see routine="see_alloc">
@seealso <see routine="see_first">}
function ss_count(var ss: TXOSS): Integer;
begin
  Result := ss.idx + 1;
end;
{-----------------------------------------------------------------------------}
{:Free multiple slots
@desc Frees one or multiple slot from stack. If number of slots is less than specified
  procedure will fail and program will be halted
@param ss Stack structure
@param c Number of slots to free (default 1)
@seealso <see routine="ss_alloc">
@seealso <see routine="ss_current">
@seealso <see routine="ss_count">}
procedure ss_free(var ss: TXOSS; c: Integer = 1);
var
  newTop: Integer;
begin
  if ss.idx - c + 1 < 0 then begin
    MessageDlg('Stack currupted. Fatal Error. Index underflow.', mtError, [mbOK], 0);
    Halt;
  end;

  Dec(ss.idx, c);
  newTop := ((ss.idx + ss.delta) div ss.delta) * ss.delta;
  if (newTop > ss.top) or ((newTop < ss.top) and (ss.idx < ss.top - (ss.delta shr 1))) then begin
    ReallocMem(ss.data, newTop * ss.size * SizeOf(Integer));
    ss.top := newTop;
  end;
end;
{ *************************************************************************** }
end.
