unit MXStr; interface

uses
  SysUtils, Math,
  MXMemory;
{***************************************************************************}
const
  CR = #13; LF = #10; CRLF = #13#10;

  del_before = $01;
  del_after  = $02;
  del_global = $04;
  del_double = $08;

  fll_before = $01;
  fll_after  = $02;
  fll_center = $03;
var
  ValResult : Byte = 0;

function StrBytesToHex( var h; i : Integer ) : String;

function StrCount(const s: String; c: Char): Integer;
function StrGetPart( s : String; i : Byte; ch : Char ) : String;
function StrCreate( vx : Integer; vc : Char ) : String;
function StrSpc( x : Integer ) : String;
function StrFillUpSpc( s : String; l, mode : Byte ) : String;
function StrDelSpc( s : String; mode : Byte ) : String;
function StrTab2Spc( s : String; tabs : Byte ) : String;

function StrCharFind( const s, c : String ) : Byte;

function StrFPos(s: String; c: Char): Integer;
function StrLPos(s: String; c: Char): Integer;
function StrNPos(s: String; c: Char; i: Integer): Integer;

function StrR( v : Double; maxlen, x, y : Byte ) : String;
function StrRE( v : Double; maxlen, y: Integer ) : String;
function StrI( v : Longint; maxlen, x : Byte ) : String;
function ValR( s : String ) : Double;
function ValI( s : String ) : LongInt;

function StrAdd0( const vs : String; vx : Integer ) : String;

function IsSpc( vc : Char ) : Boolean;
function IsNumI( const vs : String ) : Boolean;
function IsNumF(const vs: String): Boolean;

procedure StrExpandWithChar( var s : String; ch : Char );

function StrStripQuote(s: String; q: Char): String;
function StrAddTail(const s: String; t: Char): String;
function StrQuote(const s: String; q: Char = ''''): String;
function StrTrimQuote(const s: String; q: Char = ''''): String;
function StrCommonStr(s1, s2: String; Index: Integer = 1): String;

{***************************************************************************}
implementation
{***************************************************************************}
function StrBytesToHex( var h; i : Integer ) : String;
var
  m : PByteMem;

  function hexdigit( b : Byte ) : Char;
  begin
    if b < 10 then Result := Char( b + Byte( '0' ) ) else
    Result := Char( b - 10 + Byte( 'a' ) );
  end;

  function hex( b : Byte ) : String;
  begin
    hex := hexdigit( b shr 4 ) + hexdigit( b and $0f );
  end;

begin
  m := @h;
  Result := '';
  while i > 0 do begin
    Result := Result + hex( m^[ i - 1 ] );
    Dec( i );
  end;
end;
{***************************************************************************}
function StrCount(const s: String; c: Char): Integer;
var
  cnt, add: Integer;
begin
  add := 0;
  for cnt := 1 to Length(s) do if s[cnt] = c then Inc(add);
  StrCount := add
end;
{***************************************************************************}
function StrGetPart(s: String; i: Byte; ch: Char): String;
var
   n, c, z : byte;
begin
  s := StrAddTail(s, ch);
  if i > StrCount(s, ch) then begin
    StrGetPart := ''; exit
  end;
  n := 1; c := 1;
  while c <> i do begin
    if s[n] = ch then Inc(c); Inc(n)
  end;
  z := n;
  while s[z] <> ch do Inc(z);
  StrGetPart := Copy(s, n, z - n)
end;
{***************************************************************************}
function StrCreate( vx : Integer; vc : Char ) : String;
var
  i : Word;
begin
  Result := '';
  if vx < 0 then exit;
  SetLength( Result, vx );
  for i := 1 to vx do Result[ i ] := vc;
end;
{***************************************************************************}
function StrSpc( x : Integer ) : String;
begin
  StrSpc := StrCreate( x, ' ' )
end;
{***************************************************************************}
function StrFillUpSpc( s : String; l, mode : Byte ) : String;
var
  x, ps : Byte;
begin
  if Length( s ) > l then SetLength( s, l );
  x := l - Length( s );
  if mode = fll_center then begin
    Insert( StrSpc( ( x div 2 ) + ( x and $01 ) ), s, Length( s ) + 1 );
    Insert( StrSpc( x div 2 ), s, 1 )
  end else begin
    if mode = fll_after then ps := Length( s ) + 1 else ps := 1;
    Insert( StrSpc( x ), s, ps )
  end;
  StrFillUpSpc := s
end;
{***************************************************************************}
function StrDelSpc( s : String; mode : Byte ) : String;
var
  i : byte;
begin
  if mode and del_before <> 0 then
    while ( Length( s ) > 0 ) and ( IsSpc( s[ 1 ] ) ) do Delete( s, 1, 1 );
  if mode and del_after <> 0 then
    while ( Length( s ) > 0 ) and ( IsSpc( s[ Length( s ) ] ) ) do SetLength( s, Length( s ) - 1 );
  if mode and del_global <> 0 then begin
    i := 1;
    while i <= Length( s ) do
      if IsSpc( s[ i ] ) then Delete( s, i, 1 ) else Inc( i )
  end;
  if mode and del_double <> 0 then begin
    i := 1;
    while i < Length( s ) do
      if ( IsSpc( s[ i ] ) ) and ( IsSpc( s[ i + 1 ] ) ) then Delete( s, i, 1 )
      else Inc( i )
  end;
  StrDelSpc := s
end;
{***************************************************************************}
function StrTab2Spc( s : String; tabs : Byte ) : String;
var
  cnt : Byte;
begin
  cnt := 1;
  while cnt <= Length( s ) do begin
    if s[ cnt ] = #9 then begin
      Delete( s, cnt, 1 );
      Insert( StrSpc( tabs - ( ( cnt - 1 ) mod tabs ) ), s, cnt )
    end;
    Inc( cnt )
  end;
  StrTab2Spc := s
end;
{***************************************************************************}
function StrCharFind( const s, c : String ) : Byte;
var
  n, p : Byte;
begin
  p := 0;
  for n := 1 to Length( s ) do begin
    p := StrFPos( c, s[ n ] );
    if p <> 0 then break;
  end;
  Result := p
end;
{***************************************************************************}
function StrR( v : Double; maxlen, x, y : Byte ) : String;
var
  s : String;
begin
  if ( x = 0 ) and ( y = 0 ) then Str( v, s ) else Str( v : x : y, s );
  if ( maxlen <> 0 ) and ( Length( s ) > maxlen ) then Str( v : maxlen, s );
  StrR := s
end;
{***************************************************************************}
function StrRE( v : Double; maxlen, y: Integer ) : String;
var
  s: String;
begin
  s := StrR(v, 255, 0, y);
  if StrFPos(s, '.') <> 0 then begin
    while s[Length(s)] = '0' do SetLength(s, Length(s) - 1);
    if s[Length(s)] = '.' then SetLength(s, Length(s) - 1);
  end;
  if ( maxlen <> 0 ) and ( Length( s ) > maxlen ) then Str( v : maxlen, s );
  Result := s;
end;
{***************************************************************************}
function StrI( v : Longint; maxlen, x : Byte ) : String;
var
  s : String;
begin
  if x = 0 then Str( v, s ) else Str( v : x, s );
  if ( maxlen <> 0 ) and ( Length( s ) > maxlen ) then s := StrCreate( maxlen, '#' );
  StrI := s
end;
{***************************************************************************}
function ValR( s : String ) : Double;
var
  num  : Double;
  test : Integer;
begin
  Val( s, num, test );
  ValResult := test;
  ValR := num
end;
{***************************************************************************}
function ValI( s : String ) : LongInt;
var
  pos  : Byte;
  num  : LongInt;
  test : Integer;
begin
  pos := StrFPos( s, '.' );
  if pos <> 0 then SetLength( s, pos - 1 );
  Val( s, num, test );
  ValResult := test;
  ValI := num
end;
{***************************************************************************}
function StrAdd0( const vs : String; vx : Integer ) : String;
begin
  Result := StrCreate( vx - Length( vs ), '0' ) + vs;
end;
{***************************************************************************}
function IsSpc( vc : Char ) : Boolean;
begin
  Result := (vc = ' ') or (vc = #9) or (vc = #10) or (vc = #13);
end;
{***************************************************************************}
function IsNumI( const vs : String ) : Boolean;
var
  i : Word;
begin
  Result := True;
  for i := 1 to Length( vs ) do begin
    if( vs[ i ] < '0' ) or ( vs[ i ] > '9' ) then begin
      Result := False;
      exit
    end;
  end;
end;
{***************************************************************************}
function IsNumF(const vs: String): Boolean;
begin
  Result := True;
  try StrToFloat(vs); except Result := False; end;
end;
{***************************************************************************}
function StrFPos(s: String; c: Char): Integer;
var
  loop: Integer;
begin
  loop := 1;
  while ( loop < Length( s ) ) and ( s[ loop ] <> c ) do Inc( loop );
  if ( loop <= Length( s ) ) and ( s[ loop ] = c ) then
    StrFPos := loop else StrFPos := 0
end;
{***************************************************************************}
function StrLPos(s: String; c: Char): Integer;
var
  loop: Integer;
begin
  loop := Length( s );
  while ( loop > 0 ) and ( s[ loop ] <> c ) do Dec( loop );
  if s[ loop ] = c then StrLPos := loop else StrLPos := 0
end;
{***************************************************************************}
function StrNPos(s: String; c: Char; i: Integer): Integer;
var
  loop: Integer;
begin
  if i > Length( s ) then begin
     StrNPos := 0; exit
  end;
  loop := i;
  while ( loop < Length( s ) ) and ( s[ loop ] <> c ) do Inc( loop );
  if s[ loop ] = c then StrNPos := loop else StrNPos := 0
end;
{***************************************************************************}
procedure StrExpandWithChar( var s : String; ch : Char );
begin
  if (s = '') or (s[ Length( s ) ] <> ch) then s := s + ch;
end;
{***************************************************************************}
function StrStripQuote(s: String; q: Char): String;
var
  p: Integer;
begin
  Result := '';
  if s = '' then exit;
  if (s[1] = q) and (s[Length(s)] = q) then s := Copy(s, 2, Length(s) - 2);
  repeat
    p := Pos(q + q, s);
    if p <> 0 then Delete(s, p, 2);
  until p = 0;

  Result := s;
end;
{***************************************************************************}
function StrQuote(const s: String; q: Char = ''''): String;
begin Result := AnsiQuotedStr(s, q); end;
{***************************************************************************}
function StrTrimQuote(const s: String; q: Char = ''''): String;
begin Result := StrQuote(Trim(s), q); end;
{***************************************************************************}
function StrAddTail(const s: String; t: Char): String;
begin
  Result := s; if Result = '' then exit;
  if s[Length(s)] <> t then Result := Result + t;
end;
{***************************************************************************}
function StrCommonStr(s1, s2: String; Index: Integer = 1): String;
var
  i: Integer;
begin
  Result := '';
  for i := Index to Min(Length(s1), Length(s2)) do begin
    if s1[i] = s2[i] then Result := Result + s1[i]
    else break;
  end;
end;
{***************************************************************************}
end.
