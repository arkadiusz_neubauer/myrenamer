unit MXCompare;
{ ****************************************************************************
Purpose:
  Abstract comparing class
Version (major.minor.year.month.day):
  1.1.2001.09.13
Author:
  Milosz A. Krajewski <vilge@mud.org.pl>
Capabilities:
  + xref object compatibile
  + compare two items (abstract)
  + estimate item value (abstract)
  + hash value (abstract)
  + filter items (for list/tables/set and so on)
  + iterate (ie: forEach)
Todo:
  - nothing
***************************************************************************** }
interface

uses
  SysUtils,
  MXMemory, MXObject;
{ *************************************************************************** }
type
  XObjectCompare = class(XObject)
  protected
    bInverted: Boolean;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    procedure v_load(s: XStream); override;
    procedure v_store(s: XStream); override;
    function v_getAsString: String; override;

    // ---- from XObjectCompare ----
    function v_compare(aItem, bItem: XObject): Integer; virtual;
    function v_estimate(aItem: XObject): Double; virtual;
    function v_hash(aItem: XObject): Integer; virtual;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XObjectCompare;
    function iref: XObjectCompare;
    function setref(var v): XObjectCompare;
    function freeze(p: Boolean = True): XObjectCompare;

    function new: XObjectCompare;
    function dup: XObjectCompare;
    function copy(v: XObjectCompare): XObjectCompare;
    function swap(v: XObjectCompare): XObjectCompare;
    function tmp: XObjectCompare;

    // ---- from XCompareObject ----
    function compare(aItem, bItem: XObject): Integer;
    function estimate(aItem: XObject): Double;
    function hash(aItem: XObject): Integer;

    function isInverted: Boolean;
    function setInverted(i: Boolean = True): XObjectCompare;
  end;

  XObjectFilter = class(XObject)
  protected
    bInverted: Boolean;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    procedure v_load(s: XStream); override;
    procedure v_store(s: XStream); override;
    function v_getAsString: String; override;

    // ---- from XObjectFilter ----
    function v_filter(v: XObject): Boolean; virtual;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XObjectFilter;
    function iref: XObjectFilter;
    function setref(var v): XObjectFilter;
    function freeze(p: Boolean = True): XObjectFilter;

    function new: XObjectFilter;
    function dup: XObjectFilter;
    function copy(v: XObjectFilter): XObjectFilter;
    function swap(v: XObjectFilter): XObjectFilter;
    function tmp: XObjectFilter;

    // ---- from XObjectFilter ----
    function filter(v: XObject): Boolean;

    function isInverted: Boolean;
    function setInverted(f: Boolean = True): XObjectFilter;
  end;


  XObjectIterator = class(XObject)
  protected
    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;

    // ---- from XObjectIterator ----
    procedure v_action(o: XObject); virtual;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XObjectIterator;
    function iref: XObjectIterator;
    function setref(var v): XObjectIterator;
    function freeze(p: Boolean = True): XObjectIterator;

    function new: XObjectIterator;
    function dup: XObjectIterator;
    function copy(v: XObjectIterator): XObjectIterator;
    function swap(v: XObjectIterator): XObjectIterator;
    function tmp: XObjectIterator;

    // ---- from XObjectIterator ----
    function action(o: XObject): XObject;
  end;

const
  XObjectCompare_Magic  = $1000;
  XObjectFilter_Magic   = $1001;
  XObjectIterator_Magic = $1002;

implementation
{ *************************************************************************** }
// ---- from XObject ----
// do not change anything in these definitions - begin
function XObjectCompare.iref: XObjectCompare; begin Result := XObjectCompare(XObject(Self).iref); end;
function XObjectCompare.dref(aFree: Boolean = True): XObjectCompare; begin Result := XObjectCompare(XObject(Self).dref(aFree)); end;
function XObjectCompare.setref(var v): XObjectCompare; begin Result := XObjectCompare(XObject(Self).setref(v)); end;
function XObjectCompare.freeze(p: Boolean = True): XObjectCompare; begin Result := XObjectCompare(XObject(Self).freeze(p)); end;
function XObjectCompare.new: XObjectCompare; begin Result := XObjectCompare(XObject(Self).new); end;
function XObjectCompare.dup: XObjectCompare; begin Result := XObjectCompare(XObject(Self).dup); end;
function XObjectCompare.copy(v: XObjectCompare): XObjectCompare; begin Result := XObjectCompare(XObject(Self).copy(v)); end;
function XObjectCompare.swap(v: XObjectCompare): XObjectCompare; begin Result := XObjectCompare(XObject(Self).swap(v)); end;
function XObjectCompare.tmp: XObjectCompare; begin Result := XObjectCompare(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XObjectCompare.v_class_magic: Integer; begin Result := XObjectCompare_Magic; end;
class function XObjectCompare.v_class_create: XObject; begin Result := XObjectCompare.Create; end;
class function XObjectCompare.v_class_name: String; begin Result := 'XObjectCompare'; end;
class function XObjectCompare.v_class_self: XObjectClass; begin Result := Self; end;
class function XObjectCompare.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XObjectCompare.v_load(s: XStream);
begin
  try
    xrbegin([@s], []);
    inherited v_load(s);
    bInverted := s.readBool;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XObjectCompare.v_store(s: XStream);
begin
  try
    xrbegin([@s], []);
    inherited v_store(s);
    s.writeBool(bInverted);
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XObjectCompare.v_copy(v: XObject);
var
  o: XObjectCompare absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  bInverted := o.bInverted;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XObjectCompare.v_swap(v: XObject);
var
  o: XObjectCompare absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(bInverted, o.bInverted, SizeOf(bInverted));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.v_getAsString: String;
begin
  xrbegin([], []);
  Result := inherited v_getAsString;
  Result := Format('%s, isInverted:%s', [Result, BooleanDesc[bInverted]]);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.v_compare(aItem, bItem: XObject): Integer;
begin
  xrPbegin([@aItem, @bItem]);
  Result := 0;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.v_estimate(aItem: XObject): Double;
begin
  xrPbegin([@aItem]);
  Result := 0;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.v_hash(aItem: XObject): Integer;
begin
  xrPbegin([@aItem]);
  Result := 0;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.compare(aItem, bItem: XObject): Integer;
begin
  xrPbegin([@aItem, @bItem]);
  Result := v_compare(aItem, bItem);
  if bInverted then Result := -Result;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.estimate(aItem: XObject): Double;
begin
  xrPbegin([@aItem]);
  Result := v_estimate(aItem);
  if bInverted then Result := -Result;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.hash(aItem: XObject): Integer;
begin
  xrPbegin([@aItem]); Result := v_hash(aItem); xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.isInverted: Boolean;
begin
  xrFbegin(); Result := bInverted; xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectCompare.setInverted(i: Boolean = True): XObjectCompare;
begin
  xrFbegin(@Result).setref(Result); bInverted := i; xrend;
end;
{ *************************************************************************** }
// ---- from XObject ----
// do not change anything in these definitions - begin
function XObjectFilter.iref: XObjectFilter; begin Result := XObjectFilter(XObject(Self).iref); end;
function XObjectFilter.dref(aFree: Boolean = True): XObjectFilter; begin Result := XObjectFilter(XObject(Self).dref(aFree)); end;
function XObjectFilter.setref(var v): XObjectFilter; begin Result := XObjectFilter(XObject(Self).setref(v)); end;
function XObjectFilter.freeze(p: Boolean = True): XObjectFilter; begin Result := XObjectFilter(XObject(Self).freeze(p)); end;
function XObjectFilter.new: XObjectFilter; begin Result := XObjectFilter(XObject(Self).new); end;
function XObjectFilter.dup: XObjectFilter; begin Result := XObjectFilter(XObject(Self).dup); end;
function XObjectFilter.copy(v: XObjectFilter): XObjectFilter; begin Result := XObjectFilter(XObject(Self).copy(v)); end;
function XObjectFilter.swap(v: XObjectFilter): XObjectFilter; begin Result := XObjectFilter(XObject(Self).swap(v)); end;
function XObjectFilter.tmp: XObjectFilter; begin Result := XObjectFilter(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XObjectFilter.v_class_magic: Integer; begin Result := XObjectFilter_Magic; end;
class function XObjectFilter.v_class_create: XObject; begin Result := XObjectFilter.Create; end;
class function XObjectFilter.v_class_name: String; begin Result := 'XObjectFilter'; end;
class function XObjectFilter.v_class_self: XObjectClass; begin Result := Self; end;
class function XObjectFilter.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XObjectFilter.v_load(s: XStream);
begin
  try
    xrbegin([@s], []);
    inherited v_load(s);
    bInverted := s.readBool;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XObjectFilter.v_store(s: XStream);
begin
  try
    xrbegin([@s], []);
    inherited v_store(s);
    s.writeBool(bInverted);
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XObjectFilter.v_copy(v: XObject);
var
  o: XObjectFilter absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  bInverted := o.bInverted;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XObjectFilter.v_swap(v: XObject);
var
  o: XObjectFilter absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(bInverted, o.bInverted, SizeOf(bInverted));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectFilter.v_getAsString: String;
begin
  xrbegin([], []);
  Result := inherited v_getAsString;
  Result := Format('%s, isInverted:%s', [Result, BooleanDesc[bInverted]]);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectFilter.v_filter(v: XObject): Boolean;
begin
  xrPbegin([@v]); Result := True; xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectFilter.filter(v: XObject): Boolean;
begin
  xrPbegin([@v]); Result := v_filter(v) xor bInverted; xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectFilter.isInverted: Boolean;
begin
  xrFbegin(); Result := bInverted; xrend;
end;
{-----------------------------------------------------------------------------}
function XObjectFilter.setInverted(f: Boolean = True): XObjectFilter;
begin
  xrFbegin(@Result).setref(Result); bInverted := f; xrend;
end;
{ *************************************************************************** }
// do not change anything in these definitions - begin
function XObjectIterator.iref: XObjectIterator; begin Result := XObjectIterator(XObject(Self).iref); end;
function XObjectIterator.dref(aFree: Boolean = True): XObjectIterator; begin Result := XObjectIterator(XObject(Self).dref(aFree)); end;
function XObjectIterator.setref(var v): XObjectIterator; begin Result := XObjectIterator(XObject(Self).setref(v)); end;
function XObjectIterator.freeze(p: Boolean = True): XObjectIterator; begin Result := XObjectIterator(XObject(Self).freeze(p)); end;
function XObjectIterator.new: XObjectIterator; begin Result := XObjectIterator(XObject(Self).new); end;
function XObjectIterator.dup: XObjectIterator; begin Result := XObjectIterator(XObject(Self).dup); end;
function XObjectIterator.copy(v: XObjectIterator): XObjectIterator; begin Result := XObjectIterator(XObject(Self).copy(v)); end;
function XObjectIterator.swap(v: XObjectIterator): XObjectIterator; begin Result := XObjectIterator(XObject(Self).swap(v)); end;
function XObjectIterator.tmp: XObjectIterator; begin Result := XObjectIterator(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XObjectIterator.v_class_magic: Integer; begin Result := XObjectIterator_Magic; end;
class function XObjectIterator.v_class_create: XObject; begin Result := XObjectIterator.Create; end;
class function XObjectIterator.v_class_name: String; begin Result := 'XObjectIterator'; end;
class function XObjectIterator.v_class_self: XObjectClass; begin Result := Self; end;
class function XObjectIterator.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
// ---- from XObjectIterator ----
procedure XObjectIterator.v_action(o: XObject);
begin
  o.fin;
  // here should be:
  //   xrPbegin([@p]); xrend;
  // but this way is faster
end;
{-----------------------------------------------------------------------------}
function XObjectIterator.action(o: XObject): XObject;
begin
  xrPbegin([@o], @Result);
  v_action(o);
  o.setref(Result);
  xrend;
end;
{ *************************************************************************** }
initialization
  registerMagic(XObjectCompare);
  registerMagic(XObjectFilter);
  registerMagic(XObjectIterator);
{ *************************************************************************** }
end.
