unit MxCfgLog;
interface

uses
  SysUtils,
  IniFiles, Variants,
  MXStr;

type
  ELogException = class(Exception)
    constructor Create(const Msg: String);
  end;

  EInternalError = class(ELogException);

  TConfigFile = class(TObject)
  private
    FIni: TIniFile;
    FFileName: String;
    FSection: String;

    procedure SplitName(n: String; var s, i: String);
    function KeyExistsSI(s, i: String): Boolean;
  public
    constructor Create(AFileName: String);
    destructor Destroy; override;

    function KeyExists(n: String): Boolean;
    function getIni: TIniFile;

    function GetAsStringDef(n: String; d: String): String;
    procedure SetAsString(n: String; v: String);
    function GetAsString(n: String): String;

    function GetAsIntegerDef(n: String; d: Integer): Integer;
    procedure SetAsInteger(n: String; v: Integer);
    function GetAsInteger(n: String): Integer;

    function GetAsDoubleDef(n: String; d: Double): Double;
    procedure SetAsDouble(n: String; v: Double);
    function GetAsDouble(n: String): Double;

    function GetAsBoolDef(n: String; v: Boolean): Boolean;
    procedure SetAsBool(n: String; v: Boolean);
    function GetAsBool(n: String): Boolean;

    procedure DeleteKey(n: String);
    procedure DeleteSection(s: String);

    property Ini: TIniFile read FIni;
    property FileName: String read FFileName;
    property Section: String read FSection write FSection;

    property AsString[n: String]: String read GetAsString write SetAsString; default;
    property AsInteger[n: String]: Integer read GetAsInteger write SetAsInteger;
    property AsDouble[n: String]: Double read GetAsDouble write SetAsDouble;
    property AsBool[n: String]: Boolean read GetAsBool write SetAsBool;

    property AsStringDef[n: String; d: String]: String read GetAsStringDef;
    property AsIntegerDef[n: String; d: Integer]: Integer read GetAsIntegerDef;
    property AsDoubleDef[n: String; d: Double]: Double read GetAsDoubleDef;
    property AsBoolDef[n: String; d: Boolean]: Boolean read GetAsBoolDef;

  end;

  TLogFile = class( TObject )
  private
    LogName : String;
    LogFile : Text;
  public
    constructor Create(const AFileName: String);
    procedure Log(const msg: String);
    function  LogT(const msg: String; val: Variant): Variant;
  end;

var
  Log: TLogFile;
  Cfg: TConfigFile;
  ProgPath : String;

implementation
{***************************************************************************}
constructor ELogException.Create(const Msg: String);
begin
  Log.Log(Msg);
  inherited;
end;
{***************************************************************************}
constructor TConfigFile.Create(AFileName: String);
begin
  inherited Create;
  FIni := TIniFile.Create(AFileName);
  FFileName := AFileName;
  FSection := '';
end;
{---------------------------------------------------------------------------}
destructor TConfigFile.Destroy;
begin
  FreeAndNil(FIni);
  inherited;
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.SplitName(n: String; var s, i: String);
var
  p: Integer;
begin
  p := StrFPos(n, '$');

  if p = 0 then begin
    s := FSection; i := n;
  end else begin
    s := Copy(n, 1, p - 1); i := Copy(n, p + 1, MaxInt);
  end;
  if s = '' then s := ExtractFileName(FFileName);
end;
{---------------------------------------------------------------------------}
function TConfigFile.KeyExists(n: String): Boolean;
var
  s, i: String;
begin
  SplitName(n, s, i); Result := KeyExistsSI(s, i);
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.DeleteKey(n: String);
var
  s, i: String;
begin
  SplitName(n, s, i); Ini.DeleteKey(s, i);
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.DeleteSection(s: String);
begin
  Ini.EraseSection(s);
end;
{---------------------------------------------------------------------------}
function TConfigFile.KeyExistsSI(s, i: String): Boolean;
const
  key1 = '<empty>'; key2 = '<nosuchkey>';
begin
  Result := not((FIni.ReadString(s, i, key1) = key1) and (FIni.ReadString(s, i, key2) = key2));
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsStringDef(n: String; d: String): String;
var
  s, i: String;
begin
  SplitName(n, s, i);
  if KeyExistsSI(s, i) then Result := FIni.ReadString(s, i, d)
  else begin Result := d; FIni.WriteString(s, i, d); end;
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.SetAsString(n: String; v: String);
var
  s, i: String;
begin
  SplitName(n, s, i); FIni.WriteString(s, i, v);
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsString(n: String): String;
var
  s, i: String;
begin
  SplitName(n, s, i); Result := FIni.ReadString(s, i, '');
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsIntegerDef(n: String; d: Integer): Integer;
var
  s, i: String;
begin
  SplitName(n, s, i);
  if KeyExistsSI(s, i) then Result := FIni.ReadInteger(s, i, d)
  else begin Result := d; FIni.WriteInteger(s, i, d); end;
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.SetAsInteger(n: String; v: Integer);
var
  s, i: String;
begin
  SplitName(n, s, i); FIni.WriteInteger(s, i, v);
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsInteger(n: String): Integer;
var
  s, i: String;
begin
  SplitName(n, s, i); Result := FIni.ReadInteger(s, i, 0);
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsDoubleDef(n: String; d: Double): Double;
var
  s, i, v: String;
  reset: Boolean;
begin
  SplitName(n, s, i); v := FloatToStr(d);

  Result := 0; reset := True;

  if KeyExistsSI(s, i) then begin
    try
      Result := StrToFloat(FIni.ReadString(s, i, v));
      reset := False;
    except ;
    end;
  end;

  if reset then begin Result := d; FIni.WriteString(s, i, v); end;
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.SetAsDouble(n: String; v: Double);
var
  s, i, vs: String;
begin
  vs := FloatToStr(v); SplitName(n, s, i); FIni.WriteString(s, i, vs);
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsDouble(n: String): Double;
var
  s, i, vs: String;
begin
  SplitName(n, s, i); vs := FIni.ReadString(s, i, '0');
  Result := 0;
  try
    Result := StrToFloat(vs);
  except ;
  end;
end;
function TConfigFile.GetAsBoolDef(n: String; v: Boolean): Boolean;
var
  s, i: String;
  reset: Boolean;
begin
  SplitName(n, s, i);

  Result := false; reset := True;

  if KeyExistsSI(s, i) then begin
    try
      Result := FIni.ReadBool(s, i, v);
      reset := False;
    except ;
    end;
  end;

  if reset then begin Result := v; FIni.WriteBool(s, i, v); end;
end;
{---------------------------------------------------------------------------}
procedure TConfigFile.SetAsBool(n: String; v: Boolean);
var
  s, i: String;
begin
     SplitName(n, s, i); FIni.WriteBool(s, i, v);
end;
{---------------------------------------------------------------------------}
function TConfigFile.GetAsBool(n: String): Boolean;
var
  s, i: String;
begin
  SplitName(n, s, i);
  try
     Result := FIni.ReadBool(s, i, false);
  except ;
    Result := False;
  end;
end;
function TConfigFile.getIni: TIniFile;
begin
  Result := FIni;
end;
{***************************************************************************}
constructor TLogFile.Create(const AFileName: String);
begin
  inherited Create;
  LogName := AFileName;
  Assign(LogFile, LogName);
end;
{---------------------------------------------------------------------------}
procedure TLogFile.Log(const msg: String);
begin
  try
    Append(LogFile);
  except
    on EInOutError do Rewrite(LogFile);
  end;
//  {$IFDEF DEBUG}SendDebug(msg);{$ENDIF}
  Write(LogFile, Format('%s: %s'#13#10, [DateTimeToStr(Now), msg]));
  Close(LogFile);
end;
{---------------------------------------------------------------------------}
function TLogFile.LogT(const msg: String; val: Variant): Variant;
begin
  Log(msg + ': ' + VarAsType(val, varString)); Result := val;
end;
{***************************************************************************}
initialization
  Log      := TLogFile.Create(LowerCase(ChangeFileExt(ParamStr(0), '.log')));
  Cfg      := TConfigFile.Create(LowerCase(ChangeFileExt(ParamStr(0), '.ini')));
  ProgPath := StrAddTail(ExtractFilePath(ParamStr(0)), '\');

finalization
  FreeAndNil(Log);
  FreeAndNil(Cfg);
end.