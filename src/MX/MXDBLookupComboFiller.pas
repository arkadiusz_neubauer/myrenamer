unit MXDBLookupComboFiller;
{ ****************************************************************************
Template:
  MXObject 2001.06.20
***************************************************************************** }
interface

uses
  SysUtils, RxQuery, StdCtrls, DbTables, Classes, lucombo,
  MXMemory, MXObject, MXTimers;
{ *************************************************************************** }
type
  XDBLookupComboFiller = class(XObject)
  protected
    oDatabase: TDatabase;

    bActive: Boolean;
    bChanged: Boolean;
    sSQL: String;
    fLastTime: TDateTime;
    fTimeDiff: Double;
    iCharLimit: Integer;
    sLastText: String;

    oCombo: TLUCombo;
    timer: Integer;

    handlerEnter, handlerExit, handlerChange: TNotifyEvent;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    function v_getAsString: String; override;

    // ---- from XDBLookupComboFiller -----
    procedure p_handlerEnter(Sender: TObject);
    procedure p_handlerExit(Sender: TObject);
    procedure p_handlerChange(Sender: TObject);
    procedure p_handlerTimer(Sender: TObject);
  public
    // ---- from XDBLookupComboFiller -----
    constructor Create;
    destructor Destroy; override;

    // ---- from XObject ----
    function dref(aFree: Boolean = True): XDBLookupComboFiller;
    function iref: XDBLookupComboFiller;
    function setref(var v): XDBLookupComboFiller;
    function freeze(p: Boolean = True): XDBLookupComboFiller;

    function new: XDBLookupComboFiller;
    function dup: XDBLookupComboFiller;
    function copy(v: XDBLookupComboFiller): XDBLookupComboFiller;
    function swap(v: XDBLookupComboFiller): XDBLookupComboFiller;
    function tmp: XDBLookupComboFiller;

    // ---- from XDBLookupComboFiller -----
    function setDatabase(db: TDatabase): XDBLookupComboFiller;
    function getDatabase: TDatabase;
    function setLookupSQL(const sql: String): XDBLookupComboFiller;
    function getLookupSQL: String;
    function setDelay(delay: Double): XDBLookupComboFiller;
    function getDelay: Double;
    function setCharLimit(limit: Integer): XDBLookupComboFiller;
    function getCharLimit: Integer;

    function allocTimer(interval: Integer = 250): XDBLookupComboFiller;
    function releaseTimer: XDBLookupComboFiller;
    function suspendTimer: XDBLookupComboFiller;
    function resumeTimer: XDBLookupComboFiller;
    function isActiveTimer: Boolean;
    function isAllocatedTimer: Boolean;

    procedure notifyEnter;
    procedure notifyExit;
    procedure notifyChange(const text: String); overload;
    procedure notifyChange(combo: TLUCombo); overload;
    function notifyIdle(combo: TLUCombo; aForce: Boolean = False): Boolean;

    function hook(combo: TLUCombo): XDBLookupComboFiller;
    function force(combo: TLUCombo = nil): XDBLookupComboFiller;
  end;

const
  XDBLookupComboFiller_Magic = -1;

implementation
{ *************************************************************************** }
// ---- from XDBLookupComboFiller ----
constructor XDBLookupComboFiller.Create;
begin
  xrFcreate;
  inherited;
  fLastTime := Now;
  fTimeDiff := 1 / SecsPerDay;
  iCharLimit := 4;
  timer := -1;
  xrend;
end;
{-----------------------------------------------------------------------------}
destructor XDBLookupComboFiller.Destroy;
begin
  xrFdestroy;
  releaseTimer;
  inherited;
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XObject ----
// do not change anything in these definitions - begin
function XDBLookupComboFiller.iref: XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).iref); end;
function XDBLookupComboFiller.dref(aFree: Boolean = True): XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).dref(aFree)); end;
function XDBLookupComboFiller.setref(var v): XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).setref(v)); end;
function XDBLookupComboFiller.freeze(p: Boolean = True): XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).freeze(p)); end;
function XDBLookupComboFiller.new: XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).new); end;
function XDBLookupComboFiller.dup: XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).dup); end;
function XDBLookupComboFiller.copy(v: XDBLookupComboFiller): XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).copy(v)); end;
function XDBLookupComboFiller.swap(v: XDBLookupComboFiller): XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).swap(v)); end;
function XDBLookupComboFiller.tmp: XDBLookupComboFiller; begin Result := XDBLookupComboFiller(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XDBLookupComboFiller.v_class_magic: Integer; begin Result := XDBLookupComboFiller_Magic; end;
class function XDBLookupComboFiller.v_class_create: XObject; begin Result := XDBLookupComboFiller.Create; end;
class function XDBLookupComboFiller.v_class_name: String; begin Result := 'XDBLookupComboFiller'; end;
class function XDBLookupComboFiller.v_class_self: XObjectClass; begin Result := Self; end;
class function XDBLookupComboFiller.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.v_copy(v: XObject);
// you can override this, but you can also delete this definition
// if there is no new fields to copy
var
  o: XDBLookupComboFiller absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  oDatabase := o.oDatabase;
  bActive := o.bActive;
  bChanged := o.bChanged;
  sSQL := o.sSQL;
  fLastTime := o.fLastTime;
  fTimeDiff := o.fTimeDiff;
  sLastText := o.sLastText;
  iCharLimit := o.iCharLimit;

  // these field are not copied
  // oCombo
  // timer
  // handlerEnter, handlerExit, handlerChange

  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.v_swap(v: XObject);
var
  o: XDBLookupComboFiller absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(bActive, o.bActive, SizeOf(bActive));
  memSwap(bChanged, o.bChanged, SizeOf(bChanged));
  memSwap(iCharLimit, o.iCharLimit, SizeOf(iCharLimit));
  memSwap(oDatabase, o.oDatabase, SizeOf(oDatabase));
  memSwapString(sSQL, o.sSQL);
  memSwap(fLastTime, o.fLastTime, SizeOf(fLastTime));
  memSwap(fTimeDiff, o.fTimeDiff, SizeOf(fTimeDiff));
  memSwapString(sLastText, o.sLastText);

  memSwap(oCombo, o.oCombo, SizeOf(oCombo));
  memSwap(timer, o.timer, SizeOf(timer));
  { TODO -clater : swap handlers }
  // memSwap(handlerEnter, o.handlerEnter, SizeOf(handlerEnter));
  // memSwap(handlerExit, o.handlerExit, SizeOf(handlerExit));
  // memSwap(handlerChange, o.handlerChange, SizeOf(handlerChange));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.v_getAsString: String;
begin
  xrbegin([], []);
  Result := inherited v_getAsString;
  Result := Format('%s, SQL:[%s], timer:%d', [Result, sSQL, timer]);
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XDBLookupComboFiller -----
function XDBLookupComboFiller.setDatabase(db: TDatabase): XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  oDatabase := db;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.getDatabase: TDatabase;
begin
  xrFbegin; Result := oDatabase; xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.setLookupSQL(const sql: String): XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result); sSQL := sql; xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.getLookupSQL: String;
begin
  xrFbegin; Result := sSQL; xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.setDelay(delay: Double): XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if delay <= 0 then delay := 0.001;
  fTimeDiff := delay / SecsPerDay;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.getDelay: Double;
begin
  xrFbegin;
  Result := fTimeDiff * SecsPerDay;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.setCharLimit(limit: Integer): XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if limit < 0 then limit := 0;
  iCharLimit := limit;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.getCharLimit: Integer;
begin
  xrFbegin;
  Result := iCharLimit;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.notifyEnter;
begin
  xrFbegin; bActive := True; fLastTime := Now; xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.notifyExit;
begin
  xrFbegin; bActive := False; xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.notifyChange(const text: String);
begin
  xrFbegin;
  if bActive then begin
    fLastTime := Now; sLastText := text; bChanged := True;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.notifyChange(combo: TLUCombo);
begin
  xrFbegin;
  if bActive then begin
    fLastTime := Now;
    sLastText := System.Copy(combo.Text, 1, combo.SelStart);
    bChanged := True;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.notifyIdle(combo: TLUCombo; aForce: Boolean = False): Boolean;
var
  q: TRXQuery;
  l: TStringList;
  ss: Integer;
begin
  try
    xrFbegin;
    Result := False;

    if (aForce) or ((bActive) and ((bChanged) or ((combo.Items.Count = 0) and (iCharLimit = 0))) and
    (Now >= fLastTime + fTimeDiff) and (Length(sLastText) >= iCharLimit)) then begin
      bChanged := False;

      fLastTime := Now;

      // creating query
      q := TRxQuery.Create(nil);
      if oDatabase <> nil then q.DatabaseName := oDatabase.DatabaseName;
      q.SQL.Text := Format(sSQL, [sLastText]);

      // gathering
      q.Open; q.First;
      l := TStringList.Create;
      while not q.Eof do begin l.Add(q.Fields[0].AsString); q.Next; end;
      q.Close; q.Free;

      // assign phase
      if (l.Count > 0) and (combo <> nil) then begin
        ss := combo.SelStart;
        combo.Items.Clear;
        combo.Items.Assign(l);
        combo.Text := l[0];
        combo.SelStart := ss; combo.SelLength := Length(combo.Text) - ss;
        Result := True;
      end;

      l.Free;
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.p_handlerEnter(Sender: TObject);
begin
  xrFbegin;
  if Assigned(handlerEnter) then handlerEnter(Sender);
  if Sender is TLUCombo then notifyEnter;
  resumeTimer;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.p_handlerExit(Sender: TObject);
begin
  xrFbegin;
  if Assigned(handlerExit) then handlerExit(Sender);
  if Sender is TLUCombo then notifyEnter;
  suspendTimer;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.p_handlerChange(Sender: TObject);
begin
  xrFbegin;
  if Assigned(handlerChange) then handlerChange(Sender);
  if Sender is TLUCombo then notifyChange(Sender as TLUCombo);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller.p_handlerTimer(Sender: TObject);
begin
  if oCombo = nil then exit;
  xrFbegin; notifyIdle(oCombo); xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.allocTimer(interval: Integer = 250): XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then defaultTimers.releaseTimer(timer);
  timer := defaultTimers.allocTimer(interval, p_handlerTimer, False);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.releaseTimer: XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then begin
    defaultTimers.releaseTimer(timer);
    timer := -1;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.suspendTimer: XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then defaultTimers.suspendTimer(timer);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.resumeTimer: XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then defaultTimers.resumeTimer(timer);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.isActiveTimer: Boolean;
begin
  xrFbegin;
  if timer = -1 then
    Result := False
  else begin
    Result := defaultTimers.isActiveTimer(timer);
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.isAllocatedTimer: Boolean;
begin
  xrFbegin;
  Result := (timer <> -1) and (defaultTimers.isAllocatedTimer(timer));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.hook(combo: TLUCombo): XDBLookupComboFiller;
var
  cname: String;
begin
  try
    xrFbegin(@Result).setref(Result);

    oCombo := combo;

    if combo = nil then exit;
    cname := combo.Name;
    if cname = '' then cname := '<noname>';
    if (Assigned(handlerEnter)) or (Assigned(handlerExit)) or (Assigned(handlerChange)) then
      xrraise(Exception.CreateFmt('Cannot hook events on %s. Already hooked.', [cname]));

    handlerEnter := combo.OnEnter; combo.OnEnter := p_handlerEnter;
    handlerExit := combo.OnExit; combo.OnExit := p_handlerExit;
    handlerChange := combo.OnChange; combo.OnChange := p_handlerChange;

    if isAllocatedTimer then resumeTimer;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller.force(combo: TLUCombo = nil): XDBLookupComboFiller;
begin
  xrFbegin(@Result).setref(Result);
  if combo = nil then combo := oCombo;
  if combo <> nil then notifyIdle(combo, True);
  xrend;
end;
{ *************************************************************************** }
initialization
  // registerMagic(XDBLookupComboFiller);
{ *************************************************************************** }
end.
