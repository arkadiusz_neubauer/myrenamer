unit MXMemory;

interface

uses
  SysUtils, Types
  {$IFDEF MSWINDOWS}
   ,Windows
  {$IFEND}
  ;
{ *************************************************************************** }
const
  MaxCharMem = $7FFFFFFF div SizeOf(Char);
  MaxByteMem = $7FFFFFFF div SizeOf(Byte);
  MaxWordMem = $7FFFFFFF div SizeOf(Word);
  MaxIntMem  = $7FFFFFFF div SizeOf(Integer);
  MaxLongMem = $7FFFFFFF div SizeOf(LongInt);
  MaxPtrMem  = $7FFFFFFF div SizeOf(Pointer);
  MaxLWMem   = $7FFFFFFF div SizeOf(Longword);

type
  PCharMem = ^TCharMem;
  TCharMem = packed array[0..MaxCharMem - 1] of Char;
  PByteMem = ^TByteMem;
  TByteMem = packed array[0..MaxByteMem - 1] of Byte;
  PWordMem = ^TWordMem;
  TWordMem = packed array[0..MaxWordMem - 1] of Word;
  PIntMem  = ^TIntMem;
  TIntMem  = packed array[0..MaxIntMem - 1] of Integer;
  PLongMem = ^TLongMem;
  TLongMem = packed array[0..MaxLongMem - 1] of LongInt;
  PPtrMem  = ^TPtrMem;
  TPtrMem  = packed array[0..MaxPtrMem - 1] of Pointer;
  PLWMem   = ^TLWMem;
  TLWMem   = packed array[0..MaxLWMem - 1] of Longword;

type
  PPoint = ^TPoint;
  PPointArr = ^TPointArr;
  TPointArr = array[0..$7FFFFFFF div SizeOf(TPoint) - 1] of TPoint;

const
  dumpCodes = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*^';

{ *************************************************************************** }
type
  PString   = ^String;

  PByte     = ^Byte;
  PWord     = ^Word;
  PInt      = ^Integer;
  PLong     = ^LongInt;
  PLW       = ^Longword;
  PPtr      = ^Pointer;

  PSingle   = ^Single;
  PDouble   = ^Double;
  PExtended = ^Extended;
  PComp     = ^Comp;

  PObj      = ^TObject;

  EInternalError = class(Exception);
{ *************************************************************************** }
function memOffs(aP: Pointer; aOffs: Integer): Pointer;
procedure memSwap(var aA, aB; aCount: Integer);
procedure memSwapString(var a, b: String);
procedure memCopy(var aDst, aSrc; aCount: Integer);
function memAlloc(Size: Cardinal): Pointer;
function memRealloc(var AP; Size: Cardinal): Pointer;
procedure memFree(var AP);
procedure objZero(var AO);
procedure objFree(var AO);

function hashBuffer(var Buffer; Size: Integer; Hash: Integer = 0; Start: Integer = 0): Integer;
function hashString(const aText: String; ignoreCase: Boolean = False; Hash: Integer = 0): Integer;

function dump3(memory: Pointer): string;
procedure undump3(const s: string; memory: Pointer);
function dumpMemory(memory: Pointer; size: Integer): string;
function undumpMemory(const s: String; memory: Pointer; start: Integer = 1): Integer;
function dumpLength(const s: String; start: Integer = 1): Integer;

{ *************************************************************************** }
implementation
{ *************************************************************************** }
function memOffs(aP: Pointer; aOffs: Integer): Pointer;
begin
  Result := Pointer(Integer(aP) + aOffs);
end;
{ *************************************************************************** }
procedure memSwap(var aA, aB; aCount: Integer);
var
  A: PByteMem;
  B: PByteMem;
  i: Integer;
  h: Byte;
begin
  A := @aA; B := @aB;

  for i := 0 to aCount - 1 do begin
    h := A^[i]; A^[i] := B^[i]; B^[i] := h;
  end;
end;
{ **************************************************************************** }
procedure memSwapString(var a, b: String);
var
  s: String;
begin
  s := a; a := b; b := s;
end;
{ *************************************************************************** }
procedure memCopy(var aDst, aSrc; aCount: Integer);
begin
  Move(aSrc, aDst, aCount);
end;
{ **************************************************************************** }
function memAlloc(Size: Cardinal): Pointer;
begin
  Result := AllocMem(Size);
end;
{ **************************************************************************** }
function memRealloc(var AP; Size: Cardinal): Pointer;
var
  P: Pointer absolute AP;
begin
  ReallocMem(P, Size); Result := P;
end;
{ **************************************************************************** }
procedure memFree(var AP);
var
  P : Pointer absolute AP;
begin
  if P = nil then exit;
  FreeMem(P); P := nil;
end;
{ *************************************************************************** }
procedure objZero(var AO);
var
  O: TObject absolute AO;
begin
  O := nil;
end;
{ **************************************************************************** }
procedure objFree(var AO);
var
  O: TObject absolute AO;
begin
  if Assigned(O) then begin O.Free; O := nil; end;
end;
{ **************************************************************************** }
function hashBuffer(var Buffer; Size: Integer; Hash: Integer = 0; Start: Integer = 0): Integer;
var
  i: Integer;
begin
  for i := 0 to Size - 1 do begin
    PByteMem(@Hash)^[(i + Start) mod SizeOf(Integer)] :=
      PByteMem(@Hash)^[(i + Start) mod SizeOf(Integer)] xor
      PByteMem(@Buffer)^[i];
  end;
  Result := Hash;
end;
{ **************************************************************************** }
function hashString(const aText: String; ignoreCase: Boolean = False; Hash: Integer = 0): Integer;
var
  s: String;
  i, c: Integer;
  b: PByteMem;
  c1, c2: Byte;
  l: Integer;
begin
  Result := Hash;

  if ignoreCase
    then s := AnsiLowerCase(aText)
    else s := aText;

  c := 0; b := PByteMem(@Result);
  // l := Min(Length(s), xscHashMaxLen);
  l := Length(s);

  for i := 1 to l do begin
    c1 := Byte(s[i]); if i < Length(s) then c2 := Byte(s[i+1]) else c2 := 17;
    b^[c] := b^[c] xor (c1 + c2); c := (c + 1) mod (SizeOf(Result));
  end;
  Result := (Result * $7124A661 + $34618949) and $7FFFFFFF;
end;
{ **************************************************************************** }
function codeAsC(i: Integer): char;
begin
  Result := dumpCodes[i + 1];
end;
{ **************************************************************************** }
function codeAsI(c: char): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 1 to Length(dumpCodes) do
    if dumpCodes[i] = c then Result := i - 1;
end;
{ **************************************************************************** }
function dump3(memory: Pointer): string;
var
  a: PByteMem absolute memory;
begin
  // 012345 670123 456701 234567
  // 012345 012345 012345 012345
  SetLength(Result, 4);
  Result[1] := codeAsC(a^[0] and not $C0);
  Result[2] := codeAsC(((a^[0] and $C0) shr 2) or (a^[1] and not $F0));
  Result[3] := codeAsC(((a^[1] and $F0) shr 2) or (a^[2] and not $FC));
  Result[4] := codeAsC(((a^[2] and $FC) shr 2));
end;
{ **************************************************************************** }
procedure undump3(const s: string; memory: Pointer);
var
  a: PByteMem absolute memory;
  w0, w1, w2, w3: Byte;
begin
  if Length(s) <> 4 then Exit;
  w0 := codeAsI(s[1]); w1 := codeAsI(s[2]);
  w2 := codeAsI(s[3]); w3 := codeAsI(s[4]);

  a^[0] := w0 or ((w1 shl 2) and $C0);
  a^[1] := (w1 and $0F) or ((w2 shl 2) and $F0);
  a^[2] := (w2 and $03) or ((w3 shl 2) and $FC);
end;
{ **************************************************************************** }
function dumpMemory(memory: Pointer; size: Integer): string;
var
  memoryhlp: PByteMem absolute memory;
  input: Integer;
  inputhlp: TByteMem absolute input;
  i, c: Integer;
begin
  i := 0;
  Result := dump3(@Size);
  while i < Size do begin
    c := 0; input := 0;
    while (c < 3) and (i < Size) do begin
      inputhlp[c] := memoryhlp^[i];
      Inc(i); Inc(c);
    end;
    Result := Result + dump3(@input);
  end;
end;
{ **************************************************************************** }
function undumpMemory(const s: String; memory: Pointer; start: Integer = 1): Integer;
var
  memoryhlp: PByteMem absolute memory;
  input: Integer;
  inputhlp: TByteMem absolute input;
  i, si, c, len: Integer;
begin
  len := 0; si := start;
  undump3(Copy(s, si, 4), @len); Inc(si, 4);

  i := 0;
  while i < len do begin
    undump3(Copy(s, si, 4), @input); Inc(si, 4);
    c := 0;
    while (c < 3) and (i < len) do begin
      memoryhlp^[i] := inputhlp[c];
      Inc(i); Inc(c);
    end;
  end;

  Result := si;
end;
{ **************************************************************************** }
function dumpLength(const s: String; start: Integer = 1): Integer;
begin
  Result := 0;
  undump3(Copy(s, start, 4), @Result);
end;
{ **************************************************************************** }
end.