{$IFDEF DBG_XOKERNEL}{$D+,L+,O-}{$ELSE}{$D-,L-,O+}{$ENDIF}
{:XObject's base definitions
@desc Declaration of XObject base class. Everything you need to define your own
  XO-like classes.<br><br>
  <b>Version</b>: <b>1.4.2001.09.25</b><br><br>
  <b>Author</b>: Milosz A. Krajewski (vilge@mud.org.pl)<br><br>
  <b>Advantages</b>:<ul>
    <li>reference counting (garbage collecting)
    <li>duplicate
    <li>copy
    <li>swap
    <li>streaming (serialization)
    <li>easy python encasulation (sharing data with scripting language)</ul>
  <b>Disadvantages</b>:<ul>
    <li>XObject are slower than standard TObject (speed is comparable to working with Interfaces)
  </ul>
  <b>Revision</b>:<ul>
    <li><b>1.0</b> serialization (load/save)
    <li><b>1.2</b> new stack management (35% faster)
    <li><b>1.3</b> implements IUnknown interface for further use
    <li><b>1.4</b> some bugfixes (50% speed improvement in some operations)
  </ul>
  <b>Comments</b><ul>
    <li>Use "fin()" on the end of every call-chain ie.:<br>
  "o.add(C.Create).add(C.Create).add(C.Create).fin"</ul>
@todo test threaded stack frames}

{ TODO : XObject.Create.exception.fin creates problem }
unit MXObject;

interface

uses
  Variants,
  Windows, SysUtils, Dialogs, Classes,
  MXMemory, MXObjectSupportStack;

const
  iXCounter: Integer = 0;

type
  TXResultMode = (xrrGet, xrrSkip);

{ *************************************************************************** }

type
  TStreamingMode = (smNone, smRead, smWrite);
const
  BooleanDesc: array[False..True] of String = ('False', 'True');
  TStreamingModeDesc: array[0..2] of String = ('smNone', 'smRead', 'smWrite');

type
  EStreamingError = class(Exception);

  XObject = class;

  XObjectClass = class of XObject;

  XStream = class;

  IXObject = interface(IUnknown)
    function getXObject: XObject; stdcall;
  end;

  PXObject = ^XObject;
  XObject = class(TObject, IXObject)
  protected
    xRef: Integer;
    xFrozen: Boolean;

    class function v_class_magic: Integer; virtual;
    class function v_class_create: XObject; virtual;
    class function v_class_name: String; virtual;
    class function v_class_self: XObjectClass; virtual;
    class function v_class_parent: XObjectClass; virtual;
    procedure v_copy(v: XObject); virtual;
    procedure v_swap(v: XObject); virtual;
    procedure v_load(s: XStream); virtual;
    procedure v_store(s: XStream); virtual;
    procedure v_cleanup; virtual;
    function v_getAsString: String; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Cleanup;

    function getXObject: XObject; stdcall;

    // implementation of IUnknown interface - copied from TInterfacedObject
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;

    // xrbegin, xrcreate, xrdestroy is final - do not ever override it...
    function xrbegin(xrparams, xrvars: array of PXObject; xrresult: PXObject = nil): XObject; register;
    function xrPbegin(xrparams: array of PXObject; xrresult: PXObject = nil): XObject; register;
    function xrFbegin(xrresult: PXObject = nil): XObject; register;

    procedure xrcreate(xrparams, xrvars: array of PXObject); register;
    procedure xrPcreate(xrparams: array of PXObject); register;
    procedure xrFcreate; register;

    procedure xrdestroy(xrvars: array of PXObject); register;
    procedure xrFdestroy; register;
    // ...because it's hardly machine-dependent

    class function getClass: XObjectClass; // identify class
    class function getParentClass: XObjectClass; // identify parent class
    class function getClassName: String; // get class name
    class function getParentClassName: String; // get parent class name
    class function getClassInfo: String;

    function dref(aFree: Boolean = True): XObject;
    function iref: XObject;
    function setref(var v): XObject;
    function freeze(p: Boolean = True): XObject;
    function fin: Boolean; // fin stands for "free if needed", not "finalize"

    function new: XObject;
    function dup: XObject;
    function copy(v: XObject): XObject;
    function swap(v: XObject): XObject;
    function tmp: XObject;

    function getRef: Integer;
    function getHandle: Integer;
    function getAsString: String;
  end;

  XAbstractInstanceFinder = class(XObject)
  protected
    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;

    // ---- from XAbstractInstanceFinder ----
    function v_find(handle: Integer): XObject; virtual; abstract;
    procedure v_add(handle: Integer; obj: XObject); virtual; abstract;
    procedure v_clear; virtual; abstract;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XAbstractInstanceFinder;
    function iref: XAbstractInstanceFinder;
    function setref(var v): XAbstractInstanceFinder;
    function freeze(p: Boolean = True): XAbstractInstanceFinder;

    function new: XAbstractInstanceFinder;
    function dup: XAbstractInstanceFinder;
    function copy(v: XAbstractInstanceFinder): XAbstractInstanceFinder;
    function swap(v: XAbstractInstanceFinder): XAbstractInstanceFinder;

    // ---- from XAbstractInstanceFinder ----
    function find(handle: Integer): XObject;
    function add(handle: Integer; obj: XObject): XAbstractInstanceFinder;
    function clear: XAbstractInstanceFinder;
  end;

  XAbstractInstanceFinderClass = class of XAbstractInstanceFinder;

  XStream = class(XObject)
  protected
    bOwner: Boolean;
    vclStream: TStream;
    instanceFinder: XAbstractInstanceFinder;
    streamingMode: TStreamingMode;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    function v_getAsString: String; override;

    // ---- from XStream ----
    procedure v_read(var Buffer; Size: Integer); virtual;
    procedure v_write(var Buffer; Size: Integer); virtual;
    function v_getpos: Integer; virtual;
    procedure v_setpos(p: Integer); virtual;
    function v_getsize: Integer; virtual;
    procedure v_setsize(s: Integer); virtual;

    function ComponentToString(Component: TComponent): String;
    function StringToComponent(Value: String): TComponent;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XStream;
    function iref: XStream;
    function setref(var v): XStream;
    function freeze(p: Boolean = True): XStream;

    function new: XStream;
    function dup: XStream;
    function copy(v: XStream): XStream;
    function swap(v: XStream): XStream;
    function tmp: XStream;

    // ---- from XStream ----
    constructor Create;
    destructor Destroy; override;

    function setInstanceFinder(f: XAbstractInstanceFinder): XStream;
    function getInstanceFinder: XAbstractInstanceFinder; //p

    function setStreamingMode(aStreamingMode: TStreamingMode): XStream;

    function readBuffer(var Buffer; Size: Integer): XStream;
    function writeBuffer(var Buffer; Size: Integer): XStream;

    function getPosition: Integer;
    function setPosition(p: Integer): XStream;
    function getSize: Integer;
    function setSize(s: Integer): XStream;

    function readDouble: Double;
    function writeDouble(d: Double): XStream;
    function readCurrency: Currency;
    function writeCurrency(d: Currency): XStream;
    function readString: String;
    function writeString(s: String): XStream;
    function readInteger: Integer;
    function writeInteger(i: Integer): XStream;
    function readInt64: Int64;
    function writeInt64(i: Int64): XStream;
    function readBool: Boolean;
    function writeBool(b: Boolean): XStream;
    function readVariant: Variant;
    function writeVariant(v: Variant): XStream;
    function readByte: Byte;
    function writeByte(b: Byte): XStream;
    function readChar: Char;
    function writeChar(c: Char): XStream;

    function readXObject: XObject; overload;
    function readXObject(var o): XStream; overload;
    function writeXObject(o: XObject): XStream;

    function writeComponent(c: TComponent): XStream;
    function readComponent: TComponent;

    function createFile(FileName: String): XStream;
    function createMem: XStream;

    function getVCLStream: TStream;
    function setVCLStream(s: TStream): XStream;
    function isVCLOwner: Boolean;
    function setVCLOwner(aOwner: Boolean = True): XStream;

    function dumpString: String; //p
    function undumpString(const s: String): XStream; //p

    function isEOF: Boolean; //p
    function isBOF: Boolean; //p

    function appendToStream(aDestination: XStream): XStream; //p
  end;

const
  XObject_Magic = -1;
  XStream_Magic = -1;
  XAbstractInstanceFinder_Magic = -1;

const
  defaultInstanceFinderClass: XAbstractInstanceFinderClass = nil;

function xrunq(var v): XObject;

procedure initThreadStack;
procedure doneThreadStack;

procedure xrbegin(xrparams, xrvars: array of PXObject; xrresult: PXObject = nil); register;
procedure xrPbegin(xrparams: array of PXObject; xrresult: PXObject = nil); register;
procedure xrFbegin(xrresult: PXObject = nil); register;
procedure xrend(t: TXResultMode = xrrGet); register;
function xrnil(var v): XObject;
procedure xrraise(e: Exception);

function createMagic(magic: Integer): XObject;
procedure registerMagic(aClass: XObjectClass);

implementation
{ *************************************************************************** }
type
{* Class magic numbers handling & python registration *********************** }
  PXObjectClassArray = ^TXObjectClassArray;
  TXObjectClassArray = array[0..$7FFFFFFF div SizeOf(XObjectClass) - 1] of XObjectClass;

var
  ssmagic: TXOSS;

const
  dumpBufferSize = $8000;

{* Stack frames handling **************************************************** }
type
  EStackCorrupted = class(Exception);

  PPF = ^TPF;
  TPF = record
    _self: XObject;
    _ebp: Longword;
    _result: PXObject;
    _prm_cnt: Integer;
    _var_cnt: Integer;
    _constructor: Boolean;
  end;
  PPFArray = ^TPFArray;
  TPFArray = array[0..$7FFFFFFF div SizeOf(TPF) - 1] of TPF;

  PPXObjectArray = ^TPXObjectArray;
  TPXObjectArray = array[0..$7FFFFFFF div SizeOf(PXObject) - 1] of PXObject;

threadvar
  ssfun, ssfunvars: TXOSS; // stack frame info - for every thread
{ **************************************************************************** }
procedure initThreadStack;
begin
  ss_init(ssfun, SizeOf(TPF)); ss_init(ssfunvars, SizeOf(PXObject));
end;
{-----------------------------------------------------------------------------}
procedure doneThreadStack;
begin
  ss_done(ssfun); ss_done(ssfunvars);
end;
{-----------------------------------------------------------------------------}
procedure initMagicStack;
begin
  ss_init(ssmagic, SizeOf(XObjectClass));
  ssmagic.delta := 16;
end;
{-----------------------------------------------------------------------------}
procedure doneMagicStack;
begin
  ss_done(ssmagic);
end;
{ *************************************************************************** }
procedure xrinit(xrebp: Longword; xrself: XObject; xrparams, xrvars: array of PXObject; xrresult: PXObject = nil; xrconstructor: Boolean = False);
var
  i: Integer;
  frame: PPF;
  ptr: PPXObjectArray;
  p: PXObject;
begin
  if ssfun.tag <> ss_magictag then ss_test_failed();

  frame := ss_alloc(ssfun);

  with frame^ do begin
    _constructor := xrconstructor;
    _self := xrself;
    _result := xrresult;
    _ebp := xrebp;

    _self.iref;

    _prm_cnt := Length(xrparams);
    if _prm_cnt > 0 then begin
      ptr := ss_alloc(ssfunvars, _prm_cnt);
      for i := 0 to _prm_cnt - 1 do begin
        p := xrparams[i];
        if p <> nil then p^.iref;
        ptr^[i] := p;
      end;
    end;

    _var_cnt := Length(xrvars);
    if _var_cnt > 0 then begin
      ptr := ss_alloc(ssfunvars, _var_cnt);
      for i := 0 to _var_cnt - 1 do begin
        ptr^[i] := xrvars[i];
        if ptr^[i] <> nil then ptr^[i]^ := nil;
      end;
    end;

    if _result <> nil then _result^ := nil;
  end;
end;
{-----------------------------------------------------------------------------}
procedure xrinit_no_v(xrebp: Longword; xrself: XObject; xrparams: array of PXObject; xrresult: PXObject = nil; xrconstructor: Boolean = False);
// this procedure should be identical with xrinit()
// the only difference is lack of xrvars passing
// I decided to implement it this way because of 25% speed improvement
var
  i: Integer;
  frame: PPF;
  ptr: PPXObjectArray;
  p: PXObject;
begin
  if ssfun.tag <> ss_magictag then ss_test_failed();

  frame := ss_alloc(ssfun);

  with frame^ do begin
    _constructor := xrconstructor;
    _self := xrself;
    _result := xrresult;
    _ebp := xrebp;

    _self.iref;

    _prm_cnt := Length(xrparams);
    if _prm_cnt > 0 then begin
      ptr := ss_alloc(ssfunvars, _prm_cnt);
      for i := 0 to _prm_cnt - 1 do begin
        p := xrparams[i];
        if p <> nil then p^.iref;
        ptr^[i] := p;
      end;
    end;

    _var_cnt := 0;

    if _result <> nil then _result^ := nil;
  end;
end;
{-----------------------------------------------------------------------------}
procedure xrinit_no_pv(xrebp: Longword; xrself: XObject; xrresult: PXObject = nil; xrconstructor: Boolean = False);
// this procedure should be identical with xrinit()
// the only difference is lack of xrvars passing
// I decided to implement it this way because of 25% speed improvement
var
  frame: PPF;
begin
  if ssfun.tag <> ss_magictag then ss_test_failed();

  frame := ss_alloc(ssfun);

  with frame^ do begin
    _constructor := xrconstructor;
    _self := xrself;
    _result := xrresult;
    _ebp := xrebp;

    _self.iref;

    _prm_cnt := 0;
    _var_cnt := 0;

    if _result <> nil then _result^ := nil;
  end;
end;
{-----------------------------------------------------------------------------}
procedure xrbegin(xrparams, xrvars: array of PXObject; xrresult: PXObject = nil); register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit(last_ebp, nil, xrparams, xrvars, xrresult, False);
end;
{-----------------------------------------------------------------------------}
procedure xrPbegin(xrparams: array of PXObject; xrresult: PXObject = nil); register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_v(last_ebp, nil, xrparams, xrresult, False);
end;
{-----------------------------------------------------------------------------}
procedure xrFbegin(xrresult: PXObject = nil); register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_pv(last_ebp, nil, xrresult, False);
end;
{-----------------------------------------------------------------------------}
procedure xrend(t: TXResultMode = xrrGet); register;
var
  {$IFOPT W+}
  last_ebp: Longword;
  {$ENDIF}
  i: Integer;
  frame: PPF;
  ptr: PPXObjectArray;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ENDIF}

  // this code is from xrdone - it has been embeded due speed optimization

  if ssfun.tag <> ss_magictag then ss_test_failed();

  frame := ss_current(ssfun);

  with frame^ do begin
    {$IFOPT W+}
    if last_ebp <> _ebp then begin
      MessageDlg('Stack currupted. Fatal Error. EBP not matched.', mtError, [mbOK], 0);
      Halt;
    end;
    {$ENDIF}

    _self.dref(not _constructor);

    if _var_cnt > 0 then begin
      ptr := ss_current(ssfunvars, _var_cnt);
      for i := 0 to _var_cnt - 1 do if ptr^[i] <> nil then xrnil(ptr^[i]^);
      ss_free(ssfunvars, _var_cnt);
    end;

    if _prm_cnt > 0 then begin
      ptr := ss_current(ssfunvars, _prm_cnt);
      for i := 0 to _prm_cnt - 1 do if ptr^[i] <> nil then ptr^[i]^.dref;
      ss_free(ssfunvars, _prm_cnt);
    end;

    if (_result <> nil) and (_result^ <> nil) then
      _result^ := _result^.dref(t = xrrSkip); // decrease reference counter
  end;

  ss_free(ssfun);
end;
{-----------------------------------------------------------------------------}
procedure xrraise(e: Exception);
var
  frame: PPF;
begin
  if ssfun.tag <> ss_magictag then ss_test_failed();
  frame := ss_current(ssfun);
  if frame^._result <> nil then xrnil(frame^._result^);
  raise e;
end;
{ *************************************************************************** }
function xrunq(var v): XObject;
var
  O: XObject absolute v;
begin
  Result := O;
  if (O <> nil) and (O.xRef > 1) then begin
    Result := O.dup.iref; O.dref; O := Result;
  end;
end;
{-----------------------------------------------------------------------------}
function xrnil(var v): XObject;
var
  o: XObject absolute v;
begin
  Result := nil;
  if o = nil then exit;
  o.dref; o := nil;
end;
{ *************************************************************************** }
function createMagic(magic: Integer): XObject;
var
  i: Integer;
  ptr: PXObjectClassArray;
begin
  xrFbegin(@Result);
  ptr := ss_first(ssmagic);
  for i := 0 to ss_count(ssmagic) - 1 do begin
    if magic = ptr^[i].v_class_magic then begin
      ptr^[i].v_class_create.setref(Result); break;
    end;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure registerMagic(aClass: XObjectClass);
var
  i, magic: Integer;
  ptr: PXObjectClassArray;
begin
  magic := aClass.v_class_magic;
  ptr := ss_first(ssmagic);

  {$IFDEF DBG_XO}SendDebug(Format('registering %s as %s...', [aClass.v_class_name, IntToHex(aClass.v_class_magic, 8)])); {$ENDIF}
  for i := 0 to ss_count(ssmagic) - 1 do begin
    if magic = ptr^[i].v_class_magic then begin
      MessageDlg(Format('Magic %s already exists for class %s. Cannot register %s.', [IntToHex(magic, 8), ptr^[i].v_class_name, aClass.v_class_name]), mtError, [mbOK], 0);
      Halt;
    end;
  end;

  ptr := ss_alloc(ssmagic);
  ptr^[0] := aClass;
end;
{ *************************************************************************** }
constructor XObject.Create;
begin
  xrFcreate();
  {$IFDEF DBG_XO}SendDebug(Format('NEW: %d, %s, %s', [iXCounter, IntToHex(Integer(Self), 8), v_class_name]));{$ENDIF}
  inherited;
  //Inc(iXCounter);
  {$IFDEF DBG_TAGGER}xTag := allocXTagger;{$ENDIF}
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.getXObject: XObject;
begin
  xrFbegin(@Result).setref(Result); xrend;
end;
{-----------------------------------------------------------------------------}
procedure XObject.Cleanup;
var
  b: Boolean;
begin
  if Self = nil then exit;
  xrFbegin;
  b := xFrozen; xFrozen := True; v_cleanup; xFrozen := b;
  xrend;
end;
{-----------------------------------------------------------------------------}
destructor XObject.Destroy;
begin
  xrFdestroy;
  // if ssfun.weak = Self then ssfun.weak := nil; // weak reference - see comments
  //Dec(iXCounter);
  v_cleanup;
  {$IFDEF DBG_XO}SendDebug(Format('DEL: %d, %s, %s', [iXCounter, IntToHex(Integer(Self), 8), v_class_name]));{$ENDIF}
  inherited;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.xrbegin(xrparams, xrvars: array of PXObject; xrresult: PXObject = nil): XObject; register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit(last_ebp, Self, xrparams, xrvars, xrresult, False); Result := Self;
end;
{-----------------------------------------------------------------------------}
function XObject.xrPbegin(xrparams: array of PXObject; xrresult: PXObject = nil): XObject; register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_v(last_ebp, Self, xrparams, xrresult, False); Result := Self;
end;
{-----------------------------------------------------------------------------}
function XObject.xrFbegin(xrresult: PXObject = nil): XObject; register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_pv(last_ebp, Self, xrresult, False); Result := Self;
end;
{-----------------------------------------------------------------------------}
procedure XObject.xrcreate(xrparams, xrvars: array of PXObject); register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit(last_ebp, Self, xrparams, xrvars, nil, True);
end;
{-----------------------------------------------------------------------------}
procedure XObject.xrPcreate(xrparams: array of PXObject); register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_v(last_ebp, Self, xrparams, nil, True);
end;
{-----------------------------------------------------------------------------}
procedure XObject.xrFcreate; register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_pv(last_ebp, Self, nil, True);
end;
{-----------------------------------------------------------------------------}
{ TODO : test it, but if destroy is called so freeze will not change anything }
procedure XObject.xrFdestroy; register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit_no_pv(last_ebp, nil, nil, False);
  freeze();
end;
{-----------------------------------------------------------------------------}
procedure XObject.xrdestroy(xrvars: array of PXObject); register;
var
  last_ebp: Longword;
begin
  {$IFOPT W+}
  asm
    push eax
    mov eax,[ebp]
    mov last_ebp,eax
    pop eax
  end;
  {$ELSE}
  last_ebp := 0;
  {$ENDIF}
  xrinit(last_ebp, nil, [], xrvars, nil, False);
  freeze();
end;
{-----------------------------------------------------------------------------}
function XObject.fin: Boolean;
begin
  Result := False;
  if (xRef <= 0) and (not xFrozen) then begin Self.Free; Result := True; end;
end;
{-----------------------------------------------------------------------------}
function XObject.dref(aFree: Boolean = True): XObject;
begin
  Result := Self; if Self = nil then exit;
  Dec(xRef);
  if (aFree) and (xRef <= 0) and (not xFrozen) then begin Self.Free; Result := nil; end;
end;
{-----------------------------------------------------------------------------}
function XObject.iref: XObject;
begin
  Result := Self; if Self = nil then exit;
  Inc(xRef);
end;
{-----------------------------------------------------------------------------}
function XObject.setref(var v): XObject;
var
  o: XObject absolute v;
  b: XObject;
begin
  Result := Self; if o = Self then exit;
  b := o; o := Self;
  if o <> nil then o.iref;
  if b <> nil then b.dref;
end;
{-----------------------------------------------------------------------------}
function XObject.freeze(p: Boolean = True): XObject;
begin
  Result := Self; if Self = nil then exit;
  xFrozen := p;
  if (not xFrozen) and (xRef <= 0) then begin Self.Free; Result := nil; end;
end;
{-----------------------------------------------------------------------------}
class function XObject.v_class_magic: Integer; begin Result := XObject_Magic; end;
{-----------------------------------------------------------------------------}
class function XObject.v_class_create: XObject; begin Result := XObject.Create; end;
class function XObject.v_class_name: String; begin Result := 'XObject'; end;
class function XObject.v_class_self: XObjectClass; begin Result := Self; end;
class function XObject.v_class_parent: XObjectClass; begin Result := nil; end;
{-----------------------------------------------------------------------------}
class function XObject.getClass: XObjectClass; begin Result := v_class_self; end;
class function XObject.getParentClass: XObjectClass; begin Result := v_class_parent; end;
class function XObject.getClassName: String; begin Result := v_class_name; end;
{-----------------------------------------------------------------------------}
class function XObject.getClassInfo: String;
var
  cnt: String;
begin
  cnt := 'N/A';
  Result := Format('Name:%s, Parent:%s, Magic:%s, Counter:%s',
    [getClassName, getParentClassName, IntToHex(v_class_magic, 8), cnt]);
end;
{-----------------------------------------------------------------------------}
class function XObject.getParentClassName: String;
var
  pc: XObjectClass;
begin
  pc := v_class_parent; if pc = nil then Result := '' else Result := pc.v_class_name;
end;
{-----------------------------------------------------------------------------}
procedure XObject.v_load(s: XStream);
begin
  try
    xrPbegin([@s]);
    xFrozen := s.readBool;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XObject.v_copy(v: XObject); begin end;
procedure XObject.v_swap(v: XObject); begin end;
procedure XObject.v_cleanup;          begin end;
{-----------------------------------------------------------------------------}
procedure XObject.v_store(s: XStream);
begin
  try
    xrPbegin([@s]);
    s.WriteBool(xFrozen);
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XObject.v_getAsString: String;
begin
  xrFbegin();
  Result := Format('%s<%d>', [v_class_name, xRef]);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.new: XObject;
begin
  Result := nil; if Self = nil then exit;
  xrFbegin(@Result);
  v_class_create.setref(Result);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.dup: XObject;
begin
  Result := nil; if Self = nil then exit;
  xrFbegin(@Result);
  v_class_create.setref(Result).v_copy(Self);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.copy(v: XObject): XObject;
begin
  Result := nil; if Self = nil then exit;
  xrPbegin([@v], @Result).setref(Result).v_copy(v);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.swap(v: XObject): XObject;
begin
  Result := nil; if Self = nil then exit;
  xrPbegin([@v], @Result).setref(Result).v_swap(v);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.tmp: XObject;
begin
  Result := nil; if Self = nil then exit;
  xrFbegin(@Result);
  if xref = 1 then setref(Result) else dup.setref(Result);
  // duplicates object to be unique/tempoary
  // it works like 'dup' but it does only one copy ie:
  //   anyFunction(x.dup.dup.dup); makes 3 copies
  // and:
  //   anyFunction(x.tmp.tmp.tmp); makes only one copy
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.getAsString: String;
begin
  Result := 'None'; if Self = nil then exit;
  xrFbegin(); Result := v_getAsString; xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.getHandle: Integer;
begin
  Result := 0; if Self = nil then exit;
  xrFbegin();
  if xref = 1 then Result := 0 else Result := Integer(Self);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.getRef: Integer;
begin
  Result := -1; if Self = nil then exit;
  xrFbegin();
  Result := xRef - 1; // decrement because xrbegin icreases xref
  xrend;
end;
{-----------------------------------------------------------------------------}
function XObject.QueryInterface(const IID: TGUID; out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;
{-----------------------------------------------------------------------------}
function XObject._AddRef: Integer;
begin
  iref(); Result := xref;
end;
{-----------------------------------------------------------------------------}
function XObject._Release: Integer;
begin
  Result := xref - 1; dref();
end;
{ *************************************************************************** }
// do not change anything in these definitions - begin
function XAbstractInstanceFinder.iref: XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).iref); end;
function XAbstractInstanceFinder.dref(aFree: Boolean = True): XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).dref(aFree)); end;
function XAbstractInstanceFinder.setref(var v): XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).setref(v)); end;
function XAbstractInstanceFinder.freeze(p: Boolean = True): XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).freeze(p)); end;
function XAbstractInstanceFinder.new: XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).new); end;
function XAbstractInstanceFinder.dup: XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).dup); end;
function XAbstractInstanceFinder.copy(v: XAbstractInstanceFinder): XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).copy(v)); end;
function XAbstractInstanceFinder.swap(v: XAbstractInstanceFinder): XAbstractInstanceFinder; begin Result := XAbstractInstanceFinder(XObject(Self).swap(v)); end;
{-----------------------------------------------------------------------------}
class function XAbstractInstanceFinder.v_class_magic: Integer; begin Result := XAbstractInstanceFinder_Magic; end;
class function XAbstractInstanceFinder.v_class_create: XObject; begin Result := nil; {XAbstractInstanceFinder.Create;} end;
class function XAbstractInstanceFinder.v_class_name: String; begin Result := 'XAbstractInstanceFinder'; end;
class function XAbstractInstanceFinder.v_class_self: XObjectClass; begin Result := Self; end;
class function XAbstractInstanceFinder.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
function XAbstractInstanceFinder.find(handle: Integer): XObject;
begin
  xrFbegin(@Result); v_find(handle).setref(Result); xrend;
end;
{-----------------------------------------------------------------------------}
function XAbstractInstanceFinder.add(handle: Integer; obj: XObject): XAbstractInstanceFinder;
begin
  xrPbegin([@obj], @Result).setref(Result); v_add(handle, obj); xrend;
end;
{-----------------------------------------------------------------------------}
function XAbstractInstanceFinder.clear: XAbstractInstanceFinder;
begin
  xrFbegin(@Result).setref(Result); v_clear; xrend;
end;
{ *************************************************************************** }
// do not change anything in these definitions - begin
function XStream.iref: XStream; begin Result := XStream(XObject(Self).iref); end;
function XStream.dref(aFree: Boolean = True): XStream; begin Result := XStream(XObject(Self).dref(aFree)); end;
function XStream.setref(var v): XStream; begin Result := XStream(XObject(Self).setref(v)); end;
function XStream.freeze(p: Boolean = True): XStream; begin Result := XStream(XObject(Self).freeze(p)); end;
function XStream.new: XStream; begin Result := XStream(XObject(Self).new); end;
function XStream.dup: XStream; begin Result := XStream(XObject(Self).dup); end;
function XStream.copy(v: XStream): XStream; begin Result := XStream(XObject(Self).copy(v)); end;
function XStream.swap(v: XStream): XStream; begin Result := XStream(XObject(Self).swap(v)); end;
function XStream.tmp: XStream; begin Result := XStream(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XStream.v_class_magic: Integer; begin Result := XStream_Magic; end;
class function XStream.v_class_create: XObject; begin Result := nil; {XStream.Create;} end;
class function XStream.v_class_name: String; begin Result := 'XStream'; end;
class function XStream.v_class_self: XObjectClass; begin Result := Self; end;
class function XStream.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XStream.v_copy(v: XObject);
var
  o: XStream absolute v;
begin
  xrPbegin([@v]);
  inherited v_copy(o);
  setVCLStream(o.getVCLStream);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XStream.v_swap(v: XObject);
var
  o: XStream absolute v;
begin
  xrPbegin([@v]);
  inherited v_swap(o);
  memSwap(vclStream, o.vclStream, SizeOf(vclStream));
  memSwap(bOwner, o.bOwner, SizeOf(bOwner));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.v_getAsString: String;
begin
  xrFbegin();
  Result := inherited v_getAsString;
  Result := Format('%s, Owner:%s, instanceFinder:[%s], streamingMode:%s', [
    Result, BooleanDesc[isVCLOwner], instanceFinder.getAsString,
    TStreamingModeDesc[Integer(streamingMode)]]);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XStream.v_read(var Buffer; Size: Integer);
begin
  xrFbegin();
  if vclStream <> nil then vclStream.ReadBuffer(Buffer, Size);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XStream.v_write(var Buffer; Size: Integer);
begin
  xrFbegin();
  if vclStream <> nil then vclStream.WriteBuffer(Buffer, Size);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.v_getpos: Integer;
begin
  xrFbegin();
  if vclStream <> nil then Result := vclStream.Position else Result := -1;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XStream.v_setpos(p: Integer);
begin
  xrFbegin();
  if vclStream <> nil then vclStream.Position := p;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.v_getsize: Integer;
begin
  xrFbegin();
  if vclStream <> nil then Result := vclStream.Size else Result := -1;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XStream.v_setsize(s: Integer);
begin
  xrFbegin();
  if vclStream <> nil then vclStream.Size := s;
  xrend;
end;
{-----------------------------------------------------------------------------}
constructor XStream.Create;
begin
  xrFcreate();
  inherited;
  bOwner := False;
  vclStream := nil;
  xrend;
end;
{-----------------------------------------------------------------------------}
destructor XStream.Destroy;
begin
  xrnil(instanceFinder);
  if bOwner then vclStream.Free;
  inherited;
end;
{-----------------------------------------------------------------------------}
function XStream.createFile(FileName: String): XStream;
begin
  try
    xrFbegin(@Result).setref(Result);
    if bOwner then vclStream.Free;
    bOwner := True;
    try
      vclStream := TFileStream.Create(FileName, fmOpenReadWrite);
    except
      vclStream := TFileStream.Create(FileName, fmCreate);
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.createMem: XStream;
begin
  xrFbegin(@Result).setref(Result);
  if bOwner then vclStream.Free;
  bOwner := True;
  vclStream := TMemoryStream.Create;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.getVCLStream: TStream;
begin
  xrFbegin(); Result := vclStream; xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.setVCLStream(s: TStream): XStream;
begin
  xrFbegin(@Result).setref(Result);
  if s <> vclStream then begin
    if bOwner then vclStream.Free;
    vclStream := s;
    bOwner := False;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.isVCLOwner: Boolean;
begin
  xrFbegin(); Result := bOwner; xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.setVCLOwner(aOwner: Boolean = True): XStream;
begin
  xrFbegin(@Result).setref(Result);
  bOwner := aOwner;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.setInstanceFinder(f: XAbstractInstanceFinder): XStream;
begin
  xrPbegin([@f], @Result).setref(Result);
  if f <> instanceFinder then begin
    f.setref(instanceFinder);
    setStreamingMode(smNone);
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.getInstanceFinder: XAbstractInstanceFinder;
begin
  xrFbegin(@Result);
  instanceFinder.setref(Result);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.setStreamingMode(aStreamingMode: TStreamingMode): XStream;
begin
  try
    xrFbegin(@Result).setref(Result);
    if aStreamingMode <> streamingMode then begin
      if instanceFinder <> nil then instanceFinder.clear;
      streamingMode := aStreamingMode;
    end;
    if (streamingMode <> smNone) and (instanceFinder = nil) then
      if defaultInstanceFinderClass <> nil then
        defaultInstanceFinderClass.v_class_create.setref(instanceFinder)
      else
        raise EStreamingError.Create('Cannot set streaming mode without XInstanceFinder');
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.readBuffer(var Buffer; Size: Integer): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_read(Buffer, Size);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeBuffer(var Buffer; Size: Integer): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(Buffer, Size);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.getPosition: Integer;
begin
  xrFbegin();
  Result := v_getpos();
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.setPosition(p: Integer): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_setpos(p);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.getSize: Integer;
begin
  xrFbegin();
  Result := v_getsize();
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.setSize(s: Integer): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_setsize(s);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readDouble: Double;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeDouble(d: Double): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(d, SizeOf(d));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readCurrency: Currency;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeCurrency(d: Currency): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(d, SizeOf(d));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readString: String;
var
  l: Integer;
  s: String;
begin
  xrFbegin();
  v_read(l, SizeOf(l)); SetLength(s, l); v_read(s[1], l);
  Result := s;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeString(s: String): XStream;
var
  l: Integer;
begin
  xrFbegin(@Result).setref(Result);
  l := Length(s); v_write(l, SizeOf(l)); v_write(s[1], l);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readInteger: Integer;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeInteger(i: Integer): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(i, SizeOf(i));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readInt64: Int64;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeInt64(i: Int64): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(i, SizeOf(i));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readBool: Boolean;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeBool(b: Boolean): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(b, SizeOf(b));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readVariant: Variant;
var
  t: Integer;
begin
  try
    xrFbegin();
    t := readInteger;
    case t of
      varEmpty: VarClear(Result);
      varNull: Result := Null;
      varByte, varSmallint, varInteger: VarCast(Result, readInteger, t);
      varSingle, varDouble: VarCast(Result, readDouble, t);
      varCurrency: VarCast(Result, readCurrency, t);
      varDate: VarCast(Result, VarFromDateTime(readDouble), t);
      varStrArg, varString, varOleStr: VarCast(Result, ReadString, t);
      varBoolean: VarCast(Result, readBool, t);
    else // varDispatch, varError, varVariant, varUnknown, varAny, varTypeMask, varArray, varByRef
      raise EStreamingError.CreateFmt('Writing and reading Variants of type %s are not allowed at this time', [IntToHex(t, 4)]);
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.writeVariant(v: Variant): XStream;
var
  t: Integer;
begin
  try
    xrFbegin(@Result).setref(Result);
    t := VarType(v);
    writeInteger(t);
    case t of
      varEmpty, varNull: ;
      varByte, varSmallint, varInteger: writeInteger(v);
      varSingle, varDouble: writeDouble(v);
      varCurrency: writeCurrency(v);
      varDate: writeDouble(VarToDateTime(v));
      varStrArg, varString, varOleStr: writeString(v);
      varBoolean: writeBool(v);
    else // varDispatch, varError, varVariant, varUnknown, varAny, varTypeMask, varArray, varByRef
      raise EStreamingError.CreateFmt('Writing and reading Variants of type %s are not allowed at this time', [IntToHex(t, 4)]);
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.readByte: Byte;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeByte(b: Byte): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(b, SizeOf(b));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readChar: Char;
begin
  xrFbegin();
  v_read(Result, SizeOf(Result));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.writeChar(c: Char): XStream;
begin
  xrFbegin(@Result).setref(Result);
  v_write(c, SizeOf(c));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readXObject: XObject;
// load object from stream
// if object was previously loaded it finds object's current adress to avoid duplicates
var
  magic: Integer;
  handle: Integer;
begin
  try
    xrFbegin(@Result);
    if (instanceFinder = nil) or (streamingMode <> smRead) then
      raise EStreamingError.Create('Stream is not in object-reading mode');

    handle := readInteger; // get old handle
    if (Handle <> 0) and (instanceFinder.find(handle).setref(Result) = nil) then begin
      // if there is no instanceFinder or object was not loaded yet, load it
      magic := readInteger; // get magic number
      instanceFinder.add(handle, createMagic(magic).setref(Result));
      // create empty object and add it instantly (before load) to avoid self-refferencing object problem

      if Result = nil then
        raise EStreamingError.CreateFmt('Class %s is not registered', [IntToHex(magic, 8)]);
        // sometimes class is not registered and result is nil
        // probably you forgot to call registerMagic(...)

      Result.v_load(Self); // load its data
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.readXObject(var o): XStream;
begin
  try
    xrFbegin(@Result).setref(Result);
    readXObject().setref(o);
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.writeXObject(o: XObject): XStream;
var
  handle: Integer;
begin
  try
    xrPbegin([@o], @Result).setref(Result);
    if (instanceFinder = nil) or (streamingMode <> smWrite) then
      raise EStreamingError.Create('Stream is not in object-writing mode');
    if (o <> nil) and (o.v_class_magic = -1) then
      raise EStreamingError.Create(Format('Class %s is not streamable', [o.getClassName]));

    handle := o.getHandle;
    writeInteger(handle);
    if (handle <> 0) and (instanceFinder.find(handle) = nil) then begin
      instanceFinder.add(handle, o); // add stored object to instanceFinder instantly
      // if there is no instanceFinder or object was not stored yet, store it
      writeInteger(o.v_class_magic); // write class's magic
      o.v_store(Self); // store class
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.ComponentToString(Component: TComponent): string;
var
  BinStream: TMemoryStream;
  StrStream: TStringStream;
  s: String;
begin
  try
    xrFbegin();
    Result := '';
    if Component = nil then exit;
    BinStream := TMemoryStream.Create;
    StrStream := TStringStream.Create(s);
    BinStream.WriteComponent(Component);
    BinStream.Seek(0, soFromBeginning);
    ObjectBinaryToText(BinStream, StrStream);
    StrStream.Seek(0, soFromBeginning);
    Result := StrStream.DataString;
    StrStream.Free;
    BinStream.Free;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.StringToComponent(Value: string): TComponent;
var
  StrStream: TStringStream;
  BinStream: TMemoryStream;
begin
  try
    Result := nil;
    if Value = '' then exit;
    StrStream := TStringStream.Create(Value);
    BinStream := TMemoryStream.Create;
    ObjectTextToBinary(StrStream, BinStream);
    BinStream.Seek(0, soFromBeginning);
    Result := BinStream.ReadComponent(nil);
    BinStream.Free;
    StrStream.Free;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XStream.writeComponent(c: TComponent): XStream;
begin
  xrFbegin(@Result).setref(Result);
  { TODO : probably this will not save linked objects }
  writeString(ComponentToString(c));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.readComponent: TComponent;
begin
  xrFbegin();
  Result := StringToComponent(readString);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.dumpString: String;
var
  Buffer: PByteMem;
begin
  xrbegin([], []);
  setPosition(0);
  Buffer := AllocMem(getSize);
  readBuffer(Buffer^, getSize);
  Result := dumpMemory(Buffer, getSize);
  FreeMem(Buffer);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.undumpString(const s: String): XStream;
var
  Buffer: PByteMem;
  len: Integer;
begin
  xrbegin([], [], @Result).setref(Result);
  setPosition(0); setSize(0);
  len := dumpLength(s);
  Buffer := AllocMem(len);
  undumpMemory(s, Buffer);
  writeBuffer(Buffer^, len);
  FreeMem(Buffer);
  setPosition(0);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.isEOF: Boolean;
begin
  xrFbegin();
  Result := (v_getpos = v_getsize);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.isBOF: Boolean;
begin
  xrFbegin();
  Result := (v_getpos = 0);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XStream.appendToStream(aDestination: XStream): XStream;
begin
  xrPbegin([@aDestination], @Result);
  aDestination.getVCLStream.CopyFrom(Self.vclStream, getSize);
  xrend;
end;
{ *************************************************************************** }
initialization
  initThreadStack; initMagicStack; {$IFDEF PYTHON}initPythonStack; {$ENDIF}
  registerMagic(XObject);
{-----------------------------------------------------------------------------}
finalization
  doneThreadStack;
  doneMagicStack;
{ *************************************************************************** }
end.

