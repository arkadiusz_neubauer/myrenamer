unit MXPassword;

interface

uses
  MXBigIntDynamic;

function EncodeString(s: String): String;
function DecodeString(s: String): String;

implementation

function EncodeString(s: String): String;
var
  i: Integer;
  a: TBigIntValue;
begin
  SetLength(a, Length(s) + 1);
  a[0] := 1;
  for i := 1 to Length(s) do a[i] := Byte(s[i]) xor $FF;
  Result := mmap2str(a, MAXRADIX);
  SetLength(a, 0);
end;

function DecodeString(s: String): String;
var
  i: Integer;
  a: TBigIntValue;
  s2:string;
begin
  a := mstr2map(s, MAXRADIX);
  for i := 1 to Length(a)-1 do s2 := s2+Char(a[i] xor $FF);
  Result := s2;
end;

end.
 