unit MXNotifyCallback;
{ ****************************************************************************
Template:
  MXObject 2001.06.20
***************************************************************************** }
interface

uses
  SysUtils,
  MXMemory, MXObject;
{ *************************************************************************** }
type
  TPyCallbackMode = (pcmNative, pcmPython);

const
  PyCallbackModeDesc: array[pcmNative..pcmPython] of String = ('pcmNative', 'pcmPython');

type
  TNotifyCallback = procedure(aSender: XObject) of object;

  XNotifyCallback = class(XObject)
  protected
    callbackMode: TPyCallbackMode;

    nativeCallback: TNotifyCallback;
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    function v_getAsString: String; override;
  public
    // ---- from XNotifyCallback ----
    destructor Destroy; override;

    // ---- from XObject ----
    function dref(aFree: Boolean = True): XNotifyCallback;
    function iref: XNotifyCallback;
    function setref(var v): XNotifyCallback;
    function freeze(p: Boolean = True): XNotifyCallback;

    function new: XNotifyCallback;
    function dup: XNotifyCallback;
    function copy(v: XNotifyCallback): XNotifyCallback;
    function swap(v: XNotifyCallback): XNotifyCallback;
    function tmp: XNotifyCallback;

    // ---- from XNotifyCallback ----
    function isNativeCallback: Boolean;

    // following methods *requires* override
    function setNativeCallback(aCallback: TNotifyCallback): XNotifyCallback;
    procedure launch(aSender: XObject);
  end;

const
  XNotifyCallback_Magic = -1;

function newXNotifyCallback(aCallback: TNotifyCallback): XNotifyCallback;

implementation
{ *************************************************************************** }
function newXNotifyCallback(aCallback: TNotifyCallback): XNotifyCallback;
begin
  xrFbegin(@Result);
  XNotifyCallback.Create.setNativeCallback(aCallback).setref(Result);
  xrend;
end;
{ *************************************************************************** }
// ---- from XNotifyCallback ----
destructor XNotifyCallback.Destroy;
begin
  xrFdestroy;
  inherited;
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XObject ----
// do not change anything in these definitions - begin
function XNotifyCallback.iref: XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).iref); end;
function XNotifyCallback.dref(aFree: Boolean = True): XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).dref(aFree)); end;
function XNotifyCallback.setref(var v): XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).setref(v)); end;
function XNotifyCallback.freeze(p: Boolean = True): XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).freeze(p)); end;
function XNotifyCallback.new: XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).new); end;
function XNotifyCallback.dup: XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).dup); end;
function XNotifyCallback.copy(v: XNotifyCallback): XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).copy(v)); end;
function XNotifyCallback.swap(v: XNotifyCallback): XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).swap(v)); end;
function XNotifyCallback.tmp: XNotifyCallback; begin Result := XNotifyCallback(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XNotifyCallback.v_class_magic: Integer; begin Result := XNotifyCallback_Magic; end;
class function XNotifyCallback.v_class_create: XObject; begin Result := XNotifyCallback.Create; end;
class function XNotifyCallback.v_class_name: String; begin Result := 'XNotifyCallback'; end;
class function XNotifyCallback.v_class_self: XObjectClass; begin Result := Self; end;
class function XNotifyCallback.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XNotifyCallback.v_copy(v: XObject);
var
  o: XNotifyCallback absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  callbackMode := o.callbackMode;
  nativeCallback := o.nativeCallback;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XNotifyCallback.v_swap(v: XObject);
var
  o: XNotifyCallback absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(nativeCallback, o.nativeCallback, SizeOf(TNotifyCallback));
  memSwap(callbackMode, o.callbackMode, SizeOf(callbackMode));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XNotifyCallback.v_getAsString: String;
begin
  xrFbegin;
  Result := inherited v_getAsString;
  Result := Format('%s, Mode:%s', [Result, PyCallbackModeDesc[callbackMode]]);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XNotifyCallback.isNativeCallback: Boolean;
begin
  xrFbegin; Result := (callbackMode = pcmNative) and (Assigned(nativeCallback)); xrend;
end;
{-----------------------------------------------------------------------------}
// following methods *requires* override
function XNotifyCallback.setNativeCallback(aCallback: TNotifyCallback): XNotifyCallback;
begin
  xrFbegin(@Result).setref(Result);
  nativeCallback := aCallback;
  if Assigned(aCallback) then callbackMode := pcmNative;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XNotifyCallback.launch(aSender: XObject);
begin
  try
    xrPbegin([@aSender]);
    if Self = nil then exit;
    if isNativeCallback then begin
      nativeCallback(aSender);
    end
  finally
    xrend;
  end;
end;
{ *************************************************************************** }
//initialization
  // registerMagic(XNotifyCallback);
{ *************************************************************************** }
end.
