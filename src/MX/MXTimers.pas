unit MXTimers;
{ ****************************************************************************
Template:
  2001.07.19
***************************************************************************** }
interface

uses
  SysUtils, Classes, TimerLst,
  MXMemory, MXObject, MXVCLShell, MXNotifyCallback;

type
  XTimer = class(XObject)
  protected
    oTimerCallback: XNotifyCallback;
    iHandle: Integer;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    function v_getAsString: String; override;
  public
    // ---- from XTimer -----
    constructor Create;
    destructor Destroy; override;

    // ---- from XObject ----
    function dref(aFree: Boolean = True): XTimer;
    function iref: XTimer;
    function setref(var v): XTimer;
    function freeze(p: Boolean = True): XTimer;

    function new: XTimer;
    function dup: XTimer;
    function copy(v: XTimer): XTimer;
    function swap(v: XTimer): XTimer;
    function tmp: XTimer;

    // ---- from XTimer ----
    procedure timerEvent(Sender: TObject);

    function alloc(interval: Integer; handler: XNotifyCallback; start: Boolean = True; cycled: Boolean = True): XTimer;
    function release: XTimer;
    function suspend: XTimer;
    function resume: XTimer;
    function isActive: Boolean;
  end;

  XTimers = class(XVCLShell)
  protected
    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;

    // ---- from XVCLShell ----
    class function v_vcl_create: TPersistent; override;
    class function v_vcl_class: TPersistentClass; override;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XTimers;
    function iref: XTimers;
    function setref(var v): XTimers;
    function freeze(p: Boolean = True): XTimers;

    function new: XTimers;
    function dup: XTimers;
    function copy(v: XTimers): XTimers;
    function swap(v: XTimers): XTimers;
    function tmp: XTimers;

    // ---- from XVCLShell ----
    function getVCL: TRxTimerList;
    function createVCL: XTimers;

    // ---- from XTimers -----
    function allocTimer(interval: Integer; handler: TNotifyEvent; start: Boolean = True; cycled: Boolean = True): Integer;
    function releaseTimer(handle: Integer): XTimers;
    function suspendTimer(handle: Integer): XTimers;
    function resumeTimer(handle: Integer): XTimers;
    function isActiveTimer(handle: Integer): Boolean;
    function isAllocatedTimer(handle: Integer): Boolean;

    function suspendAll: XTimers;
    function resumeAll: XTimers;
  end;

const
  XTimer_Magic = -1;
  XTimers_Magic = -1;

var
  defaultTimers: XTimers;

implementation
{ *************************************************************************** }
// ---- from XTimer ----
constructor XTimer.Create;
begin
  xrFcreate;
  inherited;
  iHandle := -1;
  xrend;
end;
{-----------------------------------------------------------------------------}
destructor XTimer.Destroy;
begin
  xrFdestroy;
  if iHandle <> -1 then defaultTimers.releaseTimer(iHandle);
  xrnil(oTimerCallback);
  inherited;
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XObject ----
// do not change anything in these definitions - begin
function XTimer.iref: XTimer; begin Result := XTimer(XObject(Self).iref); end;
function XTimer.dref(aFree: Boolean = True): XTimer; begin Result := XTimer(XObject(Self).dref(aFree)); end;
function XTimer.setref(var v): XTimer; begin Result := XTimer(XObject(Self).setref(v)); end;
function XTimer.freeze(p: Boolean = True): XTimer; begin Result := XTimer(XObject(Self).freeze(p)); end;
function XTimer.new: XTimer; begin Result := XTimer(XObject(Self).new); end;
function XTimer.dup: XTimer; begin Result := XTimer(XObject(Self).dup); end;
function XTimer.copy(v: XTimer): XTimer; begin Result := XTimer(XObject(Self).copy(v)); end;
function XTimer.swap(v: XTimer): XTimer; begin Result := XTimer(XObject(Self).swap(v)); end;
function XTimer.tmp: XTimer; begin Result := XTimer(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XTimer.v_class_magic: Integer; begin Result := XTimer_Magic; end;
class function XTimer.v_class_create: XObject; begin Result := XTimer.Create; end;
class function XTimer.v_class_name: String; begin Result := 'XTimer'; end;
class function XTimer.v_class_self: XObjectClass; begin Result := Self; end;
class function XTimer.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XTimer.v_copy(v: XObject);
var
  o: XTimer absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  oTimerCallback.setref(o.oTimerCallback);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XTimer.v_swap(v: XObject);
var
  o: XTimer absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(oTimerCallback, o.oTimerCallback, SizeOf(oTimerCallback));
  memSwap(iHandle, o.iHandle, SizeOf(iHandle));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimer.v_getAsString: String;
begin
  xrbegin([], []);
  Result := inherited v_getAsString;
  Result := Format('%s, Callback:[%s], Handle:%d', [Result, oTimerCallback.getAsString, iHandle]);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XTimer.timerEvent(Sender: TObject);
begin
  xrFbegin;
  if oTimerCallback <> nil then oTimerCallback.launch(Self);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimer.alloc(interval: Integer; handler: XNotifyCallback; start: Boolean = True; cycled: Boolean = True): XTimer;
begin
  xrPbegin([@handler], @Result).setref(Result);
  handler.setref(oTimerCallback);
  iHandle := defaultTimers.allocTimer(interval, timerEvent, start, cycled);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimer.release: XTimer;
begin
  xrFbegin(@Result).setref(Result);
  if iHandle <> -1 then begin
    defaultTimers.releaseTimer(iHandle);
    iHandle := -1;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimer.suspend: XTimer;
begin
  xrFbegin(@Result).setref(Result);
  if iHandle <> -1 then defaultTimers.suspendTimer(iHandle);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimer.resume: XTimer;
begin
  xrFbegin(@Result).setref(Result);
  if iHandle <> -1 then defaultTimers.resumeTimer(iHandle);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimer.isActive: Boolean;
begin
  xrFbegin(@Result).setref(Result);
  Result := False;
  if iHandle <> -1 then Result := defaultTimers.isActiveTimer(iHandle);
  xrend;
end;
{ *************************************************************************** }
// ---- from XObject ----
// do not change anything in these definitions - begin
function XTimers.iref: XTimers; begin Result := XTimers(XObject(Self).iref); end;
function XTimers.dref(aFree: Boolean = True): XTimers; begin Result := XTimers(XObject(Self).dref(aFree)); end;
function XTimers.setref(var v): XTimers; begin Result := XTimers(XObject(Self).setref(v)); end;
function XTimers.freeze(p: Boolean = True): XTimers; begin Result := XTimers(XObject(Self).freeze(p)); end;
function XTimers.new: XTimers; begin Result := XTimers(XObject(Self).new); end;
function XTimers.dup: XTimers; begin Result := XTimers(XObject(Self).dup); end;
function XTimers.copy(v: XTimers): XTimers; begin Result := XTimers(XObject(Self).copy(v)); end;
function XTimers.swap(v: XTimers): XTimers; begin Result := XTimers(XObject(Self).swap(v)); end;
function XTimers.tmp: XTimers; begin Result := XTimers(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XTimers.v_class_magic: Integer; begin Result := XTimers_Magic; end;
class function XTimers.v_class_create: XObject; begin Result := XTimers.Create; end;
class function XTimers.v_class_name: String; begin Result := 'XTimers'; end;
class function XTimers.v_class_self: XObjectClass; begin Result := Self; end;
class function XTimers.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
// ---- from XVCLShell ----
class function XTimers.v_vcl_class: TPersistentClass; begin Result := TRxTimerList; end;
{-----------------------------------------------------------------------------}
function XTimers.getVCL: TRxTimerList; begin Result := TRxTimerList(XVCLShell(Self).getVCL()); end;
function XTimers.createVCL: XTimers; begin Result := XTimers(XVCLShell(Self).createVCL()); end;
{-----------------------------------------------------------------------------}
class function XTimers.v_vcl_create: TPersistent; begin Result := TRxTimerList.Create(nil); end;
{-----------------------------------------------------------------------------}
// ---- from XTimers -----
function XTimers.allocTimer(interval: Integer; handler: TNotifyEvent; start: Boolean = True; cycled: Boolean = True): Integer;
begin
  xrFbegin;
  makeValid;
  Result := getVCL.Add(handler, interval, cycled);
  getVCL.ItemByHandle(Result).Enabled := start;
  resumeAll;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.releaseTimer(handle: Integer): XTimers;
begin
  xrFbegin(@Result).setref(Result);
  makeValid;
  getVCL.Delete(handle);
  if getVCL.Count <= 0 then suspendAll;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.suspendTimer(handle: Integer): XTimers;
begin
  xrFbegin(@Result).setref(Result);
  makeValid;
  getVCL.ItemByHandle(handle).Enabled := False;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.resumeTimer(handle: Integer): XTimers;
begin
  xrFbegin(@Result).setref(Result);
  makeValid;
  getVCL.ItemByHandle(handle).Enabled := True;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.isActiveTimer(handle: Integer): Boolean;
begin
  xrFbegin;
  makeValid;
  Result := getVCL.ItemByHandle(handle).Enabled;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.isAllocatedTimer(handle: Integer): Boolean;
begin
  xrFbegin;
  makeValid;
  Result := (getVCL.ItemByHandle(handle) <> nil);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.suspendAll: XTimers;
begin
  xrFbegin(@Result).setref(Result);
  makeValid;
  getVCL.Active := False;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XTimers.resumeAll: XTimers;
begin
  xrFbegin(@Result).setref(Result);
  makeValid;
  getVCL.Active := True;
  xrend;
end;
{ *************************************************************************** }
initialization
  XTimers.Create.createVCL.setref(defaultTimers);
{ *************************************************************************** }
finalization
  xrnil(defaultTimers);
{ *************************************************************************** }
end.
