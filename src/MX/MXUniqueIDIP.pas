unit MXUniqueIDIP;

interface

uses
  Windows, SysUtils, WinSock,
  MXStr, MXBigIntDynamic;

function GetUniqID: String;

implementation

var
  aSeq: Integer;
  aIP: LongWord;
  aPID: LongWord;

function UnqPID: LongWord;
begin
  Result := GetCurrentProcessID;
end;

function UnqIP: LongWord;
var
  stemp: array[0..255] of char;
  h: PHostEnt;
  s: string;
begin
  if gethostname(@stemp, 128) <> 0 then
    S := SysErrorMessage(WSAGetLastError);
  h := gethostbyname(@stemp);
  Result := PLongWord(h^.h_addr_list^)^;
end;

function UnqTime: Int64;
var
  dt: TDateTime;
  i8: Int64 absolute dt;
begin
  dt := Now; Result := i8;
end;

function UnqSeq: Integer;
begin
  Inc(aSeq); Result := aSeq;
end;

function GetUniqID: String;
var
  pid, ip: LongWord;
  dt: Int64;
  seq: Integer;
  b: TBigIntValue;
begin
  pid := aPID; ip := aIP; seq := UnqSeq; dt := UnqTime;
  msetlen(b, 18);
  Move(ip, b[0], 4);
  Move(pid, b[4], 4);
  Move(seq, b[8], 2);
  Move(dt, b[10], 8); // 4 + 4 + 2 + 8 = 18

  Result := mmap2str(b, 36);
end;

procedure MyStartup;
var
  WSAData  : TWSAData;
begin
  if WSAStartup($0101, WSAData) <> 0 then
     MessageBox(HWND(nil),PChar(SysErrorMessage(WSAGetLastError)),'B��d',MB_OK);
end;

initialization
  MyStartup();
  aSeq := 0;
  aIP := UnqIP;
  aPID := UnqPID;
finalization
  WSACleanup();
end.
