{$IFDEF DBG_iSA}{$D+,L+,W+,O-}{$ELSE}{$D-,L-,O+,W-}{$ENDIF}
{$A+,B-,C-,E-,F-,G+,H+,I-,J+,K-,M-,N+,P+,Q-,R-,S-,T-,U-,V+,X+,Y-,Z1}
{:in-Slot memory Allocator
@desc Fast memory allocation routines. It's used to alloc large amount of same-sized
  memory block (ie: objects).<br><br>
  <b>Version</b>: <b>0.5.2001.10.01</b><br><br>
  <b>Author</b>: Milosz A. Krajewski (vilge@mud.org.pl)<br><br>
  <b>Advantages</b>:<ul>
  <li>iSAalloc and iSAfree take O(1) time.
  <li>They're 25%-50% faster than AllocMem/FreeMem.
  </ul>
  <b>Disadvantages</b>:<ul>
  <li>Slots are constant-sized so it can generate slack space.
  <li>Every slot take 8 bytes extra
  </ul>
  <b>Revision</b>:<ul>
  <li><b>0.1</b> strongly experimental
  <li><b>0.2</b> multiple block supported
  <li><b>0.3</b> growing block list
  <li><b>0.4</b> profiled
  <li><b>0.5</b> working with unitialized blocks
  </ul>
  <b>Comments</b><ul>
  <li>it's not better, it's just less universal so it can be faster
  <li>thank to Primoz Gabrijelcic (primoz.gabrijelcic@altavista.net)
    for GpProfile, very nice profilig tool
  </ul>}
unit MXiSA;
interface

uses
  Windows, SysUtils, Dialogs,
  MXMemory;

type
  //:pointer to iSA item
  PiSAItem = ^TiSAItem;
  //:pointer to iSA block
  PiSABlock = ^TiSABlock;
  //:pointer to iSA structure
  PiSA = ^TiSA;

  //:iSA item internal structure
  TiSAItem = record
    next: PiSAItem;
    block: PiSABlock;
    buf: array[0..0] of LongWord; // fake buffer, holds user data
  end;

  //:iSA block internal structure
  TiSABlock = record
    prev, next: PiSABlock;
    counter: LongWord; // free block counter
    slotSize: LongWord; // slot size (in bytes)
    blockSize: LongWord; // block size (in slots)
    firstItem: PiSAItem;
    rawDataSize: Longword;
    iSAowner: PiSA;
    buf: array[0..0] of LongWord; // fake buffer, holds multiple TiSAItem
  end;

  {:holds information about allocated block
  @desc You can declare many iSA variables for different slot-sizes.
  @seealso <see routine="iSAinitialize">
  @seealso <see routine="iSAalloc">
  @example <pre>
  var
    iSA_TVertex: TiSA; // there are many vertices allocated in this project
      // and thats why we need fast allocator
    iSA_T2DPoint: TiSA; // each vertex has T2DPoint object linked
    iSA_128: TiSA;
      // there many classes with size about 128 bytes, but there's no need
      // to declare separeted slot-spaces for them</pre>} 
  TiSA = record
    slotSize: LongWord; // slot size (in bytes)
    blockSize: LongWord; // block size (in slots)
    firstBlock: PiSABlock; // pointer to first block contains free slots
  end;

{$IFDEF DBG_iSA}
const
  rawAllocCount: Integer = 0;
  missedBlockCount: Integer = 0;
{$ENDIF}

var
  iSA40: TiSA;
  iSA80: TiSA;
{ *************************************************************************** }
procedure iSAinitialize(iSA: PiSA; aSlotSize, aBlockSize: Longword);
procedure iSAfinalize(iSA: PiSA);
function iSAalloc(iSA: PiSA; Size: Longword): Pointer;
procedure iSAfree(p: Pointer);
{ *************************************************************************** }
implementation
{ *************************************************************************** }
const
  //:"TiSAItem.next = isa_alloc" marks that slot is allocated
  isa_alloc = Pointer(-1);
  //:"TiSAItem.next = isa_last" marks that slot is empty but there is no more slots left
  isa_last = Pointer(-2);
{ *************************************************************************** }
function allocRaw(Size: LongWord): Pointer;
var
  buf: PiSAItem absolute Result;
begin
  {$IFDEF DBG_iSA}Inc(rawAllocCount);{$ENDIF}
  buf := AllocMem(Size + SizeOf(TiSAItem) - SizeOf(LongWord));
  buf^.block := nil; buf^.next := Pointer(-1);
  Result := @(buf^.buf);
end;
{-----------------------------------------------------------------------------}
procedure freeRaw(ptr: Pointer; adjust: Boolean = True);
begin
  if adjust then Dec(LongWord(ptr), SizeOf(TiSAItem) - SizeOf(LongWord)); // adjust item
  FreeMem(ptr);
end;
{-----------------------------------------------------------------------------}
procedure moveBlockFront(block: PiSABlock);
var
  iSA: PiSA;
begin
  iSA := block^.iSAowner;

  if iSA^.firstBlock = block then exit;
  if (iSA^.firstBlock^.counter = 0) or
    ((block^.counter > 0) and (iSA^.firstBlock^.counter > block^.counter)) then begin

    // delete
    if block^.prev <> nil then block^.prev^.next := block^.next;
    if block^.next <> nil then block^.next^.prev := block^.prev;

    // insert
    block^.prev := nil;
    block^.next := iSA^.firstBlock;
    block^.next^.prev := block; // no need to test, always not-nil

    iSA^.firstBlock := block;
  end;
end;
{-----------------------------------------------------------------------------}
procedure moveBlockBack(block: PiSABlock);
var
  iSA: PiSA;
  lastBlock: PiSABlock;
begin
  iSA := block^.iSAowner;

  if iSA^.firstBlock = nil then begin iSA^.firstBlock := block; exit; end;

  lastBlock := iSA^.firstBlock;
  while lastBlock^.next <> nil do lastBlock := lastBlock^.next;
  if lastBlock = block then exit;

  if iSA^.firstBlock = block then iSA^.firstBlock := block^.next;

  // delete
  if block^.prev <> nil then block^.prev^.next := block^.next;
  if block^.next <> nil then block^.next^.prev := block^.prev;

  // insert
  block^.prev := lastBlock;
  block^.next := nil;
  lastBlock^.next := block; // no need to test, always not-nil
end;
{-----------------------------------------------------------------------------}
function allocBlock(iSA: PiSA): PiSABlock;
var
  allocSize: LongWord;
  rawArraySize: LongWord;
  loopPtr: PiSAItem;
  aSlotSize, aBlockSize: Longword;
begin
  aSlotSize := iSA^.slotSize; aBlockSize := iSA^.blockSize;
  rawArraySize := (aSlotSize + SizeOf(TiSAItem) - SizeOf(LongWord))*aBlockSize;
    // size of raw data multiple blocks of with PiSAItem header without fake buffer
  allocSize :=
    SizeOf(TiSABlock) - SizeOf(Longword) + // size without fake buffer
    rawArraySize;

  // memory *MUST* be initialized by zeros
  Result := Pointer(GlobalAlloc(GPTR, allocSize));
  //Result := PiSABlock(AllocMem(allocSize));

  with Result^ do begin
    prev := nil;
    next := nil;
    counter := aBlockSize;
    slotSize := aSlotSize;
    blockSize := aBlockSize;
    rawDataSize := rawArraySize;
    iSAowner := iSA;
    firstItem := @(Result^.buf);
  end;

  if iSA^.firstBlock = nil then iSA^.firstBlock := Result;

  aSlotSize := aSlotSize + SizeOf(TiSAItem) - SizeOf(Longword);

  loopPtr := Result^.firstItem;
  Inc(Longword(loopPtr), (aBlockSize - 1)*aSlotSize);
  loopPtr^.next := isa_last;

  moveBlockFront(Result);
end;
{-----------------------------------------------------------------------------}
procedure freeBlock(block: PiSABlock; forced: Boolean = False);
begin
  if (not forced) and (block^.prev = nil) and (block^.next = nil) then Exit;

  if block^.prev <> nil then block^.prev^.next := block^.next;
  if block^.next <> nil then block^.next^.prev := block^.prev;
  if (block^.iSAowner <> nil) and (block^.iSAowner^.firstBlock = block) then
    block^.iSAowner^.firstBlock := block^.next;
  GlobalFree(LongWord(block));
  //FreeMem(block);
end;
{-----------------------------------------------------------------------------}
{:Allocates size bytes of memory.
@desc Due memory is allocated in slot-space
if size if less than slot-size it will generate slack-space, and if
size is greater than slot-size it will allocate memory by default
memory menager (AllocMem) which slows down whole process.
Memory can be deallocated by <see routine="iSAfree">
@param iSA point to iSA strcuture hold slot-space information (cannot be nil)
@param Size number of bytes
@result pointer to newly allocated slot
@seealso <see routine="iSAinitialize">
@seealso <see routine="iSAfree">
@example <pre>
type
  T2DPoint = class(TObject)
    X, Y: Integer;
    class function NewInstance: TObject; override;
    procedure FreeInstance; override;
    // other T2DPoint members
  end;

var iSA_T2DPoint: TiSA;

class function T2DPoint.NewInstance: TObject;
begin
  Result := TObject(iSAalloc(@iSA_T2DPoint, InstanceSize)); InitInstance(Self);
end;

procedure T2DPoint.FreeInstance; begin iSAfree(Self); end;

begin
  iSAinitialize(@iSA_T2DPoint, T2DPoint.InstanceSize), $10000);
  for i := 0 to 1000 T2DPoint.Create.Free; // I know, it does not make sense
  iSAfinalize(@iSA_T2DPoint);
end;</pre>}
function iSAalloc(iSA: PiSA; Size: Longword): Pointer;
var
  buf: PiSAItem;
  block: PiSABlock;
begin
  Result := nil;

  if Size > iSA^.slotSize then begin Result := allocRaw(Size); exit; end;

  if (iSA^.firstBlock = nil) or (iSA^.firstBlock^.counter = 0) then begin
    allocBlock(iSA);
  end;

  block := iSA^.firstBlock;

  if block^.firstItem <> isa_last then begin
    buf := block^.firstItem;
    Result := @(buf^.buf);
    if buf^.next = nil then
      // last item is marked by isa_last (not nil)
      // nil means "not initialized" and assumes that next slot is free
      block^.firstItem := PiSAItem(Longword(buf) + iSA^.slotSize + SizeOf(TiSAItem) - SizeOf(Longword))
    else
      block^.firstItem := buf^.next;
    buf^.block := block;
    buf^.next := isa_alloc;

    Dec(block^.counter);
    if block^.counter = 0 then moveBlockBack(block);
  end;
end;
{-----------------------------------------------------------------------------}
{:Frees memory allocated by iSAalloc.
@param p pointer to previously allocated slot
@seealso <see routine="iSAalloc">}
procedure iSAfree(p: Pointer);
var
  item: PiSAItem absolute p;
  block: PiSABlock;
begin
  Dec(LongWord(p), SizeOf(TiSAItem) - SizeOf(LongWord)); // adjust item
  block := item^.block;

  if block = nil then begin freeRaw(item, False); exit; end;

  if (item^.next <> isa_alloc) or
    (LongWord(p) - LongWord(@(block^.buf)) >= block^.rawDataSize) then begin
    {$IFDEF DBG_iSA}Inc(missedBlockCount);{$ENDIF}
    exit;
  end;

  item^.next := block^.firstItem;
  block^.firstItem := item;
  Inc(block^.counter);

  if block^.counter = block^.blockSize then
    freeBlock(block)
  else
    if block^.iSAowner^.firstBlock <> block then moveBlockFront(block);
end;
{ *************************************************************************** }
{:Initializes iSA structure.
@desc Must be called before using iSA. Often used in initialization section.
@param iSA points to TiSA structure (cannot be nil)
@param aSlotSize size of single slot, ie: SizeOf(TMyObject)
@param aBlockSize size of block (in slots) ie: 1MB/aSlotSize
@example <pre>
var
  iSA80: TiSA;
begin
  iSAinitialize(@iSA80, 80, $10000);
  // working with 80-byte objects
  iSAfinalize(@iSA);
end.</pre>
@seealso <see type="TiSA">
@seeAlso <see routine="iSAfinalize">}
procedure iSAinitialize(iSA: PiSA; aSlotSize, aBlockSize: Longword);
begin
  {$IFNDEF DBG_iSA}
  if aSlotSize < 32 then aSlotSize := 32;
  if aBlockSize < 1024 then aBlockSize := 1024;
  aBlockSize := (aBlockSize + 31) and not 31;
  {$ENDIF}
  with iSA^ do begin
    slotSize := aSlotSize;
    blockSize := aBlockSize;
    firstBlock := nil;
  end;
end;
{-----------------------------------------------------------------------------}
{:Finalizes iSA structure.
@desc Must be called to cleanup memory. Often used in finalization section.
@param iSA points to TiSA structure (cannot be nil)
@seealso <see routine="iSAinitialize">}
procedure iSAfinalize(iSA: PiSA); begin
  while iSA^.firstBlock <> nil do freeBlock(iSA^.firstBlock, True);
end;
{ *************************************************************************** }
initialization
  iSAinitialize(@iSA40, 40, $10000);
  iSAinitialize(@iSA80, 80, $10000);
{ ************************************************************************** }
finalization
  iSAfinalize(@iSA40);
  iSAfinalize(@iSA80);
end.
