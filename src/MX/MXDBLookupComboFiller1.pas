unit MXDBLookupComboFiller1;
{ ****************************************************************************
Template:
  MXObject 2001.06.20
***************************************************************************** }
interface

uses
  SysUtils, RxQuery, StdCtrls, DbTables, Classes,
  MXMemory, MXObject, MXTimers, ComCtrls;
{ *************************************************************************** }
type
  XDBLookupComboFiller1 = class(XObject)
  protected
    oDatabase: TDatabase;

    bActive: Boolean;
    bChanged: Boolean;
    sSQL: String;
    fLastTime: TDateTime;
    fTimeDiff: Double;
    iCharLimit: Integer;
    sLastText: String;

    oCombo: TComboBoxEx;
    timer: Integer;

    handlerEnter, handlerExit, handlerChange: TNotifyEvent;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    function v_getAsString: String; override;

    // ---- from XDBLookupComboFiller -----
    procedure p_handlerEnter(Sender: TObject);
    procedure p_handlerExit(Sender: TObject);
    procedure p_handlerChange(Sender: TObject);
    procedure p_handlerTimer(Sender: TObject);
  public
    // ---- from XDBLookupComboFiller -----
    constructor Create;
    destructor Destroy; override;

    // ---- from XObject ----
    function dref(aFree: Boolean = True): XDBLookupComboFiller1;
    function iref: XDBLookupComboFiller1;
    function setref(var v): XDBLookupComboFiller1;
    function freeze(p: Boolean = True): XDBLookupComboFiller1;

    function new: XDBLookupComboFiller1;
    function dup: XDBLookupComboFiller1;
    function copy(v: XDBLookupComboFiller1): XDBLookupComboFiller1;
    function swap(v: XDBLookupComboFiller1): XDBLookupComboFiller1;
    function tmp: XDBLookupComboFiller1;

    // ---- from XDBLookupComboFiller -----
    function setDatabase(db: TDatabase): XDBLookupComboFiller1;
    function getDatabase: TDatabase;
    function setLookupSQL(const sql: String): XDBLookupComboFiller1;
    function getLookupSQL: String;
    function setDelay(delay: Double): XDBLookupComboFiller1;
    function getDelay: Double;
    function setCharLimit(limit: Integer): XDBLookupComboFiller1;
    function getCharLimit: Integer;

    function allocTimer(interval: Integer = 250): XDBLookupComboFiller1;
    function releaseTimer: XDBLookupComboFiller1;
    function suspendTimer: XDBLookupComboFiller1;
    function resumeTimer: XDBLookupComboFiller1;
    function isActiveTimer: Boolean;
    function isAllocatedTimer: Boolean;

    procedure notifyEnter;
    procedure notifyExit;
    procedure notifyChange(const text: String); overload;
    procedure notifyChange(combo: TComboBoxEx); overload;
    function notifyIdle(combo: TComboBoxEx; aForce: Boolean = False): Boolean;

    function hook(combo: TComboBoxEx): XDBLookupComboFiller1;
    function force(combo: TComboBoxEx = nil): XDBLookupComboFiller1;
  end;

const
  XDBLookupComboFiller_Magic = -1;

implementation
{ *************************************************************************** }
// ---- from XDBLookupComboFiller ----
constructor XDBLookupComboFiller1.Create;
begin
  xrFcreate;
  inherited;
  fLastTime := Now;
  fTimeDiff := 1 / SecsPerDay;
  iCharLimit := 4;
  timer := -1;
  xrend;
end;
{-----------------------------------------------------------------------------}
destructor XDBLookupComboFiller1.Destroy;
begin
  xrFdestroy;
  releaseTimer;
  inherited;
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XObject ----
// do not change anything in these definitions - begin
function XDBLookupComboFiller1.iref: XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).iref); end;
function XDBLookupComboFiller1.dref(aFree: Boolean = True): XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).dref(aFree)); end;
function XDBLookupComboFiller1.setref(var v): XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).setref(v)); end;
function XDBLookupComboFiller1.freeze(p: Boolean = True): XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).freeze(p)); end;
function XDBLookupComboFiller1.new: XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).new); end;
function XDBLookupComboFiller1.dup: XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).dup); end;
function XDBLookupComboFiller1.copy(v: XDBLookupComboFiller1): XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).copy(v)); end;
function XDBLookupComboFiller1.swap(v: XDBLookupComboFiller1): XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).swap(v)); end;
function XDBLookupComboFiller1.tmp: XDBLookupComboFiller1; begin Result := XDBLookupComboFiller1(XObject(Self).tmp); end;
{-----------------------------------------------------------------------------}
class function XDBLookupComboFiller1.v_class_magic: Integer; begin Result := XDBLookupComboFiller_Magic; end;
class function XDBLookupComboFiller1.v_class_create: XObject; begin Result := XDBLookupComboFiller1.Create; end;
class function XDBLookupComboFiller1.v_class_name: String; begin Result := 'XDBLookupComboFiller'; end;
class function XDBLookupComboFiller1.v_class_self: XObjectClass; begin Result := Self; end;
class function XDBLookupComboFiller1.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.v_copy(v: XObject);
// you can override this, but you can also delete this definition
// if there is no new fields to copy
var
  o: XDBLookupComboFiller1 absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  oDatabase := o.oDatabase;
  bActive := o.bActive;
  bChanged := o.bChanged;
  sSQL := o.sSQL;
  fLastTime := o.fLastTime;
  fTimeDiff := o.fTimeDiff;
  sLastText := o.sLastText;
  iCharLimit := o.iCharLimit;

  // these field are not copied
  // oCombo
  // timer
  // handlerEnter, handlerExit, handlerChange

  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.v_swap(v: XObject);
var
  o: XDBLookupComboFiller1 absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(bActive, o.bActive, SizeOf(bActive));
  memSwap(bChanged, o.bChanged, SizeOf(bChanged));
  memSwap(iCharLimit, o.iCharLimit, SizeOf(iCharLimit));
  memSwap(oDatabase, o.oDatabase, SizeOf(oDatabase));
  memSwapString(sSQL, o.sSQL);
  memSwap(fLastTime, o.fLastTime, SizeOf(fLastTime));
  memSwap(fTimeDiff, o.fTimeDiff, SizeOf(fTimeDiff));
  memSwapString(sLastText, o.sLastText);

  memSwap(oCombo, o.oCombo, SizeOf(oCombo));
  memSwap(timer, o.timer, SizeOf(timer));
  { TODO -clater : swap handlers }
  // memSwap(handlerEnter, o.handlerEnter, SizeOf(handlerEnter));
  // memSwap(handlerExit, o.handlerExit, SizeOf(handlerExit));
  // memSwap(handlerChange, o.handlerChange, SizeOf(handlerChange));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.v_getAsString: String;
begin
  xrbegin([], []);
  Result := inherited v_getAsString;
  Result := Format('%s, SQL:[%s], timer:%d', [Result, sSQL, timer]);
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XDBLookupComboFiller -----
function XDBLookupComboFiller1.setDatabase(db: TDatabase): XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  oDatabase := db;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.getDatabase: TDatabase;
begin
  xrFbegin; Result := oDatabase; xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.setLookupSQL(const sql: String): XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result); sSQL := sql; xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.getLookupSQL: String;
begin
  xrFbegin; Result := sSQL; xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.setDelay(delay: Double): XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if delay <= 0 then delay := 0.001;
  fTimeDiff := delay / SecsPerDay;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.getDelay: Double;
begin
  xrFbegin;
  Result := fTimeDiff * SecsPerDay;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.setCharLimit(limit: Integer): XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if limit < 0 then limit := 0;
  iCharLimit := limit;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.getCharLimit: Integer;
begin
  xrFbegin;
  Result := iCharLimit;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.notifyEnter;
begin
  xrFbegin; bActive := True; fLastTime := Now; xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.notifyExit;
begin
  xrFbegin; bActive := False; xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.notifyChange(const text: String);
begin
  xrFbegin;
  if bActive then begin
    fLastTime := Now; sLastText := text; bChanged := True;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.notifyChange(combo: TComboBoxEx);
begin
  xrFbegin;
  if bActive then begin
    fLastTime := Now;
    sLastText := System.Copy(combo.Text, 1, combo.SelStart);
    bChanged := True;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.notifyIdle(combo: TComboBoxEx; aForce: Boolean = False): Boolean;
var
  q: TRXQuery;
  l: TStringList;
  ss: Integer;
begin
  try
    xrFbegin;
    Result := False;

    if (aForce) or ((bActive) and ((bChanged) or ((combo.Items.Count = 0) and (iCharLimit = 0))) and
    (Now >= fLastTime + fTimeDiff) and (Length(sLastText) >= iCharLimit)) then begin
      bChanged := False;

      fLastTime := Now;

      // creating query
      q := TRxQuery.Create(nil);
      if oDatabase <> nil then q.DatabaseName := oDatabase.DatabaseName;
      q.SQL.Text := Format(sSQL, [sLastText]);

      // gathering
      q.Open; q.First;
      l := TStringList.Create;
      while not q.Eof do begin l.Add(q.Fields[0].AsString); q.Next; end;
      q.Close; q.Free;

      // assign phase
      if (l.Count > 0) and (combo <> nil) then begin
        ss := combo.SelStart;
        combo.Items.Clear;
        combo.Items.Assign(l);
        combo.DroppedDown := true;
        //RAV
        //combo.Text := l[0];
        //combo.SelStart := ss; combo.SelLength := Length(combo.Text) - ss;
        Result := True;
      end;

      l.Free;
    end;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.p_handlerEnter(Sender: TObject);
begin
  xrFbegin;
  if Assigned(handlerEnter) then handlerEnter(Sender);
  if Sender is TComboBoxEx then notifyEnter;
  resumeTimer;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.p_handlerExit(Sender: TObject);
begin
  xrFbegin;
  if Assigned(handlerExit) then handlerExit(Sender);
  if Sender is TComboBoxEx then notifyEnter;
  suspendTimer;
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.p_handlerChange(Sender: TObject);
begin
  xrFbegin;
  if Assigned(handlerChange) then handlerChange(Sender);
  if Sender is TComboBoxEx then notifyChange(Sender as TComboBoxEx);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XDBLookupComboFiller1.p_handlerTimer(Sender: TObject);
begin
  if oCombo = nil then exit;
  xrFbegin; notifyIdle(oCombo); xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.allocTimer(interval: Integer = 250): XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then defaultTimers.releaseTimer(timer);
  timer := defaultTimers.allocTimer(interval, p_handlerTimer, False);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.releaseTimer: XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then begin
    defaultTimers.releaseTimer(timer);
    timer := -1;
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.suspendTimer: XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then defaultTimers.suspendTimer(timer);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.resumeTimer: XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if timer <> -1 then defaultTimers.resumeTimer(timer);
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.isActiveTimer: Boolean;
begin
  xrFbegin;
  if timer = -1 then
    Result := False
  else begin
    Result := defaultTimers.isActiveTimer(timer);
  end;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.isAllocatedTimer: Boolean;
begin
  xrFbegin;
  Result := (timer <> -1) and (defaultTimers.isAllocatedTimer(timer));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.hook(combo: TComboBoxEx): XDBLookupComboFiller1;
var
  cname: String;
begin
  try
    xrFbegin(@Result).setref(Result);

    oCombo := combo;

    if combo = nil then exit;
    cname := combo.Name;
    if cname = '' then cname := '<noname>';
    if (Assigned(handlerEnter)) or (Assigned(handlerExit)) or (Assigned(handlerChange)) then
      xrraise(Exception.CreateFmt('Cannot hook events on %s. Already hooked.', [cname]));

    handlerEnter := combo.OnEnter; combo.OnEnter := p_handlerEnter;
    handlerExit := combo.OnExit; combo.OnExit := p_handlerExit;
    handlerChange := combo.OnChange; combo.OnChange := p_handlerChange;

    if isAllocatedTimer then resumeTimer;
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XDBLookupComboFiller1.force(combo: TComboBoxEx = nil): XDBLookupComboFiller1;
begin
  xrFbegin(@Result).setref(Result);
  if combo = nil then combo := oCombo;
  if combo <> nil then notifyIdle(combo, True);
  xrend;
end;
{ *************************************************************************** }
initialization
  // registerMagic(XDBLookupComboFiller);
{ *************************************************************************** }
end.
