unit MXGEDebug;

interface

{$IFDEF DEBUG}
uses
  Windows, SysUtils, Forms, Registry, Dialogs;

function  GetLastErrorText: String;
procedure SendDebugEx(const Msg: String; MType:TMsgDlgType);
procedure SendDebug(const Msg: String);
procedure ClearDebug;
function  StartDebugWin:hWnd;

function  BooleanToStr(B: Boolean): String;
function  TPointToStr (P: TPoint) : String;
function  TRectToStr  (R: TRect)  : String;

implementation

function BooleanToStr(B: Boolean): String;
begin
  if B
    then Result := 'True'
    else Result := 'False';
end;

function TRectToStr(R: TRect): String;
begin
  Result := Format('(%d, %d, %d, %d)', [R.Left, R.Top, R.Right, R.Bottom]);
end;

function TPointToStr(P: TPoint): String;
begin
  Result := Format('(%d, %d)', [P.X, P.Y]);
end;

function GetLastErrorText: String;
var
  buf: PChar;
begin
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
                nil, GetLastError(), 0, @buf, 0, nil);
  Result := StrPas(buf);
  LocalFree(Integer(buf));
end;

function StartDebugWin:hWnd;
var RegIni:TRegIniFile;
  Count:Integer;
  Filename: String;
begin
  Result:=0;
  Count:=0;
  RegIni:=TRegIniFile.Create('\Software\GExperts');
  try
    Filename:=RegIni.ReadString('Debug','FilePath','');
  finally
    RegIni.Free;
  end;
  if (FileName<>'') and
     (WinExec(PChar(Filename),SW_SHOW)>31) then
    while (Result=0) and (Count<1000) do begin
      Application.ProcessMessages;
      Result:=FindWindow('TfmDebug', nil);
      inc(Count);
    end;
end;

procedure ClearDebug;
var
  CDS:TCopyDataStruct;
  DebugWin:hWnd;
  PMsg:PChar;
Begin
  DebugWin:=FindWindow('TfmDebug', nil);
  if DebugWin=0 then
    DebugWin:=StartDebugWin;
  if DebugWin<>0 then Begin
    CDS.cbData:=7;
    PMsg:=StrNew(' Clear');
    PMsg[0]:=#3;
    Try
      CDS.lpData:=PMsg;
      SendMessage(DebugWin, WM_COPYDATA, 0, LParam(@CDS));
    Finally
      StrDispose(PMsg);
    end;
  end;
End;

procedure SendDebugEx(const Msg: String; MType:TMsgDlgType);
var
  CDS:TCopyDataStruct;
  DebugWin:hWnd;
  PMsg:PChar;

  procedure CopyMessage;
  var i: Integer;
  Begin
    PMsg[0]:=#1;
    PMsg[1]:=Char(ord(MType)+1); {Add 1 to avoid 0}
    for i:=1 to length(Msg) do
      PMsg[1+i]:=Msg[i];
    PMsg[2+length(Msg)]:=#0; {Terminate string}
  End;

begin
  DebugWin:=FindWindow('TfmDebug', nil);
  if DebugWin=0 then
    DebugWin:=StartDebugWin;
  if DebugWin<>0 then begin
    CDS.cbData:=Length(Msg)+3;
    PMsg:=StrAlloc(Length(Msg)+3);
    Try
      CopyMessage;
      CDS.lpData:=PMsg;
      SendMessage(DebugWin, WM_COPYDATA, 0, LParam(@CDS));
    Finally
      StrDispose(PMsg);
    end;
  end;
end;

procedure SendDebug(const Msg: String);
begin SendDebugEx(Msg, mtInformation); end;

{$ELSE}

implementation

{$ENDIF}

end.
