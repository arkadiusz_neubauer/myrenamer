unit MXVCLShell;
{ ****************************************************************************
Purpose:
  Shell for TPersistent VCL object
Version (major.minor.year.month.day):
  0.4.2001.07.19
Author:
  Milosz A. Krajewski <vilge@mud.org.pl>
Capabilities:
  + copy/swap reimplemented
  + abstract load/save methods
Todo:
  - there is something for sure
Whats new:
  0.4 + owner management
***************************************************************************** }
interface

uses
  Classes, SysUtils, MXMemory, MXObject;
{ *************************************************************************** }
type
  TPersistentClass = class of TPersistent;

  XVCLShell = class(XObject) // you can change XObject for any other XObject based type
  protected
    oVCL: TPersistent;
    bVCLOwner: Boolean;

    // ---- from XObject ----
    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    procedure v_load(s: XStream); override;
    procedure v_store(s: XStream); override;
    function v_getAsString: String; override;

    // ---- from XVCLShell ----
    class function v_vcl_create: TPersistent; virtual;
    class function v_vcl_class: TPersistentClass; virtual;

    procedure makeValid;
  public
    // ---- from XVCLShell ----
    function getVCL: TPersistent;
    function setVCL(o: TPersistent): XVCLShell;
    function getVCLHandle: Integer;
    function setVCLHandle(h: Integer): XVCLShell;
    function createVCL: XVCLShell;
    function getOwner: Boolean;
    function setOwner(o: Boolean = True): XVCLShell;
  end;

const
  XVCLShell_Magic = -1;

implementation
{ *************************************************************************** }
{-----------------------------------------------------------------------------}
class function XVCLShell.v_class_magic: Integer; begin Result := XVCLShell_Magic; end;
class function XVCLShell.v_class_create: XObject; begin Result := XVCLShell.Create; end;
class function XVCLShell.v_class_name: String; begin Result := 'XVCLShell'; end;
class function XVCLShell.v_class_self: XObjectClass; begin Result := Self; end;
class function XVCLShell.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
procedure XVCLShell.v_load(s: XStream);
begin
  try
    xrbegin([@s], []);
    inherited v_load(s);
    // raise EStreamingError.Create('XVCLShell is not streamable');
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XVCLShell.v_store(s: XStream);
begin
  try
    xrbegin([@s], []);
    inherited v_store(s);
    // raise EStreamingError.Create('XVCLShell is not streamable');
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XVCLShell.v_copy(v: XObject);
var
  o: XVCLShell absolute v;
begin
  xrbegin([@v], []);
  inherited v_copy(o);
  makeValid;
  oVCL.Assign(o.oVCL);
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XVCLShell.v_swap(v: XObject);
var
  o: XVCLShell absolute v;
begin
  xrbegin([@v], []);
  inherited v_swap(o);
  memSwap(oVCL, o.oVCL, SizeOf(o.oVCL));
  xrend;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.v_getAsString: String;
begin
  xrbegin([], []);
  Result := inherited v_getAsString;
  Result := Format('%s, Owner: %s', [Result, BooleanDesc[bVCLOwner]]);
  xrend;
end;
{-----------------------------------------------------------------------------}
// ---- from XVCLShell ----
class function XVCLShell.v_vcl_create: TPersistent; begin Result := TPersistent.Create; end;
class function XVCLShell.v_vcl_class: TPersistentClass; begin Result := TPersistent; end;
{-----------------------------------------------------------------------------}
function XVCLShell.getVCL: TPersistent;
begin
  if Self = nil then begin Result := nil; exit; end;
  xrFbegin(); Result := oVCL; xrend;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.setVCL(o: TPersistent): XVCLShell;
begin
  try
    xrFbegin(@Result).setref(Result);
    if o = oVCL then exit;

    with o as v_vcl_class do begin
      if bVCLOwner then oVCL.Free;
      oVCL := o;
      bVCLOwner := False;
    end;

  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.getVCLHandle: Integer;
begin
  xrFbegin(); Result := Integer(getVCL); xrend;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.setVCLHandle(h: Integer): XVCLShell;
begin
  xrFbegin(@Result).setref(Result);
  setVCL(TPersistent(h));
  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XVCLShell.makeValid;
begin
  if oVCL <> nil then exit;
  xrFbegin(); createVCL; xrend;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.createVCL: XVCLShell;
begin
  xrFbegin(@Result).setref(Result);
  setVCL(v_vcl_create).setOwner;
  xrend;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.getOwner: Boolean;
begin
  xrFbegin(); Result := bVCLOwner; xrend;
end;
{-----------------------------------------------------------------------------}
function XVCLShell.setOwner(o: Boolean = True): XVCLShell;
begin
  xrFbegin(@Result).setref(Result); bVCLOwner := o; xrend;
end;
{ *************************************************************************** }
end.
