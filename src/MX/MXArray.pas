unit MXArray;
{ ****************************************************************************
Purpose:
  Dynamic arrays of xref objects
Version (major.minor.year.month.day):
  2.1.2001.09.16
Author:
  Milosz A. Krajewski <vilge@mud.org.pl>
Capabilities:
  + xref object compatibile
  + insert item, insert array
  + append item, append array
  + delete, slice, cut
  + dynamic length
  + sort (quick, insertion, selection)
  + search (linear, binary, interpolation)
  + forEach, filter, findFirst, findNext
  + supports Python for Delphi
What's new:
  2.1 + forEach, filter, findFirst, findNext
Todo:
  - exponential growth
  - nothing special i think
***************************************************************************** }
interface

uses
  SysUtils, Math,
  MXiSA, MXMemory, MXObject, MXCompare;

type
  TASearchStrategy = (xassScan, xassLinear, xassBinary, xassInterpolation);

  XArray = class(XObject)
  protected
    iCount, iLimit, iDelta: Integer;
    pArr: PByteMem;
    oCompareObject: XObjectCompare;

    class function v_class_magic: Integer; override;
    class function v_class_create: XObject; override;
    class function v_class_name: String; override;
    class function v_class_self: XObjectClass; override;
    class function v_class_parent: XObjectClass; override;
    procedure v_copy(v: XObject); override;
    procedure v_swap(v: XObject); override;
    procedure v_load(s: XStream); override;
    procedure v_store(s: XStream); override;
    function v_getAsString: String; override;

    procedure adjustLimit(c: Integer);
    procedure adjustIndexBeforeDelete(var i, c: Integer);
    procedure adjustIndexBeforeInsert(var i: Integer);
    procedure adjustBeforeSearch(var aLo, aHi: Integer);

    function setLimit(l: Integer): Integer;
    function setCount(c: Integer): Integer;
    function incCount(c: Integer = 1): Integer;
    function decCount(c: Integer = 1): Integer;

    function areExact(IX, IY: XObject): Boolean;
    function exactNeighbourSearch(aObject: XObject; aStart, aLo, aHi: Integer): Integer;

    function compareItems(aItem, bItem: XObject): Integer;
    function estimateItem(aItem: XObject): Double;

    function at(i: Integer): PXObject;
    function width(i: Integer): Integer;
  public
    // ---- from XObject ----
    function dref(aFree: Boolean = True): XArray;
    function iref: XArray;
    function setref(var v): XArray;
    function freeze(p: Boolean = True): XArray;

    function new: XArray;
    function dup: XArray;
    function copy(v: XArray): XArray;
    function swap(v: XArray): XArray;

    // ---- from XArray ----
    constructor Create;
    destructor Destroy; override;

    function uniqueItems: XArray;

    function getCount: Integer;
    function getLimit: Integer;
    function setDelta(d: Integer): XArray;
    function getDelta: Integer;

    function getItem(i: Integer): XObject;
    function setItem(i: Integer; o: XObject): XArray;

    function addEmpty(c: Integer = 1): XArray;
    function addItem(o: XObject): XArray;
    function addArray(o: XArray): XArray;

    function insertEmpty(i: Integer; c: Integer = 1): XArray;
    function insertItem(i: Integer; o: XObject): XArray;
    function insertArray(i: Integer; o: XArray): XArray;

    function swapItems(IX, IY: Integer): XArray;
    function quickSort(ALo: Integer = -1; AHi: Integer = -1): XArray;
    function insertionSort(ALo: Integer = -1; AHi: Integer = -1): XArray;
    function selectionSort(ALo: Integer = -1; AHi: Integer = -1): XArray;

    function delete(i: Integer; c: Integer = 1): XArray;
    function slice(i: Integer; c: Integer = 1): XArray;
    function extract(i: Integer; c: Integer = 1): XArray;
    function cut(i: Integer; c: Integer = 1): XArray;
    function clear: XArray;
    function invert: XArray;
    function setCapacity(i: Integer): XArray;

    function isSorted: Boolean;

    function scanSearch(aObject: XObject; Exact: Boolean = False; aLo: Integer = -1; aHi: Integer = -1): Integer;
    function linearSearch(aObject: XObject; Exact: Boolean = False; aLo: Integer = -1; aHi: Integer = -1): Integer;
    function binarySearch(aObject: XObject; Exact: Boolean = False; aLo: Integer = -1; aHi: Integer = -1): Integer;
    function interpolationSearch(aObject: XObject; Exact: Boolean = False; aLo: Integer = -1; aHi: Integer = -1): Integer;
    function search(aObject: XObject; Exact: Boolean = False; aLo: Integer = -1; aHi: Integer = -1; aSearchStrategy: TASearchStrategy = xassBinary): Integer;

    function searchInsert(o: XObject; Exact: Boolean = False; aAllowDuplicates: Boolean = True; aStrategy: TASearchStrategy = xassBinary): XArray;
    function searchDelete(o: XObject; Exact: Boolean = False; aStrategy: TASearchStrategy = xassBinary): Boolean;

    function encodeSearchResult(aResult: Integer; aFound: Boolean): Integer;
    function decodeSearchResult(aResult: Integer): Integer;

    function setCompareObject(aCompareObject: XObjectCompare): XArray;
    function getCompareObject: XObjectCompare;

    function forEach(i: XObjectIterator): XArray;

    function filter(f: XObjectFilter; negativeFilter: Boolean = False): XArray;
    function filterDelete(f: XObjectFilter): XArray;
    function filterCopy(f: XObjectFilter): XArray;
    function filterCut(f: XObjectFilter): XArray;

    function filterCount(f: XObjectFilter): Integer;
    function findFirst(f: XObjectFilter): Integer;
    function findNext(f: XObjectFilter; aStart: Integer = 0): Integer;
    function findLast(f: XObjectFilter): Integer;
  end;

const
  XArray_Magic = $5000;

function decodeSearchResult(aResult: Integer): Integer;

implementation
{ ************************************************************************** }
function decodeSearchResult(aResult: Integer): Integer;
begin
  if aResult < 0 then Result := -aResult - 1 else Result := aResult;
end;
{ ************************************************************************** }
// do not change anything in these definitions - begin
function XArray.iref: XArray; begin Result := XArray(XObject(Self).iref); end;
function XArray.dref(aFree: Boolean = True): XArray; begin Result := XArray(XObject(Self).dref(aFree)); end;
function XArray.setref(var v): XArray; begin Result := XArray(XObject(Self).setref(v)); end;
function XArray.freeze(p: Boolean = True): XArray; begin Result := XArray(XObject(Self).freeze(p)); end;
function XArray.new: XArray; begin Result := XArray(XObject(Self).new); end;
function XArray.dup: XArray; begin Result := XArray(XObject(Self).dup); end;
function XArray.copy(v: XArray): XArray; begin Result := XArray(XObject(Self).copy(v)); end;
function XArray.swap(v: XArray): XArray; begin Result := XArray(XObject(Self).swap(v)); end;
{-----------------------------------------------------------------------------}
class function XArray.v_class_magic: Integer; begin Result := XArray_Magic; end;
class function XArray.v_class_create: XObject; begin Result := XArray.Create; end;
class function XArray.v_class_name: String; begin Result := 'XArray'; end;
class function XArray.v_class_self: XObjectClass; begin Result := Self; end;
class function XArray.v_class_parent: XObjectClass; begin Result := inherited v_class_self; end;
// do not change anything in these definitions - end
{-----------------------------------------------------------------------------}
constructor XArray.Create;
begin
  xrFcreate();

  inherited Create;
  pArr := nil;
  setCount(0); setLimit(0); setDelta(4);
  setCompareObject(nil);

  xrend;
end;
{----------------------------------------------------------------------------}
destructor XArray.Destroy;
begin
  xrFdestroy();
  clear;
  setCompareObject(nil);
  inherited;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.v_getAsString: String;
var
  sItems, sI: String;
  i: Integer;
  itm: XObject;
begin
  Result := 'None'; if Self = nil then exit;

  xrbegin([], [@itm]);

  Result := inherited v_getAsString;
  sItems := '';

  for i := 0 to getCount - 1 do begin
    getItem(i).setref(itm);
    if itm = nil
      then sI := 'None'
      else sI := itm.getAsString;
    sItems := sItems + Format('[Idx:%d, Value:[%s]], ', [i, sI]);
  end;

  if Length(sItems) > 0 then SetLength(sItems, Length(sItems) - 2);

  Result := Format('%s, Count:%d, Limit:%d, Delta:%d, Items:[%s]', [Result, getCount, getLimit, getDelta, sItems]);

  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XArray.v_load(s: XStream);
var
  i: Integer;
begin
  try
    xrPbegin([@s]);
    clear;
    inherited v_load(s);
    setDelta(s.readInteger); // set delta
    setCapacity(s.readInteger); // set capacity (count)
    s.readXObject(oCompareObject);
    for i := 0 to getCount - 1 do setItem(i, s.readXObject);
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XArray.v_store(s: XStream);
var
  i: Integer;
begin
  try
    xrPbegin([@s]);
    inherited v_store(s);
    s.writeInteger(iDelta);
    s.writeInteger(iCount);
    s.writeXObject(oCompareObject);
    for i := 0 to iCount - 1 do s.writeXObject(getItem(i));
  finally
    xrend;
  end;
end;
{-----------------------------------------------------------------------------}
procedure XArray.v_copy(v: XObject);
var
  o: XArray absolute v;
  i: Integer;
begin
  xrPbegin([@v]);

  inherited v_copy(o);

  clear;
  setLimit(o.getLimit);
  setCount(o.getCount);
  setDelta(o.getDelta);
  setCompareObject(o.getCompareObject);

  ReAllocMem(pArr, width(getLimit));
  Move(o.pArr^, pArr^, width(getCount));
  for i := 0 to getCount - 1 do at(i)^.iref;

  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XArray.v_swap(v: XObject);
var
  o: XArray absolute v;
begin
  xrPbegin([@v]);

  inherited v_swap(o);

  memSwap(iLimit, o.iLimit, SizeOf(iLimit));
  memSwap(iCount, o.iCount, SizeOf(iCount));
  memSwap(iDelta, o.iDelta, SizeOf(iDelta));
  memSwap(pArr, o.pArr, SizeOf(pArr));
  memSwap(oCompareObject, o.oCompareObject, SizeOf(oCompareObject));

  xrend;
end;
{-----------------------------------------------------------------------------}
procedure XArray.adjustLimit(c: Integer);
var
  aLimit: Integer;
begin
  xrFbegin();

  aLimit := ((c + getDelta - 1) div getDelta) * getDelta;
  if aLimit <> getLimit then
    ReAllocMem(pArr, width(setLimit(aLimit)));

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.at(i: Integer): PXObject;
begin
  xrFbegin();

  if (i < 0) or (i >= getCount) then
    Result := nil
  else
    Result := @pArr^[i*SizeOf(XObject)];

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.width(i: Integer): Integer;
begin
  xrFbegin();
  Result := i*SizeOf(XObject);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.setCapacity(i: Integer): XArray;
begin
  try
    xrFbegin(@Result).setref(Result);
    if i = getCount then exit;
    if i > getCount then addEmpty(i - getCount) else slice(0, i);
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
procedure XArray.adjustIndexBeforeInsert(var i: Integer);
begin
  xrFbegin();
  if i < 0 then i := getCount + i;
  if i < 0 then i := 0;
  xrend;
end;
{----------------------------------------------------------------------------}
procedure XArray.adjustIndexBeforeDelete(var i, c: Integer);
begin
  xrFbegin();
  if i < 0 then i := getCount + i;
  if i < 0 then i := 0;
  if c < 0 then begin Inc(i, c); c := -c; end;
  if i < 0 then begin Inc(c, i); i := 0; end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.setCompareObject(aCompareObject: XObjectCompare): XArray;
begin
  xrFbegin(@Result).setref(Result);
  aCompareObject.setref(oCompareObject);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.getCompareObject: XObjectCompare;
begin
  xrFbegin(@Result);
  oCompareObject.setref(Result);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.compareItems(aItem, bItem: XObject): Integer;
begin
  xrPbegin([@aItem, @bItem]);

  if oCompareObject <> nil then
    Result := oCompareObject.compare(aItem, bItem)
  else
    Result := 0;

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.estimateItem(aItem: XObject): Double;
begin
  xrPbegin([@aItem]);

  if oCompareObject <> nil then
    Result := oCompareObject.estimate(aItem)
  else
    Result := 0;

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.uniqueItems: XArray;
var
  i: Integer;
begin
  xrFbegin(@Result).setref(Result);
  for i := 0 to getCount - 1 do xrunq(at(i)^);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.addEmpty(c: Integer): XArray;
begin
  xrFbegin(@Result);
  insertEmpty(iCount, c).setref(Result);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.addItem(o: XObject): XArray;
begin
  xrPbegin([@o], @Result);
  insertItem(iCount, o).setref(Result);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.addArray(o: XArray): XArray;
begin
  xrPbegin([@o], @Result);
  insertArray(iCount, o).setref(Result);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.insertEmpty(i: Integer; c: Integer = 1): XArray;
var
  r: Integer;
begin
  try
    xrFbegin(@Result).setref(Result);
    if c <= 0 then exit;

    adjustIndexBeforeInsert(i);
    r := iCount - i;
    if i > iCount then setCount(i);

    incCount(c); adjustLimit(iCount);

    Move(at(i)^, at(i + c)^, width(r));
    FillChar(at(i)^, width(c), 0);
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.insertItem(i: Integer; o: XObject): XArray;
begin
  xrPbegin([@o], @Result).setref(Result);

  adjustIndexBeforeInsert(i);
  insertEmpty(i);
  o.setref(at(i)^);

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.insertArray(i: Integer; o: XArray): XArray;
var
  j, c: Integer;
begin
  xrPbegin([@o], @Result).setref(Result);

  adjustIndexBeforeInsert(i);
  c := o.iCount;
  insertEmpty(i, c);
  Move(o.at(0)^, at(i)^, width(c));
  for j := i to i + c - 1 do at(j)^.iref;

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.delete(i: Integer; c: Integer = 1): XArray;
var
  j, p: Integer;
begin
  try
    xrFbegin(@Result).setref(Result);
    adjustIndexBeforeDelete(i, c);
    if i >= getCount then exit;

    c := Min(c, getCount - i);
    p := i + c;

    for j := i to p - 1 do at(j)^.dref;

    if p < getCount then Move(at(p)^, at(i)^, Width(getCount - p));

    decCount(c); adjustLimit(getCount);
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.slice(i: Integer; c: Integer = 1): XArray;
begin
  try
    xrFbegin(@Result).setref(Result);
    adjustIndexBeforeDelete(i, c);
    if (i >= getCount) or (c = 0) then begin clear; exit; end;

    c := Min(c, getCount - i);

    delete(i + c, getCount); delete(0, i); // first cut trailing items, then leading for better performance
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.extract(i: Integer; c: Integer = 1): XArray;
var
  n: Integer;
begin
  xrFbegin(@Result);
  adjustIndexBeforeDelete(i, c);
  new.setCapacity(c).setref(Result);
  for n := i to i + c - 1 do Result.setItem(n - i, getItem(n));
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.cut(i: Integer; c: Integer = 1): XArray;
begin
  xrFbegin(@Result);
  extract(i, c).setref(Result);
  delete(i, c);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.clear: XArray;
var
  i: Integer;
begin
  if Self = nil then begin Result := nil; exit; end;
  
  xrFbegin(@Result).setref(Result);

  for i := 0 to getCount - 1 do xrnil(at(i)^);
  adjustLimit(setCount(0));

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.invert: XArray;
var
  c, i: Integer;
begin
  xrFbegin(@Result).setref(Result);
  c := getCount;
  if c > 1 then begin
    for i := 0 to (c div 2) - 1 do begin
      memSwap(at(i)^, at(c - i - 1)^, SizeOf(XObject));
    end;
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.getItem(i: Integer): XObject;
var
  p: PXObject;
begin
  xrFbegin(@Result);

  p := at(i);
  if p <> nil then p^.setref(Result);

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.setItem(i: Integer; o: XObject): XArray;
begin
  xrPbegin([@o], @Result).setref(Result);
  try
    if i >= getCount then exit;
    o.setref(at(i)^);
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.setCount(c: Integer): Integer;
begin
  xrFbegin();
  if c < 0 then c := 0; iCount := c; Result := c;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.setLimit(l: Integer): Integer;
begin
  xrFbegin();
  if l < 0 then l := 0; iLimit := l; Result := l;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.setDelta(d: Integer): XArray;
begin
  xrFbegin(@Result).setref(Result);
  if d < 4 then d := 4; iDelta := d;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.incCount(c: Integer = 1): Integer;
begin
  xrFbegin();
  if iCount + c < 0 then c := -iCount; Inc(iCount, c); Result := iCount;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.decCount(c: Integer = 1): Integer;
begin
  xrFbegin();
  Result := incCount(-c);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.getCount: Integer;
begin
  if Self = nil then begin Result := 0; exit; end; // anti GPF

  xrFbegin();
  Result := iCount;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.getLimit: Integer;
begin
  xrFbegin();
  Result := iLimit;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.getDelta: Integer;
begin
  xrFbegin();
  Result := iDelta;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.swapItems(IX, IY: Integer): XArray;
begin
  xrFbegin(@Result).setref(Result);
  memSwap(at(IX)^, at(IY)^, SizeOf(XObject));
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.quickSort(ALo: Integer = -1; AHi: Integer = -1): XArray;
{--------------------------------------}
  procedure QSort(iLo, iHi: Integer);
  var
    Lo, Hi, Mid: Integer;
    key: XObject;
  begin
    key := nil; // deprecated but works :-)
    Lo := iLo;
    Hi := iHi;
    Mid := (Lo + Hi) shr 1;
    getItem(Mid).setref(key);
    repeat
      while compareItems(getItem(Lo), key) < 0 do Inc(Lo);
      while compareItems(getItem(Hi), key) > 0 do Dec(Hi);
      if Lo <= Hi then begin
        swapItems(Lo, Hi); Inc(Lo); Dec(Hi);
      end;
    until Lo > Hi;
    xrnil(key);
    if iLo < Hi then QSort(iLo, Hi);
    if Lo < iHi then QSort(Lo, iHi);
  end;
{--------------------------------------}
begin
  try
    xrFbegin(@Result).setref(Result);
    if ALo < 0 then ALo := 0;
    if (AHi < 0) or (AHi >= getCount) then AHi := getCount - 1;
    if ALo > AHi then exit;
    QSort(ALo, AHi);
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.insertionSort(ALo: Integer = -1; AHi: Integer = -1): XArray;
var
  i, j: Integer;
  item: XObject;
begin
  xrbegin([], [@item], @Result).setref(Result);

  if ALo < 0 then ALo := 0;
  if (AHi < 0) or (AHi >= getCount) then AHi := getCount - 1;

  for i := ALo + 1 to AHi do begin
    getItem(i).setref(item);
    j := i - 1;
    while (j >= ALo) and (compareItems(item, getItem(j)) < 0) do begin
      setItem(j + 1, getItem(j));
      Dec(j);
    end;
    setItem(j + 1, item);
    xrnil(item);
  end;

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.selectionSort(ALo: Integer = -1; AHi: Integer = -1): XArray;
var
  i, j, k: Integer;
begin
  xrFbegin(@Result).setref(Result);

  if ALo < 0 then ALo := 0;
  if (AHi < 0) or (AHi >= getCount) then AHi := getCount - 1;

  for i := ALo to AHi - 1 do begin
    k := i;
    for j := i + 1 to AHi do
      if compareItems(getItem(j), getItem(k)) < 0 then k := j;
    swapItems(k, i);
  end;

  xrend;
end;
{----------------------------------------------------------------------------}
procedure XArray.adjustBeforeSearch(var aLo, aHi: Integer);
begin
  xrFbegin();
  if aHi < aLo then memSwap(aLo, aHi, SizeOf(aLo));
  if aLo < 0 then aLo := 0;
  if aLo > getCount - 1 then aLo := getCount - 1;
  if (aHi < 0) or (aHi > getCount - 1) then aHi := getCount - 1;
  aLo := Max(aLo, 0);
  aHi := Min(aHi, getCount - 1);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.encodeSearchResult(aResult: Integer; aFound: Boolean): Integer;
begin
  xrFbegin();
  if not aFound then Result := -aResult - 1 else Result := aResult;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.decodeSearchResult(aResult: Integer): Integer;
begin
  xrFbegin();
  if aResult < 0 then Result := -aResult - 1 else Result := aResult;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.areExact(IX, IY: XObject): Boolean;
begin
  xrPbegin([@IX, @IY]);
  Result := (IX.getHandle = IY.getHandle);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.exactNeighbourSearch(aObject: XObject; aStart, aLo, aHi: Integer): Integer;
var
  i: Integer;
begin
  xrPbegin([@aObject]);
  try
    i := aStart;
    while (i >= aLo) and (compareItems(aObject, getItem(i)) = 0) do begin
      if areExact(aObject, getItem(i)) then begin
        Result := encodeSearchResult(i, True);
        exit;
      end;
      Dec(i);
    end;
    i := aStart + 1;
    while (i < getCount) and (compareItems(aObject, getItem(i)) = 0) do begin
      if areExact(aObject, getItem(i)) then begin
        Result := encodeSearchResult(i, True);
        exit;
      end;
      Inc(i);
    end;
    Result := encodeSearchResult(aStart, False);
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.scanSearch(aObject: XObject; Exact: Boolean = False;
  aLo: Integer = -1; aHi: Integer = -1): Integer;
var
  i, c: Integer;
  o: XObject;
begin
  xrbegin([@aObject], [@o]);
  try
    Result := encodeSearchResult(getCount, False);
    if aObject = nil then exit;

    adjustBeforeSearch(aLo, aHi);

    i := aLo;
    c := 1;
    while i <= aHi do begin
      getItem(i).setref(o);
      c := compareItems(aObject, o);
      if areExact(aObject, o) or ((c = 0) and (not Exact)) then break;
      c := 1;
      Inc(i);
    end;

    Result := encodeSearchResult(i, (c = 0));
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.linearSearch(aObject: XObject; Exact: Boolean = False;
  aLo: Integer = -1; aHi: Integer = -1): Integer;
var
  i: Integer;
  c: Integer;
begin
  xrPbegin([@aObject]);
  try
    Result := encodeSearchResult(getCount, False);
    if aObject = nil then exit;

    adjustBeforeSearch(aLo, aHi);

    c := 1;
    i := aLo;
    while i <= aHi do begin
      c := compareItems(aObject, getItem(i));
      if c <= 0 then break;
      Inc(i);
    end;

    if (Exact) and (c = 0) then
      Result := exactNeighbourSearch(aObject, i, aLo, aHi)
    else
      Result := encodeSearchResult(i, (c = 0));
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.binarySearch(aObject: XObject; Exact: Boolean = False; aLo: Integer = -1; aHi: Integer = -1): Integer;
var
  i, c: Integer;
begin
  xrPbegin([@aObject]);
  try
    Result := encodeSearchResult(getCount, False);
    if aObject = nil then exit;

    adjustBeforeSearch(aLo, aHi);
    if (aLo = -1) or (aHi = -1) then exit;

    c := -1;
    i := aLo + 1;
    while aLo <= aHi do begin
      i := (aLo + aHi) shr 1;
      c := compareItems(aObject, getItem(i));
      if c = 0 then break;
      if c > 0 then aLo := i + 1 else aHi := i - 1;
    end;

    if c > 0 then Inc(i); { TODO : experimental }
    // if aLo > sHi then Inc(i);

    if (Exact) and (c = 0) then
      Result := exactNeighbourSearch(aObject, i, aLo, aHi)
    else
      Result := encodeSearchResult(i, (c = 0));
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
{ TODO : something is wrong in interpolation search - GPF }
function XArray.interpolationSearch(aObject: XObject; Exact: Boolean = False;
  aLo: Integer = -1; aHi: Integer = -1): Integer;
var
  a, estimatedDiff: Double;
  i, c: Integer;
  sHi: Integer;
begin
  xrPbegin([@aObject]);
  try
    Result := encodeSearchResult(getCount, False);
    if aObject = nil then exit;

    adjustBeforeSearch(aLo, aHi);

    sHi := aHi;

    c := -1;
    i := aLo + 1;
    while aLo <= aHi do begin
      estimatedDiff := estimateItem(getItem(aHi)) - estimateItem(getItem(aLo));
      if estimatedDiff = 0 then
        a := 0.5 // use binary search here
      else
        a := (estimateItem(aObject) - estimateItem(getItem(aLo))) / (estimatedDiff);

      if a < 0 then begin i := aLo; break; end
      else if a > 1 then begin i := aHi + 1; break; end
      else begin
        i := aLo + Round(Int((aHi - aLo)*a));
        c := compareItems(aObject, getItem(i));
        if c = 0 then break;
        if c > 0 then aLo := i + 1 else aHi := i - 1;
      end;
    end;

    if c > 0 then Inc(i); // experimantal
    if aLo > sHi then Inc(i);

    if (Exact) and (c = 0) then
      Result := exactNeighbourSearch(aObject, i, aLo, aHi)
    else
      Result := encodeSearchResult(i, (c = 0));
  finally
    xrend;
  end;
end;
{----------------------------------------------------------------------------}
function XArray.search(aObject: XObject; Exact: Boolean = False;
  aLo: Integer = -1; aHi: Integer = -1; aSearchStrategy: TASearchStrategy = xassBinary): Integer;
begin
  xrPbegin([@aObject]);
  case aSearchStrategy of
    xassScan: Result := scanSearch(aObject, Exact, aLo, aHi);
    xassLinear: Result := linearSearch(aObject, Exact, aLo, aHi);
    xassBinary: Result := binarySearch(aObject, Exact, aLo, aHi);
    xassInterpolation: Result := interpolationSearch(aObject, Exact, aLo, aHi);
  else
    Result := encodeSearchResult(getCount, False);
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.isSorted: Boolean;
var
  i: Integer;
begin
  xrFbegin();
  Result := True;
  for i := 1 to getCount - 1 do begin
    if compareItems(getItem(i), getItem(i-1)) < 0 then begin
      Result := False; break;
    end;
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.searchInsert(o: XObject; Exact: Boolean = False;
  aAllowDuplicates: Boolean = True; aStrategy: TASearchStrategy = xassBinary): XArray;
var
  p: Integer;
begin
  xrPbegin([@o], @Result).setref(Result);

  p := search(o, Exact, 0, getCount - 1, aStrategy);
  if not((p >= 0) and (not aAllowDuplicates)) then insertItem(decodeSearchResult(p), o);

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.searchDelete(o: XObject; Exact: Boolean = False;
  aStrategy: TASearchStrategy = xassBinary): Boolean;
var
  p: Integer;
begin
  xrPbegin([@o]);

  Result := False;
  p := search(o, Exact, 0, getCount - 1, aStrategy);
  if p >= 0 then begin delete(p); Result := True; end;

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.forEach(i: XObjectIterator): XArray;
var
  j: Integer;
begin
  xrPbegin([@i], @Result).setref(Result);
  for j := 0 to iCount - 1 do i.action(getItem(j));
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.filter(f: XObjectFilter; negativeFilter: Boolean = False): XArray;
var
  i, c: Integer;
  res: Boolean;
begin
  xrPbegin([@f], @Result).setref(Result);
  i := iCount - 1; // last item
  c := 0; // counter
  while i >= -1 do begin
    if (i >= 0) then res := f.filter(getItem(i)) else res := True;
    res := res xor negativeFilter;

    if res then begin
      if c > 0 then begin delete(i + 1, c); c := 0; end;
    end else
      Inc(c);

    Dec(i);
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.filterDelete(f: XObjectFilter): XArray;
begin
  xrPbegin([@f], @Result).setref(Result);
  filter(f, True);
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.filterCopy(f: XObjectFilter): XArray;
var
  itm: XObject;
  i: Integer;
begin
  xrbegin([@f], [@itm], @Result);
  new.setref(Result);
  for i := 0 to iCount - 1 do begin
    if f.filter(getItem(i).setref(itm)) then Result.addItem(itm);
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.filterCut(f: XObjectFilter): XArray;
var
  itm: XObject;
  i, c: Integer;
  res: Boolean;
begin
  xrbegin([@f], [@itm], @Result);

  new.setref(Result);
  i := 0;
  c := 0;
  while i <= iCount do begin
    if i < iCount then res := f.filter(getItem(i).setref(itm)) else res := False;

    if res then begin
      Result.addItem(itm);
      Inc(c);
    end else begin
      if c > 0 then begin delete(i - c, c); c := 0; end;
    end;

    Inc(i);
  end;

  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.filterCount(f: XObjectFilter): Integer;
var
  i: Integer;
begin
  xrPbegin([@f]);
  Result := 0;
  for i := 0 to iCount - 1 do begin
    if f.filter(getItem(i)) then Inc(Result);
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.findFirst(f: XObjectFilter): Integer;
begin
  xrPbegin([@f]); Result := findNext(f); xrend;
end;
{----------------------------------------------------------------------------}
function XArray.findNext(f: XObjectFilter; aStart: Integer = 0): Integer;
var
  i: Integer;
begin
  xrPbegin([@f]);
  Result := -1;
  for i := aStart to iCount - 1 do begin
    if f.filter(getItem(i)) then begin Result := i; break; end;
  end;
  xrend;
end;
{----------------------------------------------------------------------------}
function XArray.findLast(f: XObjectFilter): Integer;
var
  i: Integer;
begin
  xrPbegin([@f]);
  Result := -1;
  for i := iCount - 1 downto 0 do begin
    if f.filter(getItem(i)) then begin Result := i; break; end;
  end;
  xrend;
end;
{ *************************************************************************** }
initialization
  registerMagic(XArray);
{ *************************************************************************** }
end.
