unit MXBigIntDynamic;

interface

uses
  Math;

type
  TBigIntValue = packed array of Byte;

const
  MAXRADIX = 36;
  RADIXCHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

function mcopy(const a: TBigIntValue): TBigIntValue;
function mlen(const m: TBigIntValue): Integer;
procedure msetlen(var m: TBigIntValue; n: Integer);
procedure mexpand(var m: TBigIntValue; n: Integer);
procedure mstrip(var m: TBigIntValue);
function mget(const m: TBigIntValue; n: Integer): Byte;
function mset(var m: TBigIntValue; n: Integer; b: Byte): Byte;
procedure msetval(var m: TBigIntValue; v: Int64);
function mcreate(v: Int64): TBigIntValue;
function mgetval(var m: TBigIntValue): Int64;
procedure madd(var a: TBigIntValue; const b: TBigIntValue);
procedure madd_i(var a: TBigIntValue; b: Integer; i: Integer = 0);
procedure msub(var a: TBigIntValue; const b: TBigIntValue);
procedure mmul(var a: TBigIntValue; const b: TBigIntValue);
procedure mmul_b(var a: TBigIntValue; b: Byte);
function mcmp(const a, b: TBigIntValue): Integer;
procedure mshl(var a: TBigIntValue; c: Integer);
procedure mshr(var a: TBigIntValue; c: Integer);
procedure mdivmod(var d, m, a: TBigIntValue; const b: TBigIntValue);
procedure mdiv(var a: TBigIntValue; const b: TBigIntValue);
procedure mmod(var a: TBigIntValue; const b: TBigIntValue);

function mi2c(i: Integer): Char;
function mc2i(c: Char): Integer;
function mmap2str(a: TBigIntValue; radix: Integer): String;
function mstr2map(const s: String; radix: Integer): TBigIntValue;

implementation

{*****************************************************************************}
function mcopy(const a: TBigIntValue): TBigIntValue;
begin Result := Copy(a, 0, mlen(a)); end;
{-----------------------------------------------------------------------------}
function mlen(const m: TBigIntValue): Integer;
begin Result := Length(m); end;
{-----------------------------------------------------------------------------}
procedure msetlen(var m: TBigIntValue; n: Integer);
var
  l: Integer;
begin
  l := mlen(m); SetLength(m, n);
  while l < mlen(m) do begin m[l] := 0; Inc(l); end;
end;
{-----------------------------------------------------------------------------}
procedure mexpand(var m: TBigIntValue; n: Integer);
begin
  if n > mlen(m) then msetlen(m, n);
end;
{-----------------------------------------------------------------------------}
procedure mstrip(var m: TBigIntValue);
var
  i, l: Integer;
begin
  l := 0;
  for i := 0 to mlen(m) - 1 do if m[i] <> 0 then l := i;
  msetlen(m, l + 1);
end;
{-----------------------------------------------------------------------------}
function mget(const m: TBigIntValue; n: Integer): Byte;
begin if n >= mlen(m) then Result := 0 else Result := m[n]; end;
{-----------------------------------------------------------------------------}
function mset(var m: TBigIntValue; n: Integer; b: Byte): Byte;
begin mexpand(m, n + 1); m[n] := b; Result := b; end;
{-----------------------------------------------------------------------------}
procedure msetval(var m: TBigIntValue; v: Int64);
var
  i: Integer;
begin
  v := Abs(v);
  msetlen(m, SizeOf(v));
  for i := 0 to mlen(m) - 1 do begin
    m[i] := Byte(v); v := v shr 8;
  end;
  mstrip(m);
end;
{-----------------------------------------------------------------------------}
function mcreate(v: Int64): TBigIntValue;
begin msetval(Result, v); end;
{-----------------------------------------------------------------------------}
function mgetval(var m: TBigIntValue): Int64;
var
  i: Integer;
begin
  Result := 0;
  for i := mlen(m) - 1 downto 0 do Result := (Result shl 8) + m[i];
end;
{-----------------------------------------------------------------------------}
procedure madd(var a: TBigIntValue; const b: TBigIntValue);
var
  i, v, c: Integer;
begin
  mexpand(a, Max(mlen(a), mlen(b)) + 1);
  c := 0;
  for i := 0 to mlen(a) - 1 do begin
    v := c + a[i] + mget(b, i);
    a[i] := Byte(v);
    c := v shr 8;
  end;
  mstrip(a);
end;
{-----------------------------------------------------------------------------}
procedure madd_i(var a: TBigIntValue; b: Integer; i: Integer = 0);
var
  v: Integer;
begin
  if b = 0 then exit;
  v := mget(a, i) + b; mset(a, i, Byte(v));
  madd_i(a, v shr 8, i + 1);
end;
{-----------------------------------------------------------------------------}
procedure msub(var a: TBigIntValue; const b: TBigIntValue);
var
  i, v, c: Integer;
begin
  c := 0;
  for i := 0 to mlen(a) - 1 do begin
    v := c + a[i] - mget(b, i);
    if v < 0 then begin
      c := -1; v := v + $100;
    end else c := 0;
    a[i] := Byte(v);
  end;
  mstrip(a);
end;
{-----------------------------------------------------------------------------}
procedure mmul(var a: TBigIntValue; const b: TBigIntValue);
var
  i, j, v: Integer;
  r: TBigIntValue;
begin
  msetlen(r, mlen(a) + mlen(b));
  for j := 0 to mlen(b) - 1 do begin
    for i := 0 to mlen(a) - 1 do begin
      v := Integer(a[i]) * Integer(b[j]); madd_i(r, v, i + j);
    end;
  end;
  a := r; mstrip(a);
end;
{-----------------------------------------------------------------------------}
procedure mmul_b(var a: TBigIntValue; b: Byte);
var
  i, v: Integer;
  r: TBigIntValue;
begin
  msetlen(r, mlen(a) + 1);
  for i := 0 to mlen(a) - 1 do begin
    v := Integer(a[i]) * b; madd_i(r, v, i);
  end;
  a := r; mstrip(a);
end;
{-----------------------------------------------------------------------------}
function mcmp(const a, b: TBigIntValue): Integer;
var
  l, c, i: Integer;
begin
  l := Max(mlen(a), mlen(b));
  c := 0;
  for i := l - 1 downto 0 do begin
    c := Integer(mget(a, i)) - Integer(mget(b, i));
    if c <> 0 then break;
  end;
  if c < 0 then c := -1 else if c > 0 then c := 1;
  Result := c;
end;
{-----------------------------------------------------------------------------}
procedure mshl_8(var a: TBigIntValue);
var
  i: Integer;
begin
  for i := mlen(a) - 1 downto 1 do a[i] := a[i - 1];
  a[0] := 0;
end;
{-----------------------------------------------------------------------------}
procedure mshl_1(var a: TBigIntValue);
var
  i, c, new_c: Integer;
begin
  c := 0;
  for i := 0 to mlen(a) - 1 do begin
    new_c := (a[i] and $80) shr 7;
    a[i] := (a[i] shl 1) or c;
    c := new_c;
  end;
end;
{-----------------------------------------------------------------------------}
procedure mshl(var a: TBigIntValue; c: Integer);
var
  b: Integer;
begin
  b := mlen(a) + ((c + 7) div 8); mexpand(a, b);
  while c > 8 do begin mshl_8(a); Dec(c, 8); end;
  while c > 0 do begin mshl_1(a); Dec(c); end;
  mstrip(a);
end;
{-----------------------------------------------------------------------------}
procedure mshr_8(var a: TBigIntValue);
var
  i, l: Integer;
begin
  l := mlen(a) - 1;
  for i := 1 to l do a[i - 1] := a[i];
  a[l] := 0;
end;
{-----------------------------------------------------------------------------}
procedure mshr_1(var a: TBigIntValue);
var
  i, c, new_c: Integer;
begin
  c := 0;
  for i := mlen(a) - 1 downto 0 do begin
    new_c := (a[i] and $01) shl 7;
    a[i] := (a[i] shr 1) or c;
    c := new_c;
  end;
end;
{-----------------------------------------------------------------------------}
procedure mshr(var a: TBigIntValue; c: Integer);
begin
  while c > 8 do begin mshr_8(a); Dec(c, 8); end;
  while c > 0 do begin mshr_1(a); Dec(c); end;
  mstrip(a);
end;
{-----------------------------------------------------------------------------}
function mfit_l(var a: TBigIntValue; const b: TBigIntValue): Integer;
begin
  Result := 0;
  while mcmp(a, b) >= 0 do begin msub(a, b); Inc(Result); end;
end;
{-----------------------------------------------------------------------------}
function mfit_b(var a: TBigIntValue; const b: TBigIntValue): Integer;
label
  fit_exit;
var
  f: Integer absolute Result;
  c, bb: Integer;
  btf, ambf: TBigIntValue;
begin
  f := $80;
  bb := $80;
  c := 0;
  while bb <> 0 do begin
    btf := mcopy(b); mmul_b(btf, f); c := mcmp(btf, a);
    if c = 0 then goto fit_exit;
    if c < 0 then begin
      ambf := mcopy(a); msub(ambf, btf);
      if mcmp(ambf, b) < 0 then goto fit_exit;
    end;
    if c > 0 then f := f and not bb;
    bb := bb shr 1; f := f or bb;
  end;
fit_exit:
  if c = 0 then a := mcreate(0);
  if c < 0 then a := ambf;
end;
{-----------------------------------------------------------------------------}
procedure mdivmod(var d, m, a: TBigIntValue; const b: TBigIntValue);
var
  c, v: Integer;
begin
  msetlen(d, mlen(a));
  c := mlen(a) - 1;
  msetval(m, 0);

  while c >= 0 do begin
    mshl(m, 8); m[0] := a[c];
    v := mfit_l(m, b);
    d[c] := Byte(v);
    Dec(c);
  end;
  mstrip(d); mstrip(m);
end;
{-----------------------------------------------------------------------------}
procedure mdiv(var a: TBigIntValue; const b: TBigIntValue);
var
  d, m: TBigIntValue;
begin
  mdivmod(d, m, a, b); a := d;
end;
{-----------------------------------------------------------------------------}
procedure mmod(var a: TBigIntValue; const b: TBigIntValue);
var
  d, m: TBigIntValue;
begin
  mdivmod(d, m, a, b); a := m;
end;
{-----------------------------------------------------------------------------}
function mi2c(i: Integer): Char;
begin Result := RADIXCHARS[i+1]; end;
{-----------------------------------------------------------------------------}
function mc2i(c: Char): Integer;
begin Result := Pos(c, RADIXCHARS) - 1; end;
{-----------------------------------------------------------------------------}
function mmap2str(a: TBigIntValue; radix: Integer): String;
var
  v, z, r, d, m: TBigIntValue;
begin
  Result := '';
  v := mcopy(a);
  msetval(z, 0);
  msetval(r, radix);
  while mcmp(v, z) <> 0 do begin
    mdivmod(d, m, v, r);
    Result := mi2c(mgetval(m)) + Result;
    v := d;
  end;
  if Result = '' then Result := '0';
end;
{-----------------------------------------------------------------------------}
function mstr2map(const s: String; radix: Integer): TBigIntValue;
var
  v, r: TBigIntValue;
  c: Integer;
begin
  msetval(v, 0);
  msetval(r, radix);
  c := 1;
  while c <= Length(s) do begin
    mmul(v, r); madd_i(v, mc2i(s[c])); Inc(c);
  end;
  Result := v;
end;
{*****************************************************************************}
end.
