#!/bin/sh
DoExitAsm ()
{ echo "An error occurred while assembling $1"; exit 1; }
DoExitLink ()
{ echo "An error occurred while linking $1"; exit 1; }
echo Linking MyRenamer
OFS=$IFS
IFS="
"
/usr/bin/ld /usr/lib/crt1.o      -pagezero_size 0x10000 -multiply_defined suppress -L. -o MyRenamer `cat link.res`
if [ $? != 0 ]; then DoExitLink MyRenamer; fi
IFS=$OFS
