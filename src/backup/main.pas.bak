unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, EditBtn, Grids, ExtCtrls, Buttons, ComCtrls, INIFiles,
  common, types, MXCfgLog, math;

type

  { TFormMain }

  TFormMain = class(TForm)
    BitBtnQuitSave: TBitBtn;
    BitBtnRename: TBitBtn;
    BitBtnQuit: TBitBtn;
    BitBtnRefresh: TBitBtn;
    CheckBoxExt: TCheckBox;
    CheckBoxCreateFile: TCheckBox;
    CheckBoxSubDirs: TCheckBox;
    CheckBox_d: TCheckBox;
    CheckBoxLeaveOriginalFiles: TCheckBox;
    CheckBoxLowercase: TCheckBox;
    CheckBoxFullPath: TCheckBox;
    ComboBoxConstants: TComboBox;
    DirectoryEditOut: TDirectoryEdit;
    DirectoryEditIn: TDirectoryEdit;
    filterDateTime: TLabeledEdit;
    GroupBoxOutDir: TGroupBox;
    GroupBoxDir: TGroupBox;
    filterDate: TLabeledEdit;
    LabelCount: TLabel;
    LabelConstFnWithoutExt: TLabel;
    LabelConstFn: TLabel;
    LabeledEditInsert: TLabeledEdit;
    LabelOut: TLabel;
    LabeledEditFileMask: TLabeledEdit;
    LabeledEditOutputFilename: TLabeledEdit;
    LabeledEditOutputLine: TLabeledEdit;
    PageControlMain: TPageControl;
    PanelCreateFile: TPanel;
    StatusBarApp: TStatusBar;
    StringGridFileConstants: TStringGrid;
    StringGridFilters: TStringGrid;
    StringGridInput: TStringGrid;
    TabSheetOutLine: TTabSheet;
    TabSheetRenamer: TTabSheet;
    TimerFilters: TTimer;
    TimerMask: TTimer;
    procedure BitBtnQuitClick(Sender: TObject);
    procedure BitBtnQuitSaveClick(Sender: TObject);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure BitBtnRenameClick(Sender: TObject);
    procedure CheckBoxCreateFileChange(Sender: TObject);
    procedure CheckBoxFullPathChange(Sender: TObject);
    procedure CheckBoxLowercaseChange(Sender: TObject);
    procedure CheckBoxSubDirsChange(Sender: TObject);
    procedure ComboBoxConstantsExit(Sender: TObject);
    procedure DirectoryEditInAcceptDirectory(Sender: TObject;
      var Value: String);
    procedure DirectoryEditInKeyPress(Sender: TObject; var Key: char);
    procedure DirectoryEditOutKeyPress(Sender: TObject; var Key: char);
    procedure filterDateTimeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LabeledEditFileMaskChange(Sender: TObject);
    procedure LabeledEditInsertChange(Sender: TObject);
    procedure SpeedButtonQuitClick(Sender: TObject);
    procedure StringGridAddRow(Sender: TObject);
    procedure StringGridFileConstantsEditingDone(Sender: TObject);
    procedure StringGridFiltersEnter(Sender: TObject);
    procedure StringGridInputDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure StringGridInputKeyPress(Sender: TObject; var Key: char);
    procedure StringGridRemoveEmpty(Sender: TObject);
    procedure StringGridFiltersEditingDone(Sender: TObject);
    procedure StringGridResize(Sender: TObject);
    procedure r(Sender: TObject);
    procedure TimerMaskTimer(Sender: TObject);
  private
    { private declarations }
    filtersFile: String;
    constFile: String;
    saveChanges,allow_refresh:Boolean;
    procedure Init;
    procedure RefreshDir(dir: String);
    procedure RefreshFilters;
    function ApplyFilters(container:TFilenameContainer;counter:Integer=0):String;
    procedure CreateFile;
    function GoRename: Boolean;

  public
    FnContainer: AFilenamesConstainer;
    { public declarations }
  end; 

var
  FormMain: TFormMain;

implementation
{ TFormMain }

procedure TFormMain.FormCreate(Sender: TObject);
var
   appPath, iniName: String;
begin
     allow_refresh := False;
     ReadControlPlacement(Self);
     StringGridResize(StringGridFilters);
     with ComboBoxConstants do begin
          Clear;
          Items.Add(const_filter_filename);
          Items.Add(const_filter_filedir);
     end;
     saveChanges := False;
     Self.Caption:='MyRenamer v0.5.4';
     appPath := ExtractFilePath(Application.ExeName);

     filtersFile := appPath + 'filters.txt';
     constFile := appPath + 'constants.txt';
     filterDate.Text := GetDateTimeString(Now);
     filterDateTime.Text := GetDateTimeString(Now, True);

     LabeledEditFileMask.Text := Cfg.AsStringDef[const_ini_file_mask, '*.jpg'];

     DirectoryEditIn.Text := Cfg.AsStringDef[const_ini_dir_input, appPath];
     CheckBoxLeaveOriginalFiles.Checked := Cfg.AsBoolDef[const_ini_leave_files, False];

     LabeledEditOutputFilename.Text := Cfg.AsStringDef[const_ini_out_filename, ''];
     DirectoryEditOut.Text := Cfg.AsStringDef[const_ini_dir_output, appPath];
     LabeledEditOutputLine.Text := Cfg.AsStringDef[const_ini_out_file_line, ''];
     CheckBoxLowercase.Checked := Cfg.AsBoolDef[const_ini_lowercase, False];
     CheckBox_d.Checked := Cfg.AsBoolDef[const_ini_replace_d, False];
     CheckBoxFullPath.Checked := Cfg.AsBoolDef[const_ini_full_path, False];
     CheckBoxExt.Checked := Cfg.AsBoolDef[const_ini_fn_with_ext, False];

     // ----- tab create file
     CheckBoxCreateFile.Checked := Cfg.AsBoolDef[const_ini_create_file, False];
     CheckBoxCreateFileChange(CheckBoxCreateFile);

     StringGridFilters.RowCount:=Cfg.AsIntegerDef[const_ini_filters_rowcount, 2];
     StringGridFilters.LoadFromFile(filtersFile);

     allow_refresh := True;
     RefreshDir(DirectoryEditIn.Text);
end;

procedure TFormMain.Init;
begin
     StringGridInput.Clean;
     StringGridInput.RowCount:=0;
     SetLength(FnContainer, 0);
     StatusBarApp.Panels[1].Text := 'ready';
end;

procedure TFormMain.RefreshDir(dir: String);
begin
     try
        Init;
        if not allow_refresh then Exit;
        // ----------------
        try
           StringGridFileConstants.RowCount:=Cfg.AsIntegerDef[const_ini_constants_rowcount, 2];
           StringGridFileConstants.LoadFromFile(constFile);
        except end;
        // ----------------
        SetLength(FnContainer, 0);
        _ReadDir(dir, @FnContainer, LabeledEditFileMask.Text, CheckBoxSubDirs.Checked);
        LabelCount.Caption:=Format('found %d files', [Length(FnContainer)]);
        RefreshFilters();
     except
           on E:Exception do begin
                   Init;
           end;
     end;
end;

procedure TFormMain.RefreshFilters;
var
   i, len, files_per_percent, counter, percent:Integer;
begin
     if not allow_refresh then Exit;
     len := Length(FnContainer);
     files_per_percent := Ceil(len / 100);
     //StatusBarApp.Panels[1].Text := Format('applying preview to %d files...please wait...', [len]);
     Screen.Cursor:=crHourglass;
     Application.ProcessMessages;
     percent := 0;
     for i:=0 to len-1 do begin
         FnContainer[i].OutputFilename := ApplyFilters(FnContainer[i], i+1);
         StringGridInput.RowCount:=i+1;
         StringGridInput.Rows[i].Text:=FnContainer[i].OutputFilename;
         if i mod files_per_percent = 0 then begin
            inc(percent);
            StatusBarApp.Panels[1].Text :=
            Format('applying preview to %d files...please wait...%d %%', [len, percent]);
            Application.ProcessMessages;
         end;
     end;
     StatusBarApp.Panels[1].Text := 'ready';
     Screen.Cursor:=crDefault;
     Application.ProcessMessages;
end;
function TFormMain.ApplyFilters(container:TFilenameContainer;counter:Integer):String;
var
   i:Integer;
   _from, _to: String;
   ext: String;
begin

     if not CheckBoxExt.Checked then begin
        ext := ExtractFileExt(container.InputFilename);
        Result := ChangeFileExt(container.InputFilename, '');
     end else begin
        ext := '';
        Result := container.InputFilename;
     end;
     if CheckBoxFullPath.Checked then
        Result := AnsiToUTF8(container.InputFileDir) + sep + Result;

     if CheckBoxLowercase.Checked then
        Result := LowerCase(Result);

     for i:=1 to StringGridFilters.RowCount-1 do begin
         _from := StringGridFilters.Rows[i][0];
         _to := StringGridFilters.Rows[i][1];

         //file
         _from := Replace(_from, const_filter_filename, container.InputFilename);
         _to := Replace(_to, const_filter_filename, container.InputFilename);
         //it's dir
         _from := Replace(_from, const_filter_filedir, container.InputFileDir);
         _to := Replace(_to, const_filter_filedir, container.InputFileDir);


         if (_from <> '') or (_to <> '') then
            Result := StringReplace(Result, _from, _to, [rfReplaceAll]);
     end;
     Result := LabeledEditInsert.Text + Result + ext;

     if CheckBox_d.Checked then begin
        Result := Format(Result, [counter]);
     end;
end;

procedure TFormMain.DirectoryEditInAcceptDirectory(Sender: TObject;
  var Value: String);
begin
     Application.ProcessMessages;
     RefreshDir(Value);
end;

procedure TFormMain.CheckBoxLowercaseChange(Sender: TObject);
begin
     Application.ProcessMessages;
     RefreshFilters;
end;

procedure TFormMain.CheckBoxSubDirsChange(Sender: TObject);
begin
  RefreshDir(DirectoryEditIn.Text);
end;

procedure TFormMain.ComboBoxConstantsExit(Sender: TObject);
begin
       ComboBoxConstants.Visible:=False;
       StringGridFilters.Cells[StringGridFilters.Col, StringGridFilters.Row] :=
                                                      ComboBoxConstants.Text;
       StringGridFiltersEditingDone(StringGridFilters);
end;

procedure TFormMain.BitBtnRefreshClick(Sender: TObject);
begin
     RefreshDir(DirectoryEditIn.Text);
end;

procedure TFormMain.CreateFile;
var
   i, k, len, max_len:Integer;
   _filename_, _fn_ucase_, _date_, _datetime_, _line_, tmp_line: String;
   _from, _to, out_dir, out_fn: String;
   _OutFile: TStringList;
begin
    _OutFile := TStringList.Create;
    try
    _date_ := filterDate.Text;
    _datetime_ := filterDateTime.Text;
    _line_ := LabeledEditOutputLine.Text;

    StatusBarApp.Panels[1].Text := 'building output file...please wait...';
    Screen.Cursor:=crHourglass;
    Application.ProcessMessages;
    max_len := 0;
    for i:=0 to Length(FnContainer)-1 do begin
        len := Length(FnContainer[i].OutputFilename);
        if len > max_len then max_len := len;
    end;

    for i:=0 to Length(FnContainer)-1 do begin
         _filename_ := ToStringLength(FnContainer[i].OutputFilename, max_len);
         _fn_ucase_ := ToStringLength(UpperCase(ChangeFileExt(_filename_, '')), max_len);

         tmp_line := _line_;
         tmp_line := StringReplace(tmp_line, const_out_filename, _filename_, [rfReplaceAll]);
         tmp_line := StringReplace(tmp_line, const_out_fn_ucase, _fn_ucase_, [rfReplaceAll]);
         tmp_line := StringReplace(tmp_line, const_out_date, _date_, [rfReplaceAll]);
         tmp_line := StringReplace(tmp_line, const_out_datetime, _datetime_, [rfReplaceAll]);

         for k:=1 to StringGridFileConstants.RowCount-1 do begin
             _from := StringGridFileConstants.Rows[k][0];
             _to := StringGridFileConstants.Rows[k][1];

             if (_from <> '') and (_to <> '') then
                tmp_line := StringReplace(tmp_line, _from, _to, [rfReplaceAll]);
         end;

         _OutFile.Add(tmp_line);
    end;
    out_dir:= DirectoryEditOut.Text+'/';
    MakeDir(out_dir);
    out_fn := LabeledEditOutputFilename.Text;
    out_fn := StringReplace(out_fn, const_out_date, _date_, [rfReplaceAll]);
    out_fn := StringReplace(out_fn, const_out_datetime, _datetime_, [rfReplaceAll]);
    _OutFile.SaveToFile(out_dir + out_fn);
    StatusBarApp.Panels[1].Text := 'ready';
    Screen.Cursor:=crDefault;
    Application.ProcessMessages;
    except
        on E:Exception do
            ShowMessage(E.Message);
    end;
    _OutFile.Free;
    _OutFile := Nil;
end;

function TFormMain.GoRename: Boolean;
var
   i, len:Integer;
   _leave_original, _errors: Boolean;
   out_dir, _from, _to: String;
begin
Result := False;
try
     _errors := False;
     out_dir:= DirectoryEditOut.Text+sep;
     MakeDir(out_dir);
     _leave_original := CheckBoxLeaveOriginalFiles.Checked;
     len := Length(FnContainer);
     Screen.Cursor:=crHourglass;
     Application.ProcessMessages;
     for i:=0 to len-1 do begin
         _from := FnContainer[i].InputFileDir + sep + FnContainer[i].InputFilename;
         _to   := out_dir+FnContainer[i].OutputFilename;
         try
            if _leave_original then begin
            StatusBarApp.Panels[1].Text:=Format('copying %d of %d files', [i+1, len]);
            if CopyFile(_from, _to) then begin
               Screen.Cursor:=crHourglass;
               Application.ProcessMessages;
            end;
            end else begin
            StatusBarApp.Panels[1].Text:=Format('renaming %d of %d files', [i+1, len]);
            if RenameFile(_from, _to) then begin
               Screen.Cursor:=crHourglass;
               Application.ProcessMessages;
            end;
            end;
         except
             on E:Exception do begin
                _errors := True;
                _Log(E.Message);
             end;
         end;
     end;
     Screen.Cursor:=crDefault;
     Application.ProcessMessages;
     if _errors then ShowMessage('There were errors. For details look at the log file located in app folder.');
     Result := True;
except
      on E:Exception do
         ShowMessage(E.Message);
end;
end;

procedure TFormMain.BitBtnRenameClick(Sender: TObject);
begin
     try
        TBitBtn(Self).Enabled := False;
        Application.ProcessMessages;
        if GoRename then begin
           if CheckBoxCreateFile.Checked then CreateFile;
           RefreshDir(DirectoryEditIn.Text);
           end;
     finally
        TBitBtn(Self).Enabled := True;
        Application.ProcessMessages;
     end;
end;

procedure TFormMain.CheckBoxCreateFileChange(Sender: TObject);
begin
     PanelCreateFile.Enabled:=TCheckBox(Sender).Checked;
end;

procedure TFormMain.CheckBoxFullPathChange(Sender: TObject);
begin

end;

procedure TFormMain.BitBtnQuitClick(Sender: TObject);
begin
    saveChanges := False;
    Self.Close;
end;

procedure TFormMain.BitBtnQuitSaveClick(Sender: TObject);
begin
    saveChanges := True;
    Self.Close;
end;

procedure TFormMain.DirectoryEditInKeyPress(Sender: TObject; var Key: char);
begin
     Key:=#0;
end;

procedure TFormMain.DirectoryEditOutKeyPress(Sender: TObject; var Key: char);
begin
     Key:=#0;
end;

procedure TFormMain.filterDateTimeChange(Sender: TObject);
begin

end;

procedure TFormMain.FormDestroy(Sender: TObject);
begin
     WriteControlPlacement(Self);
     if saveChanges then begin
        StringGridFilters.SaveToFile(filtersFile);
        Cfg.AsInteger[const_ini_filters_rowcount] := StringGridFilters.RowCount;

        StringGridFileConstants.SaveToFile(constFile);
        Cfg.AsInteger[const_ini_constants_rowcount] := StringGridFileConstants.RowCount;

        Cfg.AsString[const_ini_file_mask]:=LabeledEditFileMask.Text;
        Cfg.AsString[const_ini_dir_input]:=DirectoryEditIn.Text;
        Cfg.AsBool[const_ini_leave_files]:=CheckBoxLeaveOriginalFiles.Checked;

        Cfg.AsString[const_ini_dir_output]:=DirectoryEditOut.Text;
        Cfg.AsString[const_ini_out_filename]:=LabeledEditOutputFilename.Text;
        Cfg.AsString[const_ini_out_file_line]:=LabeledEditOutputLine.Text;
        Cfg.AsBool[const_ini_lowercase]:=CheckBoxLowercase.Checked;
        Cfg.AsBool[const_ini_replace_d]:=CheckBox_d.Checked;
        Cfg.AsBool[const_ini_full_path]:=CheckBoxFullPath.Checked;
        Cfg.AsBool[const_ini_fn_with_ext]:=CheckBoxExt.Checked;
        // ------ tab create file
        Cfg.AsBool[const_ini_create_file]:=CheckBoxCreateFile.Checked;
     end;
     Init;
end;

procedure TFormMain.LabeledEditFileMaskChange(Sender: TObject);
begin
     TimerMask.Enabled:=False;
     TimerMask.Enabled:=True;
end;

procedure TFormMain.LabeledEditInsertChange(Sender: TObject);
begin
     TimerFilters.Enabled:=False;
     TimerFilters.Enabled:=True;
end;

procedure TFormMain.SpeedButtonQuitClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFormMain.StringGridAddRow(Sender: TObject);
var
   _row, _col, _rows: Integer;
   _text:String;
begin
     _col := TStringGrid(Sender).Col;
     if _col = 0 then Exit;
     _row := TStringGrid(Sender).Row;
     _rows := TStringGrid(Sender).RowCount;
     _text := TStringGrid(Sender).Cells[_col,_row];
     if (_row + 1 = _rows) and (_text <> '') then begin
        TStringGrid(Sender).RowCount:=_rows+1;
     end;
end;

procedure TFormMain.StringGridFileConstantsEditingDone(Sender: TObject);
begin
     StringGridAddRow(Sender);
     StringGridRemoveEmpty(Sender);
end;

procedure TFormMain.StringGridFiltersEnter(Sender: TObject);
var
   nTop, nLeft, nWidth, nHeight : integer;
begin
      with ComboBoxConstants do
      begin
         PositionCB( TStringGrid(Sender), nTop, nLeft, nHeight, nWidth );
         Top    := nTop;
         Left   := nLeft;
         Width  := nWidth;
         Height := nHeight;
         Text := TStringGrid(Sender).Cells[TStringGrid(Sender).Col, TStringGrid(Sender).Row];
         Visible := True;
         ActiveControl := ComboBoxConstants;
      end;
end;

procedure TFormMain.StringGridInputDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
   _cols, _width, i, _margin: Integer;
begin
     _cols := TStringGrid(Sender).ColCount;
   _margin := ceil(25 / _cols);
   _width := Round(TStringGrid(Sender).Width/_cols);
   for i:=0 to _cols-1 do begin
       TStringGrid(Sender).ColWidths[i]:=_width-_margin;
   end;

end;

procedure TFormMain.StringGridInputKeyPress(Sender: TObject; var Key: char);
begin
  Key := #0;
end;

procedure TFormMain.StringGridRemoveEmpty(Sender: TObject);
var
   _row, _col, _rows: Integer;
   _text:String;
begin
     _rows := TStringGrid(Sender).RowCount;
     for _row:=_rows-1 downto 1 do begin
         _text := '';
         for _col:=0 to TStringGrid(Sender).ColCount-1 do
             _text := _text + TStringGrid(Sender).Cells[_col, _row];
         if (_text = '') and (_row < _rows-1) then TStringGrid(Sender).DeleteRow(_row);
     end;
end;

procedure TFormMain.StringGridFiltersEditingDone(Sender: TObject);
begin
     TimerFilters.Enabled:=False;
     StringGridAddRow(Sender);
     StringGridRemoveEmpty(Sender);
     TimerFilters.Enabled:=True;
end;

procedure TFormMain.StringGridResize(Sender: TObject);
var
   _cols, _width, i, _margin: Integer;
begin
     _cols := TStringGrid(Sender).ColCount;
     if _cols=0 then Exit;
     _margin := ceil(25 / _cols);
     _width := Round(TStringGrid(Sender).Width/_cols);
     for i:=0 to _cols-1 do begin
         TStringGrid(Sender).ColWidths[i]:=_width-_margin;
     end;
     Application.ProcessMessages;
end;

procedure TFormMain.r(Sender: TObject);
begin
     TTimer(Sender).Enabled:=False;
     RefreshFilters;
end;

procedure TFormMain.TimerMaskTimer(Sender: TObject);
begin
     TTimer(Sender).Enabled:=False;
     RefreshDir(DirectoryEditIn.Text);
end;

initialization
  {$I main.lrs}

end.

